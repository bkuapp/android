# Android-BKU-Kotlin

BKU App für Android und WearOS entwickelt mit Kotlin.

Zeigt eingeloggten Schülern und Lehrern den Stundenplan, den Vertretungsplan, den Klausurplan (nur für TG-Schüler), die Personenliste (Lehrer für Schüler und Klassen sowie Lehrer für Lehrer), die Raumliste (nur für Lehrer) und die Neuigkeiten.

Die App benutzt eine JSON API auf PHP-Basis für den Datentransfer und Firebase, um Benachrichtigungen zu schicken. Unterstützt werden alle Android und WearOS Geräte ab Version 6.0.

[Dokumentation](https://app.bkukr.de/docs/android)
