package de.bkukr.bku.login

import kotlinx.serialization.Serializable

// Copyright (C) 2020  Sven Op de Hipt
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

/**
 * The data which will be transmitted by the login request.
 *
 * @author Sven Op de Hipt
 *
 * @constructor Creates a login result with the [success] state, the [name] of the person and an [error] message which
 * can be empty.
 *
 * @param [success] true, when the login request was successful
 * @param [name] the full name of the user. empty when [success] is false
 * @param [error] the error message of the login request. empty when [success] is true
 */
@Serializable
data class LoginResult(val success: Boolean, val name: String, val error: String)