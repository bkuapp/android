package de.bkukr.bku.login

import android.content.Intent
import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.preference.PreferenceManager
import de.bkukr.bku.R
import kotlinx.android.synthetic.main.activity_terms_of_use_and_privacy.*

// Copyright (C) 2020  Sven Op de Hipt
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

/**
 * The activity which shows the terms of usage and privacy policy.
 *
 * @author Sven Op de Hipt
 */
class TermsOfUseAndPrivacyActivity : AppCompatActivity() {
    /**
     * Is called when the activity is created. Shows the pds to the user
     *
     * @param [savedInstanceState] the saved instance states for the activity.
     */
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_terms_of_use_and_privacy)

        showPDF()
    }

    /**
     * Is called when the user clicks on the back button and does nothing to prevent the user from going back
     * to the login screen.
     */
    override fun onBackPressed() {}

    /**
     * Is called when the user clicks on the [agreeButton] and goes back to the login view
     */
    fun agree(agreeButton: View) {
        val sharedPref = PreferenceManager.getDefaultSharedPreferences(this)
        sharedPref.edit().putBoolean("terms of use and privacy policy shown", true).apply()

        val intent = Intent(this, LoginActivity::class.java)
        startActivity(intent)
    }

    /**
     * Shows the pdf file to the user.
     */
    private fun showPDF() {
        val pdfFile = resources.openRawResource(R.raw.terms_of_use_and_privacy_policy)
        pdfView.fromStream(pdfFile).load()
    }
}
