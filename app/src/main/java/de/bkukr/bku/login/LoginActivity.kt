package de.bkukr.bku.login

import android.annotation.TargetApi
import android.app.NotificationChannel
import android.app.NotificationManager
import android.content.Intent
import android.content.SharedPreferences
import android.content.pm.ShortcutInfo
import android.content.pm.ShortcutManager
import android.graphics.Paint
import android.graphics.drawable.Icon
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import android.view.View
import android.view.inputmethod.EditorInfo
import android.widget.Switch
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.app.AppCompatDelegate
import androidx.preference.PreferenceManager
import com.google.firebase.messaging.FirebaseMessaging
import de.bkukr.bku.R
import de.bkukr.bku.general.*
import de.bkukr.bku.handler.MenuHandler
import de.bkukr.bku.network.JSONHandlerPhone
import kotlinx.android.synthetic.main.activity_login.*
import java.util.*

// Copyright (C) 2020  Sven Op de Hipt
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

/**
 * The activity which shows the login screen.
 *
 * @author Sven Op de Hipt
 */
class LoginActivity : AppCompatActivity(), SharedPreferences.OnSharedPreferenceChangeListener {
    /**
     * Indicates if the loading circle is currently shown.
     */
    var loadingCircleShown = false
        set(value) {
            field = value

            if (value) {
                loadingPanel.visibility = View.VISIBLE
            }
            else {
                loadingPanel.visibility = View.GONE
            }
        }

    /**
     * Is called by the system if the activity is created.
     *
     * @param [savedInstanceState] the saved instance state for the activity
     */
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val sharedPref = PreferenceManager.getDefaultSharedPreferences(this)

        // show the terms of use and privacy policy view if it wasn't shown before
        /*if (!sharedPref.getBoolean("terms of use and privacy policy shown", false)) {
            val intent = Intent(this, TermsOfUseAndPrivacyActivity::class.java)
            startActivity(intent)
            return
        }*/

        title = getString(R.string.welcome)

        createNotificationChannel()

        if (isLoggedIn) {
            showMainView(intent)
        }

        // set the dark mode corresponding to the system settings (API 29, Android 10) or to the local settings
        AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_FOLLOW_SYSTEM)

        setContentView(R.layout.activity_login)

        // set the username text view's text to the saved username
        val username = sharedPref.getString("username", null)
        if (username != null) {
            username_tf.setText(username)
        }

        username_tf.setHintTextColor(getColorFromAttr(android.R.attr.textColor))
        password_tf.setHintTextColor(getColorFromAttr(android.R.attr.textColor))

        // log the user in when user clicks on next in the password field
        password_tf.setOnEditorActionListener { _, actionId, _ ->
            if (actionId == EditorInfo.IME_ACTION_DONE) {
                login()
                true
            } else {
                false
            }
        }

        // support email
        aMakerSpaceProject.paintFlags = aMakerSpaceProject.paintFlags or Paint.UNDERLINE_TEXT_FLAG
        aMakerSpaceProject.setOnClickListener {
            writeEmail(arrayOf("bkuapp@bkukr.de"))
        }
    }

    /**
     * Is called by the system when a new intent is received. Parses the intent to the main activity
     * when the user is logged in.
     *
     * @param [intent] the new intent which is received
     */
    override fun onNewIntent(intent: Intent?) {
        super.onNewIntent(intent)

        if (intent != null && intent.hasExtra("id") && isLoggedIn) {
            showMainView(intent)
        }
    }

    /**
     * Is called when an activity after this is closed with a [resultCode]. Closes the app if the [requestCode] is 0.
     *
     * @param [requestCode] the request which was sent by the activity.
     * @param [resultCode] the result code from the activity above.
     * @param [data] the data which was delivered with the result code
     */
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        Log.d("result Code", resultCode.toString())
        when (requestCode) {
            0 -> finishAffinity()
        }
    }

    /**
     * Creates the [menu] which is shown in the top right.
     *
     * @param [menu] the menu where the items will be shown
     *
     * @return true, if the method handled the [menu] creation. Otherwise it will be handled by the parent class.
     */
    override fun onCreateOptionsMenu(menu: Menu) =
        super.onCreateOptionsMenu(menu)

    /**
     * Is called when the user clicks on a menu [item].
     *
     * @param [item] the menu item which was clicked on
     *
     * @return true, if the method handled the click
     */
    override fun onOptionsItemSelected(item: MenuItem) = MenuHandler.onOptionsItemSelected(item, this)

    /**
     * Is called if a setting is changed by the user.
     *
     * @param [sharedPreferences] the shared preferences where the setting changed
     * @param [key] the key of the changed setting
     */
    override fun onSharedPreferenceChanged(sharedPreferences: SharedPreferences, key: String) {
        if (key == "darkMode") {
            val darkTheme = sharedPreferences.getBoolean("darkMode", true)
            if (darkTheme) {
                AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_YES)
            } else {
                AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO)
            }
        }
    }

    /**
     * Is called when the user tabs the back button and minimizes the app.
     */
    override fun onBackPressed() {
        finishAffinity()
    }

    /**
     * Is called when the user clicks on the login [button] and performs the login request.
     *
     * @param [button] the login which was clicked
     */
    fun onClick(button: View) {
        login()
    }

    /**
     * Performs the login request and shows the main view if the login credentials a correct.
     */
    private fun login() {
        val username = username_tf.text.toString()
        val password = password_tf.text.toString()

        val sharedPref = PreferenceManager.getDefaultSharedPreferences(this)
        val editor = sharedPref.edit()

        checkLoginData(username, password, editor, {
            // login data are correct
            editor.putString("username", username)
            if (stayLoggedIn.isChecked) {
                editor.putBoolean("loggedIn", true)
                editor.apply()
                if (!isTeacher) {
                    // subscribe to class name as firebase topic for class notifications
                    FirebaseMessaging.getInstance().subscribeToTopic(username.substring(0 until 4).
                        toLowerCase(Locale.GERMAN))
                }

                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N_MR1) {
                    setShortcutItem()
                }
            }
            showMainView(null)
        }, { message ->
            // login data are incorrect
            displayMessage(message)
        })
    }

    /**
     * Checks the login ([username] and [password]) credentials of the user. Calls the [onSuccess] method if the credentials are correct and
     * the [onFailure] method if they are incorrect or another error occurred.
     *
     * @param [username] the username which should be checked
     * @param [password] the password which should be checked
     * @param [editor] the editor of the current shared preferences
     * @param [onSuccess] will be called if the login credentials are correct
     * @param [onFailure] will be called if the login credentials are incorrect or another error occurred
     *
     */
    private fun checkLoginData(username: String, password: String, editor: SharedPreferences.Editor,
                               onSuccess: () -> Unit, onFailure: (message: String) -> Unit) {
        FirebaseMessaging.getInstance().token.addOnSuccessListener { result ->
            if(result != null){

                val token = result
                val params = mutableMapOf(
                    "username" to username,
                    "password" to password
                )

                if (stayLoggedIn.isChecked) {
                    params["token"] = token
                }

                editor.putString("token", token)

                loadingCircleShown = true

                // performs the login request
                val serializer = LoginResult.serializer()
                JSONHandlerPhone.getJSON(this, 1, serializer, params) { result ->
                    loadingCircleShown = false
                    if (result.success) {
                        // Not needed at the moment. needed later when adding students help students feature
                        PreferenceManager.getDefaultSharedPreferences(this).edit().
                        putString("fullName", result.name).apply()
                        onSuccess()
                    }
                    else {
                        onFailure(result.error)
                    }
                }
            }





            //task. addOnFailureListener {
            //    onFailure(getString(R.string.networkErrorFirebase))
            //}
        }
    }

    /**
     * Shows the main view the login credentials are correct or the user was already logged in.
     *
     * @param [intent] the intent which will be passed to the main view
     */
    private fun showMainView(intent: Intent?) {
        val toSchedule = Intent(this, MainActivity::class.java)
        val extras = intent?.extras
        if (extras != null) {
            toSchedule.putExtras(extras)
        }
        startActivityForResult(toSchedule, 0)
    }

    /**
     * Displays a [message] to the user via a toast.
     *
     * @param [message] the message which will be displayed to the user
     */
    private fun displayMessage(message: String) {
        Toast.makeText(this, message, Toast.LENGTH_LONG).show()
    }

    /**
     * Creates the notification channel which is needed to show notifications to the user.
     */
    private fun createNotificationChannel() {
        // Create the NotificationChannel, but only on API 26+ because
        // the NotificationChannel class is new and not in the support library
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val name = getString(R.string.app_name)
            val description = getString(R.string.app_name)
            val importance = NotificationManager.IMPORTANCE_HIGH
            val channel = NotificationChannel("1", name, importance)
            channel.description = description
            // Register the channel with the system; you can't change the importance
            // or other notification behaviors after this
            val notificationManager = getSystemService(NotificationManager::class.java)
            notificationManager?.createNotificationChannel(channel)
        }
    }

    /**
     * Creates the shortcut which will appear at the app and lets the user open the app with a specific tab directly.
     */
    @TargetApi(25)
    private fun setShortcutItem() {
        val shortcutManager = getSystemService(ShortcutManager::class.java)

        // schedule shortcut
        val scheduleIntent = Intent(Intent.ACTION_VIEW, Uri.EMPTY, this, MainActivity::class.java)
        val scheduleBundle = Bundle()
        scheduleBundle.putInt("tabNumber", 0)
        scheduleIntent.putExtras(scheduleBundle)

        val scheduleShortcut = ShortcutInfo.Builder(this, "schedule")
            .setShortLabel(getString(R.string.schedule))
            .setIcon(Icon.createWithResource(this, R.drawable.ic_schedule_shortcut_icon))
            .setIntent(scheduleIntent)
            .build()

        // representation shortcut
        val representationIntent = Intent(Intent.ACTION_VIEW, Uri.EMPTY, this, MainActivity::class.java)
        val representationBundle = Bundle()
        representationBundle.putInt("tabNumber", 1)
        representationIntent.putExtras(representationBundle)

        val representationShortcut = ShortcutInfo.Builder(this, "representation")
            .setShortLabel(getString(R.string.representation))
            .setIcon(Icon.createWithResource(this, R.drawable.ic_representation_shortcut_icon))
            .setIntent(representationIntent)
            .build()

        shortcutManager?.dynamicShortcuts = listOf(scheduleShortcut, representationShortcut)

        // exam shortcut
        if (isTGStudent) {
            val examIntent = Intent(Intent.ACTION_VIEW, Uri.EMPTY, this, MainActivity::class.java)
            val examBundle = Bundle()
            examBundle.putInt("tabNumber", 2)
            examIntent.putExtras(examBundle)

            val examShortcut = ShortcutInfo.Builder(this, "exam")
                .setShortLabel(getString(R.string.examPlan))
                .setIcon(Icon.createWithResource(this, R.drawable.ic_exam_shortcut_icon))
                .setIntent(examIntent)
                .build()

            shortcutManager?.addDynamicShortcuts(listOf(examShortcut))
        }

        // person shortcut
        val personIntent = Intent(Intent.ACTION_VIEW, Uri.EMPTY, this, MainActivity::class.java)
        val personBundle = Bundle()
        personBundle.putInt("tabNumber", 3)
        personIntent.putExtras(personBundle)

        val personTextID = if (isTeacher) {
            R.string.classes
        }
        else {
            R.string.teacherList
        }

        val personShortcut = ShortcutInfo.Builder(this, "teacherOrClassList")
            .setShortLabel(getString(personTextID))
            .setIcon(Icon.createWithResource(this, R.drawable.ic_person_shortcut_icon))
            .setIntent(personIntent)
            .build()

        shortcutManager?.addDynamicShortcuts(listOf(personShortcut))

        // room shortcut
        if (isTeacher) {
            val roomIntent = Intent(Intent.ACTION_VIEW, Uri.EMPTY, this, MainActivity::class.java)
            val roomBundle = Bundle()
            roomBundle.putInt("tabNumber", 4)
            roomIntent.putExtras(roomBundle)

            val roomShortcut = ShortcutInfo.Builder(this, "room")
                .setShortLabel(getString(R.string.roomList))
                .setIcon(Icon.createWithResource(this, R.drawable.ic_room_shortcut_icon))
                .setIntent(roomIntent)
                .build()

            shortcutManager?.addDynamicShortcuts(listOf(roomShortcut))
        }

        // news shortcut
        val newsIntent = Intent(Intent.ACTION_VIEW, Uri.EMPTY, this, MainActivity::class.java)
        val newsBundle = Bundle()
        newsBundle.putInt("tabNumber", 5)
        newsIntent.putExtras(newsBundle)

        val newsShortcut = ShortcutInfo.Builder(this, "news")
            .setShortLabel(getString(R.string.news))
            .setIcon(Icon.createWithResource(this, R.drawable.ic_news_shortcut_icon))
            .setIntent(newsIntent)
            .build()

        shortcutManager?.addDynamicShortcuts(listOf(newsShortcut))
    }
}

