package de.bkukr.bku.general

import android.content.Intent
import android.os.Bundle
import android.util.Log
import androidx.preference.PreferenceManager
import com.google.firebase.messaging.FirebaseMessagingService
import com.google.firebase.messaging.RemoteMessage
import de.bkukr.bku.handler.NotificationHandler
import de.bkukr.bku.network.NetworkHandler

// Copyright (C) 2020  Sven Op de Hipt
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

/**
 * The service which handles notifications when the app is in foreground.
 *
 * @author Sven Op de Hipt
 */
class FirebaseNotificationService : FirebaseMessagingService() {
    /**
     * Is called by firebase when a new push notification is retrieved and the app is on foreground.
     *
     * @param [remoteMessage] the message, which was received
     */
    override fun onMessageReceived(remoteMessage: RemoteMessage) {
        Log.d("Message", "From: " + remoteMessage.from)

        val preferences = PreferenceManager.getDefaultSharedPreferences(baseContext)

        Log.d("Message", "Message data payload: " + remoteMessage.data)

        // Check if message contains a notification payload.
        if (preferences.getBoolean("notificationRepresentative", true)) {
            handleNow(remoteMessage)

            Log.d("Message", "Message Notification Body: " + remoteMessage.notification?.body)
        }
    }

    /**
     * Called if InstanceID token is updated. This may occur if the security of the previous token had
     * been compromised. The method is also called when the token is created for the first time.
     *
     * @param [token] the new token
     */
    override fun onNewToken(token: String) {
        Log.d("Token", "Refreshed token: $token")

        sendRegistrationToServer(token)
    }

    /**
     * Handles an incoming remote [message].
     * @param message the incoming remote message
     */
    private fun handleNow(message: RemoteMessage) {
        val messageNotification = message.notification

        val id = message.data["id"]
        val idInt = id?.toIntOrNull()
        val type = message.data["type"]

        if (messageNotification != null && id != null && idInt != null && type != null) {
            val data = message.data

            val intent = Intent(this, MainActivity::class.java)
            intent.flags = Intent.FLAG_ACTIVITY_NEW_TASK or Intent.FLAG_ACTIVITY_CLEAR_TASK or Intent.FLAG_ACTIVITY_SINGLE_TOP
            intent.action = data["type"]

            // convert the map to a bundle
            val bundle = Bundle()
            for ((key, value) in data) {
                bundle.putString(key, value)
            }

            intent.putExtras(bundle)

            // convert message notification to notification and send it
            val notification = NotificationHandler.getNotification(this, intent, messageNotification.title ?: "",
                idInt, messageNotification.body ?: "", bundle)
            NotificationHandler.sendNotification(this, notification)
        }
    }

    /**
     * Sends the new InstanceID [token] to the server.
     * @param [token] the new InstanceID token
     */
    private fun sendRegistrationToServer(token: String) {
        if (baseContext.isLoggedIn) {
            val preferences = getSharedPreferences("token", MODE_PRIVATE)
            val oldToken = preferences.getString("token", null) ?: ""

            val params = mutableMapOf("oldToken" to oldToken, "newToken" to token)
            NetworkHandler().execute(applicationContext, 3, params).get()

            val tokenPrefEdit = preferences.edit()
            tokenPrefEdit.putString("token", token)
            tokenPrefEdit.apply()
        }
    }
}
