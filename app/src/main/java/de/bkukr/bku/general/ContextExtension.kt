package de.bkukr.bku.general

import android.content.Context
import android.content.Intent
import android.util.TypedValue
import android.widget.Toast
import androidx.annotation.AttrRes
import androidx.preference.PreferenceManager
import de.bkukr.bku.R

// Copyright (C) 2020  Sven Op de Hipt
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

/**
 * Converts an [attrColor] to a Color
 * @param [attrColor] the color attribute which will be converted
 * @param [typedValue] the typed value where the color will be saved in
 * @param [resolveRefs] if true, resource references will be walked. otherwise the returned value could be a type reference
 * @return the converted color
 */
fun Context.getColorFromAttr(@AttrRes attrColor: Int, typedValue: TypedValue = TypedValue(), resolveRefs: Boolean = true): Int {
    theme.resolveAttribute(attrColor, typedValue, resolveRefs)
    return typedValue.data
}

/**
 * The username of the currently logged in user.
 */
val Context.username: String get() =
    PreferenceManager.getDefaultSharedPreferences(this).getString("username", null) ?: ""

/**
 * If a is user is currently logged in.
 */
val Context.isLoggedIn: Boolean get() =
    PreferenceManager.getDefaultSharedPreferences(this).getBoolean("loggedIn", false)

/**
 * Is the currently logged in user is a tg student.
 */
val Context.isTGStudent get() = username.startsWith("tg", true)

/**
 * If the currently logged in user is a teacher.
 */
val Context.isTeacher get() = username.startsWith("l-", true)

/**
 * Opens an email view with the [emailAddresses] as recipients.
 * @param [emailAddresses] the email addresses which should be written to
 */
fun Context.writeEmail(emailAddresses: Array<String>) {
    val email = Intent(Intent.ACTION_SEND)
    email.type = "message/rfc822"
    email.putExtra(Intent.EXTRA_EMAIL, emailAddresses)
    try {
        startActivity(email)
    } catch (ex: android.content.ActivityNotFoundException) {
        Toast.makeText(this, R.string.noEmailClients, Toast.LENGTH_SHORT).show()
    }
}