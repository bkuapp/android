package de.bkukr.bku.general

import android.content.Intent
import android.content.SharedPreferences
import android.net.Uri
import android.os.Build
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.app.AppCompatDelegate
import androidx.fragment.app.Fragment
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleObserver
import androidx.lifecycle.OnLifecycleEvent
import androidx.lifecycle.ProcessLifecycleOwner
import androidx.preference.PreferenceManager
import de.bkukr.bku.R
import de.bkukr.bku.exam.ExamDetailFragment
import de.bkukr.bku.exam.ExamFragment
import de.bkukr.bku.exam.ExamQuarter
import de.bkukr.bku.handler.ImageHandler
import de.bkukr.bku.handler.MenuHandler
import de.bkukr.bku.handler.NotificationHandler
import de.bkukr.bku.network.JSONHandlerPhone
import de.bkukr.bku.news.NewsFragment
import de.bkukr.bku.person.Person
import de.bkukr.bku.person.PersonFragment
import de.bkukr.bku.person.PersonListFragment
import de.bkukr.bku.representation.DetailRepresentationFragment
import de.bkukr.bku.representation.Representation
import de.bkukr.bku.representation.RepresentationFragment
import de.bkukr.bku.room.Room
import de.bkukr.bku.room.RoomDetailFragment
import de.bkukr.bku.room.RoomListFragment
import de.bkukr.bku.schedule.CourseFragment
import de.bkukr.bku.schedule.ScheduleFragment
import de.bkukr.bku.schedule.ScheduleWeek
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.serialization.builtins.ListSerializer
import kotlinx.serialization.builtins.list
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.HashMap

// Copyright (C) 2020  Sven Op de Hipt
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

/**
 * The main activity which hosts all fragments which show the information to the user.
 *
 * @author Sven Op de Hipt
 */
class MainActivity : AppCompatActivity(), SharedPreferences.OnSharedPreferenceChangeListener, LifecycleObserver {
    /**
     * The fragments which were shown previously.
     */
    private val previousFragments = Stack<Fragment>()
    /**
     * The current representation image which can be shared.
     */
    private var representationImage: Uri? = null
    /**
     * The schedule fragment which was shown last.
     */
    private var lastScheduleFragment: Fragment? = null
    /**
     * The representation fragment which was shown last.
     */
    private var lastRepresentationFragment: Fragment? = null
    /**
     * The person fragment which was shown last.
     */
    private var lastPersonFragment: Fragment? = null
    /**
     * The exam fragment which was shown last.
     */
    private var lastExamFragment: Fragment? = null

    /**
     * The room fragment which was shown last.
     */
    private var lastRoomFragment: Fragment? = null
    /**
     * The news fragment which exists only once.
     */
    private var newsFragment = NewsFragment()

    /**
     * The room tab id which is need because the room tab needs to be added afterwards.
     */
    private val roomTabID = Menu.FIRST + 3

    /**
     * The current schedule data which will be shown in the schedule tab.
     */
    var scheduleData: ScheduleWeek? = null
    /**
     * The current representation data which will be shown in the representation tab.
     */
    var representationData: List<List<Representation>>? = null
    /**
     * The current exam data which will be shown in the exam tab.
     */
    var examData: List<ExamQuarter>? = null
    /**
     * The current person data which will be shown in the person tab.
     */
    var personData: List<List<Person>>? = null
    /**
     * The current room data which will be shown in the room tab.
     */
    var roomData: List<Room>? = null

    /**
     * true if a different teacher is picked.
     */
    var differentTeacher = false

    /**
     * The id of the currently picked teacher.
     */
    var currentTeacherID = 0
        set(value) {
            field = value

            if (differentTeacher) {
                // reload the schedule data
                loadScheduleData {
                    val scheduleData = scheduleData
                    if (scheduleData != null) {
                        (lastScheduleFragment as? ScheduleFragment)?.scheduleData = scheduleData
                    }
                }
            }
        }

    /**
     * Indicates if the loading circle is shown at the moment.
     */
    var loadingCircleShown = true
        set(value) {
            field = value

            loadingPanel.visibility = if (value) {
                View.VISIBLE
            }
            else {
                View.GONE
            }
        }

    /**
     * Is true if there is no schedule data loading currently.
     */
    private var scheduleDataLoaded = false
        set(value) {
            field = value

            if (getPreviousFragment() is ScheduleFragment) {
                loadingCircleShown = !value
            }
        }

    /**
     * Is true if there is no representation data loading currently.
     */
    private var representationDataLoaded = false
        set(value) {
            field = value

            if (getPreviousFragment() is RepresentationFragment) {
                loadingCircleShown = !value
            }
        }

    /**
     * Is true if there is no exam data loading currently.
     */
    private var examDataLoaded = false
        set(value) {
            field = value

            if (getPreviousFragment() is ExamFragment) {
                loadingCircleShown = !value
            }
        }

    /**
     * Is true if there is no person data loading currently.
     */
    private var personDataLoaded = false
        set(value) {
            field = value

            if (getPreviousFragment() is PersonListFragment) {
                loadingCircleShown = !value
            }
        }

    /**
     * Is true if there is no room data loading currently.
     */
    private var roomDataLoaded = false
        set(value) {
            field = value

            if (getPreviousFragment() is RoomListFragment) {
                loadingCircleShown = !value
            }
        }

    /**
     * The currently shown schedule week.
     */
    private var scheduleWeek = 0

    /**
     * Is called when the app starts again after it was in the background.
     */
    @OnLifecycleEvent(Lifecycle.Event.ON_START)
    fun onMoveToForeground() {
        loadData()
    }

    /**
     * Is called by the system when the activity is created. Initializes the fragments and the tab bar.
     * Shows the schedule fragment by default.
     *
     * @param [savedInstanceState] the saved instance state
     */
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        // set the dark mode corresponding to the system settings (API 29, Android 10) or to the local settings
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.Q) {
            AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_FOLLOW_SYSTEM)
        }
        else {
            val general = PreferenceManager.getDefaultSharedPreferences(this)
            val darkTheme = general.getBoolean("darkMode", true)
            if (darkTheme) {
                AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_YES)
            } else {
                AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO)
            }

            general.registerOnSharedPreferenceChangeListener(this)
        }

        setContentView(R.layout.activity_main)

        // update the tab bar corresponding to the user type
        if (!isTGStudent) {
            tabbar.menu.removeItem(R.id.exam_tabbar)
        }
        if (isTeacher) {
            val item = tabbar.menu.add(0, roomTabID, 5, "")
            item.icon = getDrawable(R.drawable.ic_room_icon)
        }

        // update the fragments, data and title when a tab is selected
        tabbar.setOnNavigationItemSelectedListener { menuItem ->
            if (previousFragments.isNotEmpty()) {
                when (menuItem.itemId) {
                    R.id.schedule_tabbar -> if (previousFragments.peek() !is ScheduleFragment) {
                        scheduleWeek = 0
                        setFragment(lastScheduleFragment)
                        title = getString(R.string.schedule)
                        loadScheduleData {
                            val scheduleData = scheduleData
                            if (scheduleData != null) {
                                (lastScheduleFragment as? ScheduleFragment)?.scheduleData = scheduleData
                            }
                        }
                    }
                    R.id.representation_tabbar -> if (previousFragments.peek() !is RepresentationFragment) {
                        setFragment(lastRepresentationFragment)
                        title = getString(
                            R.string.dateInformation,
                            SimpleDateFormat(
                                "dd.MM.",
                                Locale.getDefault()
                            ).format(Calendar.getInstance().time)
                        )
                    }
                    R.id.exam_tabbar -> if (previousFragments.peek() !is ExamFragment) {
                        setFragment(lastExamFragment)
                        title = getString(R.string.examPlan)
                    }
                    R.id.person_tabbar -> if (previousFragments.peek() !is PersonListFragment) {
                        setFragment(lastPersonFragment)
                        title = if (isTeacher) {
                            getString(R.string.persons)
                        } else {
                            getString(R.string.teacherList)
                        }
                    }
                    roomTabID -> if (previousFragments.peek() !is RoomListFragment) {
                        setFragment(lastRoomFragment)
                        title = getString(R.string.roomList)
                    }
                    R.id.news_tabbar -> {
                        setFragment(newsFragment)
                        title = getString(R.string.news)
                    }
                }
            }
            true
        }

        // pass the intent to the on intent received method
        onIntentReceived(intent)

        ProcessLifecycleOwner.get().lifecycle.addObserver(this)
    }

    /**
     * Is called by the system if a new [intent] is send to this activity.
     *
     * @param [intent] the new intent
     */
    override fun onNewIntent(intent: Intent?) {
        super.onNewIntent(intent)

        onIntentReceived(intent)
    }

    /**
     * Is called by the system if the [menu] should be configured. The [menu] is being configured corresponding to the
     * current shown fragment.
     *
     * @param [menu] the menu that should be configured
     *
     * @return true if the menu should be displayed.
     */
    override fun onCreateOptionsMenu(menu: Menu): Boolean {
        if (previousFragments.isNotEmpty()) {
            when (previousFragments.peek()) {
                is DetailRepresentationFragment -> menuInflater.inflate(R.menu.share_menu, menu)
                is NewsFragment -> {
                    if (newsFragment.showBack) {
                        menuInflater.inflate(R.menu.menu_main, menu)
                    } else {
                        menuInflater.inflate(R.menu.menu_news, menu)
                        menu.findItem(R.id.news_backward_icon).isEnabled = newsFragment.canGoBack

                        if (!newsFragment.canGoBack) {
                            menu.findItem(R.id.news_backward_icon).icon.alpha = 90
                        } else {
                            menu.findItem(R.id.news_backward_icon).icon.alpha = 255
                        }

                        menu.findItem(R.id.news_forward_icon).isEnabled = newsFragment.canGoForward

                        if (!newsFragment.canGoForward) {
                            menu.findItem(R.id.news_forward_icon).icon.alpha = 90
                        } else {
                            menu.findItem(R.id.news_forward_icon).icon.alpha = 255
                        }
                    }
                }
                else -> menuInflater.inflate(R.menu.menu_main, menu)
            }
        }
        else {
            menuInflater.inflate(R.menu.menu_main, menu)
        }
        return true
    }

    /**
     * Handles clicks on a menu [item].
     *
     * @param [item] the menu which is being clicked
     *
     * @return true if the event should not be handled by the system.
     */
    override fun onOptionsItemSelected(item: MenuItem) =
        when (item.itemId) {
            android.R.id.home -> {
                back()
                true
            }
            R.id.share -> {
                shareRepresentationImage()
                true
            }
            R.id.news_backward_icon -> {
                newsFragment.navigateBack()
                true
            }
            R.id.news_forward_icon -> {
                newsFragment.navigateForward()
                true
            }
            R.id.refresh -> {
                loadData()
                true
            }
            else -> MenuHandler.onOptionsItemSelected(item, this)
        }

    /**
     * Handles clicks on the back button.
     */
    override fun onBackPressed() {
        if (!back()) {
            setResult(0)
            finish()
        }
    }

    /**
     * Is being called if the [sharedPreferences] are changed. Handles dark mode changes for android 9 or lower.
     *
     * @param [sharedPreferences] the shared preferences which have changed
     * @param [key] the key of the changed value
     */
    override fun onSharedPreferenceChanged(sharedPreferences: SharedPreferences, key: String) {
        if (key == "darkMode") {
            val darkTheme = sharedPreferences.getBoolean("darkMode", true)
            if (darkTheme) {
                AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_YES)
            } else {
                AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO)
            }
        }
    }

    /**
     * Shows a detail fragment (e.g. the detail schedule view).
     *
     * @param [data] the data, which should be transferred to the fragment
     * @param [destination] the fragment, which will be shown
     */
    fun showDetails(data: HashMap<String, String>, destination: Fragment) {
        val extra = Bundle()
        extra.putSerializable("data", data)
        destination.arguments = extra
        setFragment(destination)
    }

    /**
     * Sets the [representationImage] to share it later.
     *
     * @param [representationImage] The new representation image.
     */
    fun setRepresentationImage(representationImage: Uri?) {
        this.representationImage = representationImage
    }

    /**
     * Adds [weeks] to the current schedule week (e.g. by swiping).
     *
     * @param [weeks] The weeks to be added. Can be negative.
     * @param [onFinish] The method, which will be called when the schedule data is updated.
     */
    fun updateScheduleWeekBy(weeks: Int, onFinish: () -> Unit) {
        scheduleWeek += weeks
        loadScheduleData(onFinish)
    }

    /**
     * Sets the [newFragment] as the current fragment and makes it visible to the user.
     *
     * @param [newFragment] the new fragment which should be shown to the user
     */
    private fun setFragment(newFragment: Fragment?) {
        // show the back button (top left) if the fragment is a detail fragment
        val displayHomeAsUpEnabled = newFragment is CourseFragment || newFragment is DetailRepresentationFragment ||
            newFragment is ExamDetailFragment || newFragment is PersonFragment || newFragment is RoomDetailFragment
        supportActionBar?.setDisplayHomeAsUpEnabled(displayHomeAsUpEnabled)

        if (newFragment != null) {
            supportFragmentManager.beginTransaction().replace(R.id.mainFragment, newFragment).commitAllowingStateLoss()
            previousFragments.add(newFragment)

            // reload the menu
            invalidateOptionsMenu()

            if (newFragment is ScheduleFragment || newFragment is CourseFragment) {
                lastScheduleFragment = newFragment
            } else if (newFragment is RepresentationFragment && newFragment is DetailRepresentationFragment) {
                lastRepresentationFragment = newFragment
            } else if (newFragment is PersonListFragment || newFragment is PersonFragment) {
                lastPersonFragment = newFragment
            } else if (newFragment is ExamFragment || newFragment is ExamDetailFragment) {
                lastExamFragment = newFragment
            }
            else if (newFragment is RoomListFragment || newFragment is RoomDetailFragment) {
                lastRoomFragment = newFragment
            }

            // shows the loading circle corresponding to the new fragment and the loading state
            when (newFragment) {
                is ScheduleFragment -> {
                    loadingCircleShown = !scheduleDataLoaded
                }
                is RepresentationFragment -> {
                    loadingCircleShown = !representationDataLoaded
                }
                is ExamFragment -> {
                    loadingCircleShown = !examDataLoaded
                }
                is PersonListFragment -> {
                    loadingCircleShown = !personDataLoaded
                }
                is RoomListFragment -> {
                    loadingCircleShown = !roomDataLoaded
                }
                is NewsFragment -> {
                    loadingCircleShown = false
                }
            }
        }
    }

    /**
     * Handles the back event via the back button in the top left or the back button in the bottom bar.
     *
     * @return true, if the method did something which happens when a detail view was shown.
     */
    private fun back() =
        when (previousFragments.pop()) {
            is CourseFragment -> {
                setFragment(ScheduleFragment())
                title = getString(R.string.schedule)
                true
            }
            is DetailRepresentationFragment -> {
                setFragment(RepresentationFragment())
                true
            }
            is ExamDetailFragment -> {
                setFragment(ExamFragment())
                title = getString(R.string.examPlan)
                true
            }
            is PersonFragment -> {
                setFragment(PersonListFragment())

                title = if (isTeacher) {
                    getString(R.string.persons)
                } else {
                    getString(R.string.teacherList)
                }

                true
            }
            is RoomDetailFragment -> {
                setFragment(RoomListFragment())
                title = getString(R.string.roomList)
                true
            }
            is NewsFragment -> {
                if (newsFragment.showBack) {
                    newsFragment.goBack()
                    previousFragments.push(newsFragment)
                    true
                }
                else {
                    previousFragments.push(newsFragment)
                    false
                }
            }
            else -> false
    }

    /**
     * Shares the current representation image.
     */
    private fun shareRepresentationImage() {
        val representationImage = representationImage

        if (representationImage != null) {
            ImageHandler.shareImage(this, representationImage)
        }
    }

    /**
     * Handles a new [intent] which is received by the activity
     *
     * @param intent the intent which is received by the activity
     */
    private fun onIntentReceived(intent: Intent?) {
        if (intent != null) {
            val extras = intent.extras
            if (extras != null) {
                var tabNumber = extras.getInt("tabNumber", 0)

                val containsID = extras.containsKey("id")
                if (containsID) {
                    val tabType = extras.getString("type", "")

                    if (tabType == "schedule") {
                        tabNumber = 0
                    }

                    if (tabType == "representation") {
                        tabNumber = 1
                    }

                    if (tabType == "exam") {
                        tabNumber = 2
                    }
                }

                // sets up the fragments corresponding to the tab number
                when (tabNumber) {
                    0 -> {
                        defaultSetup()
                        tabbar.selectedItemId = R.id.schedule_tabbar

                        if (containsID) {
                            val params = HashMap<String, String>()

                            params["id"] = extras.getInt("id", -1).toString()

                            showDetails(params, CourseFragment())
                        }
                    }
                    1 -> {
                        setFragment(RepresentationFragment())
                        lastScheduleFragment = ScheduleFragment()
                        lastPersonFragment = PersonListFragment()
                        lastExamFragment = ExamFragment()
                        lastRoomFragment = RoomListFragment()

                        if (containsID) {
                            val params = HashMap<String, String>()

                            params["id"] = extras.getString("id", "-1")

                            showDetails(params, DetailRepresentationFragment())
                        }
                    }
                    2 -> {
                        setFragment(ExamFragment())
                        lastScheduleFragment = ScheduleFragment()
                        lastRepresentationFragment = RepresentationFragment()
                        lastPersonFragment = PersonListFragment()
                        lastRoomFragment = RoomListFragment()
                        title = getString(R.string.examPlan)
                        tabbar.selectedItemId = R.id.exam_tabbar

                        if (containsID) {
                            val params = HashMap<String, String>()
                            params["id"] = extras.getInt("id", -1).toString()
                            showDetails(params, ExamDetailFragment())
                        }
                    }
                    3 -> {
                        setFragment(PersonListFragment())
                        lastScheduleFragment = ScheduleFragment()
                        lastRepresentationFragment = RepresentationFragment()
                        lastExamFragment = ExamFragment()
                        lastRoomFragment = RoomListFragment()

                        title = if (isTeacher) {
                            getString(R.string.persons)
                        } else {
                            getString(R.string.teacherList)
                        }

                        tabbar.selectedItemId = R.id.person_tabbar
                    }
                    4 -> {
                        setFragment(RoomListFragment())
                        lastScheduleFragment = ScheduleFragment()
                        lastRepresentationFragment = RepresentationFragment()
                        lastPersonFragment = PersonListFragment()
                        lastExamFragment = ExamFragment()
                        title = getString(R.string.roomList)
                        tabbar.selectedItemId = roomTabID
                    }
                    5 -> {
                        setFragment(newsFragment)
                        lastScheduleFragment = ScheduleFragment()
                        lastRepresentationFragment = RepresentationFragment()
                        lastPersonFragment = PersonListFragment()
                        lastExamFragment = ExamFragment()
                        lastRoomFragment = RoomListFragment()
                        title = getString(R.string.news)
                        tabbar.selectedItemId = R.id.news_tabbar
                    }
                }
            } else {
                defaultSetup()
            }
        } else {
            defaultSetup()
        }
    }

    /**
     * The default setup when the schedule tab or no tab should be selected when a new intent is received.
     */
    private fun defaultSetup() {
        setFragment(ScheduleFragment())
        title = getString(R.string.schedule)
        lastRepresentationFragment = RepresentationFragment()
        lastExamFragment = ExamFragment()
        lastPersonFragment = PersonListFragment()
        lastRoomFragment = RoomListFragment()
    }

    /**
     * Reloads the current Fragment when new data is loaded.
     */
    private fun reloadFragment() {
        val fragment = getPreviousFragment()

        if (fragment != null) {
            val ft = supportFragmentManager.beginTransaction()
            ft.detach(fragment)
            ft.attach(fragment)
            ft.commitAllowingStateLoss()
        }
    }

    /**
     * Returns the previous fragment or null if there are no previous fragments
     *
     * @return the previous fragment or null
     */
    private fun getPreviousFragment() =
        if (previousFragments.isNotEmpty()) {
            previousFragments.peek()
        }
        else {
            null
        }

    /**
     * (Re)loads the main (non-detail) data from the server and shows the loading circle.
     */
    private fun loadData() {
        differentTeacher = false
        currentTeacherID = 0

        val general = PreferenceManager.getDefaultSharedPreferences(this)

        // schedule data
        scheduleWeek = 0

        loadScheduleData {
            if (getPreviousFragment() is ScheduleFragment) {
                reloadFragment()
            }
        }

        // representation data
        representationDataLoaded = false
        val representationSerializer = ListSerializer(ListSerializer(Representation.serializer()))
        JSONHandlerPhone.getJSON(this, 101, representationSerializer) { representationData ->
            this.representationData = representationData
            this.representationDataLoaded = true

            if (getPreviousFragment() is RepresentationFragment) {
                reloadFragment()
            }
        }

        // person data
        personDataLoaded = false
        val personSerializer = ListSerializer(ListSerializer(Person.serializer()))
        JSONHandlerPhone.getJSON(this, 5, personSerializer) { personData ->
            this.personData = personData
            this.personDataLoaded = true

            if (getPreviousFragment() is ExamFragment) {
                reloadFragment()
            }
        }

        // exam data
        if (isTGStudent) {
            examDataLoaded = false
            val examSerializer = ListSerializer(ExamQuarter.serializer())
            JSONHandlerPhone.getJSON(this, 103, examSerializer) { examData ->
                this.examDataLoaded = true
                this.examData = examData

                if (getPreviousFragment() is PersonListFragment) {
                    reloadFragment()
                }

                // exam notifications
                if (general.getBoolean("notificationExam", true)) {
                    NotificationHandler.setupExamNotification(this)
                }
            }
        }

        // room data
        if (isTeacher) {
            roomDataLoaded = false
            val roomSerializer = ListSerializer(Room.serializer())
            JSONHandlerPhone.getJSON(this, 110, roomSerializer) { roomData ->
                this.roomDataLoaded = true
                this.roomData = roomData

                if (getPreviousFragment() is RoomListFragment) {
                    reloadFragment()
                }
            }
        }
    }

    /**
     * Re(loads) the schedule data from the server when the whole data is loaded or the schedule week has changed.
     *
     * @param [onFinish] The method, which will be called when the schedule data is updated.
     */
    private fun loadScheduleData(onFinish: () -> Unit) {
        val scheduleSerializer = ScheduleWeek.serializer()
        val scheduleParams = mutableMapOf("inWeeks" to scheduleWeek.toString())

        // changes the request type if an other teacher is selected
        val type = if (differentTeacher) {
            scheduleParams["teacherID"] = currentTeacherID.toString()
            109
        }
        else {
            100
        }

        scheduleDataLoaded = false
        JSONHandlerPhone.getJSON(this, type, scheduleSerializer, scheduleParams) { scheduleData ->
            this.scheduleData = scheduleData
            this.scheduleDataLoaded = true
            onFinish()
        }
    }
}
