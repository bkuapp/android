package de.bkukr.bku.general

import androidx.preference.PreferenceManager
import com.google.android.gms.wearable.MessageEvent
import com.google.android.gms.wearable.Wearable
import com.google.android.gms.wearable.WearableListenerService
import de.bkukr.bku.R
import de.bkukr.bku.handler.JWTHandler
import org.json.JSONArray

// Copyright (C) 2020  Sven Op de Hipt
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

/**
 * The messaging service which sends data to the smart watch or retrieves data from it.
 *
 * @author Sven Op de Hipt
 */
class WatchMessageService : WearableListenerService() {
    /**
     * Is called by the system if the watch sends an message to the phone. Answers the message corresponding to the path.
     *
     * @param messageEvent the event which contains the message and the path
     */
    override fun onMessageReceived(messageEvent: MessageEvent?) {
        super.onMessageReceived(messageEvent)

        val path = messageEvent?.path

        val preferences = PreferenceManager.getDefaultSharedPreferences(baseContext)
        val jwt = preferences.getString("jwt", null)
        val jwtPayload = JWTHandler.decodeJWT(jwt)

        if (messageEvent != null && path == "loggedIn") {
            if (jwt != null && jwtPayload != null && jwtPayload.exp > System.currentTimeMillis() / 1000) {
                val loggedIn = baseContext.isLoggedIn
                val username = baseContext.username

                if (username != "") {
                    // Convert the values to a json string and send is as byte array.
                    val data = JSONArray(arrayOf(loggedIn, username, jwt))
                    Wearable.getMessageClient(baseContext)
                        .sendMessage(messageEvent.sourceNodeId, path, data.toString().toByteArray())
                }
            }
            else {
                // Convert the error message to a json array and send is as byte array.
                val data = JSONArray(arrayOf(getString(R.string.yourSessionOnPhoneHasExpiredToo)))
                Wearable.getMessageClient(baseContext)
                    .sendMessage(messageEvent.sourceNodeId, path, data.toString().toByteArray())
            }
        } else {
            super.onMessageReceived(messageEvent)
        }
    }
}
