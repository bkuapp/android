package de.bkukr.bku.news

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.webkit.WebResourceRequest
import android.webkit.WebView
import android.webkit.WebViewClient
import androidx.fragment.app.Fragment
import de.bkukr.bku.R
import de.bkukr.bku.general.MainActivity
import kotlinx.android.synthetic.main.fragment_news.*

// Copyright (C) 2020  Sven Op de Hipt
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

/**
 * Shows the [news website of bkukr.de](https://bkukr.de/?id=5) to the user.
 *
 * @author Sven Op de Hipt
 */
class NewsFragment : Fragment() {
    var canGoForward = true
    var canGoBack = false
    var showBack = false

    /**
     * Is called when the view is created by the system. Loads the website and removes the headers and footers
     * of the website.
     *
     * @param [inflater] the layout inflater which converts the layout file to a view
     * @param [container] the view group which will contain the view
     * @param [savedInstanceState] the saved instance states for the activity
     *
     * @return the created view
     */
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle? ): View? {
        val view = inflater.inflate(R.layout.fragment_news, container, false)

        val newsWebView: WebView = view.findViewById(R.id.newsWebView)
        newsWebView.settings.javaScriptEnabled = true
        newsWebView.webViewClient = (object : WebViewClient() {
            override fun onPageFinished(view: WebView?, url: String?) {
                super.onPageFinished(view, url)

                // hide the cookie header, other headers and the footers
                newsWebView.evaluateJavascript("const headers = document.getElementsByClassName('cs-header-top page-top');" +
                        "if (headers.length != 0) headers[0].remove();" +
                        "const logoHeader = document.getElementsByClassName('main-header bg-top page-top bg-color');" +
                        "if (logoHeader.length != 0) logoHeader[0].remove();" +
                        "const breadcrumb = document.getElementsByClassName('breadcrumb');" +
                        "if (breadcrumb.length != 0) breadcrumb[0].remove();" +
                        "const footer = document.getElementsByClassName('cs-footer-area cs-default-overlay animatedParent animateOnce');" +
                        "if (footer.length != 0) footer[0].remove();" +
                        "const copyrightFooter = document.getElementsByClassName('cs-copyright-area');" +
                        "if (copyrightFooter.length != 0) copyrightFooter[0].remove();" +
                        "const title = document.getElementsByTagName('header');" +
                        "if (title.length != 0) title[0].remove();" +
                        "const cookieHeader = document.getElementById('cc-notification');" +
                        "if (cookieHeader != null) cookieHeader.remove();" +
                        "const privacySettings = document.getElementById('cc-tag');" +
                        "if (privacySettings != null) privacySettings.remove();", null)

                if (url != null) {
                    val uri = Uri.parse(url)
                    val id = uri.getQueryParameter("id")

                    if (id != null) {
                        // show the back button when a detail website is shown.
                        showBack = id == "36"
                        (activity as? MainActivity)?.supportActionBar?.setDisplayHomeAsUpEnabled(showBack)
                    }
                }

                // check if the user can go forward and backward and recreate the menu after it
                newsWebView.evaluateJavascript("document.getElementsByClassName('previous').length != 0") { canGoBack ->
                    if (canGoBack != null) {
                        this@NewsFragment.canGoBack = canGoBack.toBoolean()
                    }

                    newsWebView.evaluateJavascript("document.getElementsByClassName('next').length != 0") { canGoForward ->
                        if (canGoForward != null) {
                            this@NewsFragment.canGoForward = canGoForward.toBoolean()

                            activity?.invalidateOptionsMenu()
                        }
                    }
                }
            }

            // check if the url is inside of the news section. otherwise open it in browser.
            override fun shouldOverrideUrlLoading(view: WebView?, request: WebResourceRequest?): Boolean {
                val url = request?.url
                val id = url?.getQueryParameter("id")

                if (url != null) {
                    if (id == "5" || id == "36") {
                        view?.loadUrl(url.toString())
                    } else {
                        val browserIntent = Intent(Intent.ACTION_VIEW, url)
                        startActivity(browserIntent)
                    }
                }

                return true
            }
        })
        newsWebView.loadUrl("https://bkukr.de/?id=5")

        return view
    }

    /**
     * Navigate back when the user tabs on the back button in the menu.
     */
    fun navigateBack() {
        if (canGoBack && newsWebView != null) {
            newsWebView.evaluateJavascript("document.getElementsByClassName('previous')[0].children[0].click()",null)
        }
    }

    /**
     * Navigate forward when the user tabs on the back button in the menu.
     */
    fun navigateForward() {
        if (canGoForward && newsWebView != null) {
            newsWebView.evaluateJavascript("document.getElementsByClassName('next')[0].children[0].click()",null)
        }
    }
    /**
     * Go back from a detail website if the user tabs on of the back buttons (top or bottom one).
     */
    fun goBack() {
        if (newsWebView != null) {
            newsWebView.goBack()
        }
    }
}
