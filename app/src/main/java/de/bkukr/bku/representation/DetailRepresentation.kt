package de.bkukr.bku.representation

import kotlinx.serialization.Serializable

// Copyright (C) 2020  Sven Op de Hipt
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

/**
 * Represents a representation in the representation detail view with the given [date], the given [hours],
 * the given [nameOfClass], the given [course] name, the given [room], the given [representation] teacher,
 * the [teacher] which holds the lesson, the [representationRules] of the representation.
 *
 * @author Sven Op de Hipt
 *
 * @constructor Create a representation for the representation detail view with the given [date], the given [hours],
 * the given [nameOfClass], the given [course] name, the given [room], the given [representation] teacher,
 * the [teacher] which holds the lesson regularly, the [representationRules] of the representation.
 *
 * @param [date] the date of the representation
 * @param [hours] the hours of the representation
 * @param [nameOfClass] the name of the represented class
 * @param [course] the course name of the representation
 * @param [room] the room of the representation
 * @param [representation] the teacher which represents the course
 * @param [teacher] the teacher which holds the lesson regularly
 * @param [representationRules] the rules of the representation
 */
@Serializable
data class DetailRepresentation(val date: String, val hours: Array<String>, val nameOfClass: String,
                                val course: String, val room: String?, val representation: String,
                                val teacher: String, val representationRules: String) {
    /**
     * Returns true if the current is equal to the [other] object.
     *
     * @param [other] the object which should be compared
     *
     * @return true if the current is equal to the [other] object
     */
    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as DetailRepresentation

        if (date != other.date) return false
        if (!hours.contentEquals(other.hours)) return false
        if (nameOfClass != other.nameOfClass) return false
        if (course != other.course) return false
        if (room != other.room) return false
        if (representation != other.representation) return false
        if (teacher != other.teacher) return false
        if (representationRules != other.representationRules) return false

        return true
    }

    /**
     * Returns the hash code of the current object.
     *
     * @return the hash code of the current object
     */
    override fun hashCode(): Int {
        var result = date.hashCode()
        result = 31 * result + hours.contentHashCode()
        result = 31 * result + nameOfClass.hashCode()
        result = 31 * result + course.hashCode()
        result = 31 * result + (room?.hashCode() ?: 0)
        result = 31 * result + representation.hashCode()
        result = 31 * result + teacher.hashCode()
        result = 31 * result + representationRules.hashCode()
        return result
    }
}
