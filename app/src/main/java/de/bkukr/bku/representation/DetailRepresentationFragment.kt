package de.bkukr.bku.representation

import android.content.res.Configuration
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import de.bkukr.bku.R
import de.bkukr.bku.general.MainActivity
import de.bkukr.bku.handler.ImageHandler
import de.bkukr.bku.network.JSONHandlerPhone
import kotlinx.android.synthetic.main.fragment_detail_representation.*

// Copyright (C) 2020  Sven Op de Hipt
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

/**
 * The fragment which shows the representation image.
 *
 * @author Sven Op de Hipt
 */
class DetailRepresentationFragment : Fragment() {
    /**
     * The representation information which will be converted to an image.
     */
    private var data: DetailRepresentation? = null

    /**
     * Is called by the system if the view needs to be created.
     *
     * @param [inflater] the layout inflater which converts the resource file to an view
     * @param [container] the view group which contains the view
     * @param [savedInstanceState] the saved instance state for this view
     *
     * @return the created view
     */
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? =
        inflater.inflate(R.layout.fragment_detail_representation, container, false)

    /**
     * Is called by the system when the activity is created or this view is created first.
     * Loads the representation data from the server and converts it to an image.
     *
     * @param [savedInstanceState] the saved instance states for the view
     */
    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        activity?.title = getString(R.string.representationPlan)

        val extras = arguments
        val currentActivity = activity

        if (extras != null && currentActivity != null) {
            val extraMap = extras.getSerializable("data") as? HashMap<String, String>
            if (extraMap != null) {
                (activity as? MainActivity)?.loadingCircleShown = true

                // loads the representation data from the server
                val serializer = DetailRepresentation.serializer()
                JSONHandlerPhone.getJSON(currentActivity, 102, serializer, extraMap) { data ->
                    this.data = data

                    createImage()
                    (activity as? MainActivity)?.loadingCircleShown = false
                }
            }
        }
    }

    /**
     * Is called by the system if the configuration has changed (e.g. if the device rotated).
     * Recreates the image then.
     *
     * @param [newConfig] the new configuration
     */
    override fun onConfigurationChanged(newConfig: Configuration) {
        super.onConfigurationChanged(newConfig)

        createImage()
    }

    /**
     * Converts the representation data to a representation image.
     */
    private fun createImage() {
        val currentData = data
        val currentActivity = activity

        if (currentActivity != null && currentData != null) {
            val content = ImageHandler.generateImage(currentActivity, currentData)
            detailedView.setImageBitmap(content)

            Thread(Runnable {
                (activity as? MainActivity)?.setRepresentationImage(ImageHandler.
                    saveImageInCache(currentActivity, content))
            }).start()
        }
    }
}
