package de.bkukr.bku.representation

import kotlinx.serialization.Serializable

// Copyright (C) 2020  Sven Op de Hipt
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

/**
 * Represents a representation in the representation list with the given [id], the given [date], the given course [name] and
 * the [represented] state, which is true when the representation is represented.
 * It is false, when the representation is canceled.
 *
 * @author Sven Op de Hipt
 *
 * @constructor Creates a representation for the representation list with the given [id], the given [date], the given course [name] and
 * the [represented] state, which is true when the representation is represented.
 * It is false, when the representation is canceled.
 *
 * @param [id] the id of the representation
 * @param [date] the date of the representation
 * @param [name] the course name of the representation
 * @param [represented] true if the representation is represented and false if it is cancelled
 */
@Serializable
data class Representation(val id: Int, val date: String, val name: String, val represented: Boolean) {
    /**
     * Returns a string which contains the name and the date of the representation.
     *
     * @return a string which contains the name and the date of the representation.
     */
    override fun toString() = "$name, $date"
}
