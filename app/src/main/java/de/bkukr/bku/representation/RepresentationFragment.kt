package de.bkukr.bku.representation

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import de.bkukr.bku.R
import de.bkukr.bku.general.MainActivity
import de.bkukr.bku.general.getColorFromAttr
import de.bkukr.bku.general.isTeacher
import de.bkukr.bku.list.*
import kotlinx.android.synthetic.main.fragment_representation.*

// Copyright (C) 2020  Sven Op de Hipt
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

/**
 * Shows the representation list to the user.
 *
 * @author Sven Op de Hipt
 */
class RepresentationFragment: Fragment() {
    /**
     * The data which stores the representations which are shown in the representation list.
     */
    private var representationData: List<List<Representation>>
        get() {
            val result = (activity as? MainActivity)?.representationData

            if (result != null) {
                return result
            }

            return emptyList()
        }
        set(representationData) {
            if (representationData.isNotEmpty()) {
                val representations = mutableListOf<ListItem>()

                if (context?.isTeacher == true) {
                    representations.add(ListHeaderItem(context?.getString(R.string.iRepresent) ?: ""))
                    for (representation in representationData.first()) {
                        representations.add(ListBodyItem(representation.toString(), representation.id,
                            DetailRepresentationFragment()))
                    }

                    representations.add(ListHeaderItem(context?.getString(R.string.iWillBeRepresentedOrInformed) ?: ""))
                    for (representation in representationData.last()) {
                        representations.add(ListBodyItem(representation.toString(), representation.id,
                            DetailRepresentationFragment()))
                    }
                }
                else {
                    for (representation in representationData.first()) {
                        val color = if (representation.represented) {
                            context?.getColorFromAttr(R.attr.green)
                        }
                        else {
                            context?.getColorFromAttr(R.attr.red)
                        }

                        representations.add(ListBodyItem(representation.toString(), representation.id,
                            DetailRepresentationFragment(), textColor = color))
                    }
                }

                val adapter = ListAdapter(activity, representations)
                representationTable.adapter = adapter
                representationTable.layoutManager = LinearLayoutManager(context)
                representationTable.addItemDecoration(ListItemDecorator(adapter))
                representationTable.addItemDecoration(DividerItemDecoration(context, DividerItemDecoration.VERTICAL))
            }
        }

    /**
     * Is called by the system if the view needs to be created.
     *
     * @param [inflater] the layout inflater which converts the resource file to a view
     * @param [container] the view group which contains the view
     * @param [savedInstanceState] the saved instance state for this view
     *
     * @return the created view
     */
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? =
        inflater.inflate(R.layout.fragment_representation, container, false)

    /**
     * Is called by the system when the activity is created or this view is created first.
     * Loads the representation data from the main activity and shows it in a list.
     *
     * @param [savedInstanceState] the saved instance states for the view
     */
    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        representationData = representationData
    }
}
