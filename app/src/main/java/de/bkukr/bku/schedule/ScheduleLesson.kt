package de.bkukr.bku.schedule

import kotlinx.serialization.Serializable

// Copyright (C) 2020  Sven Op de Hipt
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

/**
 * Represents a schedule lesson in the schedule fragment with the given [id], the [classOrTeacher] name,
 * the [start] of the lesson, the [end] ot the lesson, the [course] name of the lesson, the [roomOrTeacher] of the lesson,
 * the [represented] state of the lesson which is true, when the lesson is represented and
 * the [isCancelled] state of the lesson which is true, when the lesson is cancelled.
 *
 * @author Sven Op de Hipt
 *
 * @constructor Creates a schedule lesson in the schedule fragment with the given [id], the [classOrTeacher] name,
 * the [start] of the lesson, the [end] ot the lesson, the [course] name of the lesson, the [roomOrTeacher] of the lesson,
 * the [represented] state of the lesson which is true, when the lesson is represented and
 * the [isCancelled] state of the lesson which is true, when the lesson is cancelled.
 *
 * @param [id] the id of the lesson
 * @param [classOrTeacher] the class or the teacher name
 * @param [start] the start of the lesson
 * @param [end] the end of the lesson
 * @param [course] the course name of the lesson
 * @param [roomOrTeacher] the room or the teacher of the lesson
 * @param [represented] the represented state of the lesson which is true, when the lesson is represented
 * @param [isCancelled] the is cancelled state of the lesson which is true, when the lesson is cancelled
 */
@Serializable
data class ScheduleLesson(val id: Int, val classOrTeacher: String, val start: Int, val end: Int, val course: String,
                          val roomOrTeacher: String?, val represented: Boolean, val isCancelled: Boolean)