package de.bkukr.bku.schedule

import android.content.Context
import android.util.AttributeSet
import android.view.MotionEvent
import android.widget.ScrollView
import kotlin.math.abs

// Copyright (C) 2020  Sven Op de Hipt
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

/**
 * The schedule scroll view which shows the schedule table and handles the swipe events.
 *
 * @author Sven Op de Hipt
 *
 * @constructor Creates a schedule scroll view which shows the schedule table and handles the swipe events.
 *
 * @param [context] the context where the schedule scroll view
 * @param [set] the attribute set of the schedule scroll view
 */
class ScheduleScroll(context: Context, set: AttributeSet): ScrollView(context, set) {
    /**
     * The schedule fragment which will be notified when a swipe occured.
     */
    lateinit var scheduleFragment: ScheduleFragment

    /**
     * The x position where the user tabbed the screen.
     */
    private var downX = 0f
    /**
     * The y position where the user tabbed the screen.
     */
    private var downY = 0f

    /**
     * Is called by the system if a touch event occurred (e.g. touch down or touch up).
     *
     * @param [ev] the event which occurred
     *
     * @return true if the event was handled by this method
     */
    override fun dispatchTouchEvent(ev: MotionEvent?): Boolean {
        when (ev?.action) {
            MotionEvent.ACTION_DOWN -> {
                downX = ev.x
                downY = ev.y
            }
            MotionEvent.ACTION_UP -> {
                val upX = ev.x
                val upY = ev.y
                val deltaX = downX - upX
                val deltaY = downY - upY

                // check if the user moved more than 50 pixel vertical and more vertical than horizontal
                if (::scheduleFragment.isInitialized && abs(deltaX) > 50 && abs(deltaX) > abs(deltaY)) {
                    if (deltaX > 0) {
                        scheduleFragment.onSwipeLeft()
                    } else {
                        scheduleFragment.onSwipeRight()
                    }
                }
            }
        }

        return super.dispatchTouchEvent(ev)
    }
}