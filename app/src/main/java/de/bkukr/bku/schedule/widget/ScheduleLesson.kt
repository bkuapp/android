package de.bkukr.bku.schedule.widget

import kotlinx.serialization.Serializable

// Copyright (C) 2020  Sven Op de Hipt
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

/**
 * Represents a schedule lesson with the given [id], the teacher [title], the [lastName] of the teacher,
 * the name of the class, the [start] of the lesson, the [end] of the lesson, the [course] name of the lesson,
 * the [represented] state which is true if the lesson is represented and the [isCancelled] state for the lesson
 * which is true if the lesson is cancelled for the widget.
 *
 * @author Sven Op de Hipt
 *
 * @constructor Create a schedule lesson with the given [id], the teacher [title], the [lastName] of the teacher,
 * the name of the class, the [start] of the lesson, the [end] of the lesson, the [course] name of the lesson,
 * the [represented] state which is true if the lesson is represented and the [isCancelled] state for the lesson
 * which is true if the lesson is cancelled.
 *
 * @param [id] the id of the lesson
 * @param [title] the title of the teacher
 * @param [lastName] the last name of the teacher
 * @param [nameOfClass] the name of the class
 * @param [start] the start of the lesson
 * @param [end] the end of the lesson
 * @param [course] the course name of the lesson
 * @param [room] the room of the lesson
 * @param [represented] the representation state which is true if the lesson is represented
 * @param [isCancelled] the is cancelled state which is true if the lesson is cancelled
 */
@Serializable
data class ScheduleLesson(val id: Int, val title: String, val lastName: String, val nameOfClass: String, val start: Int,
                          val end: Int, val course: String, val room: String?, val represented: Boolean, val isCancelled: Boolean)