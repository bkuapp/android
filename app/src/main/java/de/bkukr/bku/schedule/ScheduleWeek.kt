package de.bkukr.bku.schedule

import kotlinx.serialization.Serializable

// Copyright (C) 2020  Sven Op de Hipt
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

/**
 * Represents a schedule week in schedule fragment with the given [name], the current schedule hour, the given week
 * and the schedule days of the week.
 *
 * @author Sven Op de Hipt
 *
 * @constructor Creates a schedule week in schedule fragment with the given [name], the current schedule hour, the given week
 * and the schedule days of the week.
 *
 * @param [name] the name of the class, teacher or room
 * @param [currentHour] the current schedule hour
 * @param [week] the schedule week (start date and end date)
 * @param [days] the schedule days
 */
@Serializable
data class ScheduleWeek(val name: String, val currentHour: Int, val week: String, val days: Array<ScheduleDay>) {
    /**
     * Returns true if the [other] object is equal to the current object.
     *
     * @param [other] the other object which will be compared
     *
     * @return true if the [other] object is equal to the current object
     */
    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as ScheduleWeek

        if (week != other.week) return false
        if (!days.contentEquals(other.days)) return false

        return true
    }

    /**
     * Returns the hash code of the current object.
     *
     * @return the hash code of the current object
     */
    override fun hashCode(): Int {
        var result = week.hashCode()
        result = 31 * result + days.contentHashCode()
        return result
    }
}