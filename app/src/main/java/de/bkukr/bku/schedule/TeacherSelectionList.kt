package de.bkukr.bku.schedule

import de.bkukr.bku.person.Person
import kotlinx.serialization.Serializable

// Copyright (C) 2020  Sven Op de Hipt
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

/**
 * Represents a teacher selection list which is used by the teacher selection in the schedule.
 *
 * @author Sven Op de Hipt
 *
 * @constructor Creates a teacher selection list which is used by the teacher selection in the schedule.
 *
 * @param [currentTeacherID] the id of the current teacher
 * @param [teachers] all available teachers which will be selectable in the selection
 */
@Serializable
data class TeacherSelectionList(val currentTeacherID: Int, val teachers: Array<Person>) {
    /**
     * Returns true if the [other] object is equal to the current object.
     *
     * @param [other] the other object which will be compared
     *
     * @return true if the [other] object is equal to the current object
     */
    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as TeacherSelectionList

        if (currentTeacherID != other.currentTeacherID) return false
        if (!teachers.contentEquals(other.teachers)) return false

        return true
    }

    /**
     * Returns the hash code of the current object.
     *
     * @return the hash code of the current object
     */
    override fun hashCode(): Int {
        var result = currentTeacherID
        result = 31 * result + teachers.contentHashCode()
        return result
    }
}