package de.bkukr.bku.schedule

import android.content.Context
import android.graphics.Paint
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import de.bkukr.bku.R
import de.bkukr.bku.general.MainActivity
import de.bkukr.bku.general.isTeacher
import de.bkukr.bku.general.writeEmail
import de.bkukr.bku.handler.TimeHandler
import de.bkukr.bku.network.JSONHandlerPhone
import kotlinx.android.synthetic.main.fragment_course.*
import kotlinx.serialization.builtins.ListSerializer
import kotlinx.serialization.builtins.list
import kotlinx.serialization.builtins.serializer
import java.util.*

// Copyright (C) 2020  Sven Op de Hipt
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

/**
 * Shows the detailed schedule view with the schedule table to the user.
 *
 * @author Sven Op de Hipt
 */
class CourseFragment: Fragment() {
    /**
     * Is called by the system if the view needs to be created.
     *
     * @param [inflater] the layout inflater which converts the resource file to a view
     * @param [container] the view group which contains the view
     * @param [savedInstanceState] the saved instance state for this view
     *
     * @return the created view
     */
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? =
        inflater.inflate(R.layout.fragment_course, container, false)

    /**
     * Is called by the system when the activity is created or this view is created first.
     * Loads the course data from the server and shows it to the user.
     *
     * @param [savedInstanceState] the saved instance states for the view
     */
    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        val context = activity ?: context
        val extras = arguments
        val params = extras?.getSerializable("data") as? HashMap<String, String>

        var scheduleDetailsLoaded = false
        var studentSchedulePlanLoaded = !(context?.isTeacher ?: true)

        (activity as? MainActivity)?.loadingCircleShown = true

        if (context != null) {
            if (context.isTeacher) {
                nameLabel.text = context.getString(R.string.classInfo)
                emailToAllButton.text = getString(R.string.emailToAllClasses)

                if (params != null) {
                    val fragment = ScheduleFragment()
                    fragment.touchGesturesEnabled = false
                    activity?.supportFragmentManager?.beginTransaction()?.replace(R.id.scheduleFragment, fragment)?.commit()

                    val serializer = ScheduleWeek.serializer()
                    val scheduleParams = mutableMapOf("scheduleID" to (params["id"] ?: ""), "inWeeks" to "0")

                    val activity = activity
                    if (activity is MainActivity && activity.differentTeacher) {
                        scheduleParams["teacherID"] = activity.currentTeacherID.toString()
                    }

                    if (activity != null) {
                        // loads the data for the class schedule and parses it to the view
                        JSONHandlerPhone.getJSON(activity, 107, serializer, scheduleParams) { scheduleData ->
                            (activity as MainActivity).loadingCircleShown = !scheduleDetailsLoaded
                            studentSchedulePlanLoaded = true
                            fragment.scheduleData = scheduleData
                        }
                    }
                }
            }
            else {
                scheduleHeader.visibility = View.GONE
            }

            emailInfo.paintFlags = emailInfo.paintFlags or Paint.UNDERLINE_TEXT_FLAG

            if (extras != null) {

                val activity = activity

                if (params != null && activity != null) {
                    val serializer = DetailScheduleLesson.serializer()

                    if (activity is MainActivity && activity.differentTeacher) {
                        params["teacherID"] = activity.currentTeacherID.toString()
                    }

                    // loads the data for the detail lesson and parses it to the view
                    JSONHandlerPhone.getJSON(activity, 4, serializer, params) { lesson ->
                        (activity as? MainActivity)?.loadingCircleShown = !studentSchedulePlanLoaded
                        scheduleDetailsLoaded = true
                        val courseName = lesson.course

                        activity.title = courseName

                        nameInfo.text = lesson.name
                        hourInfo.text = getString(R.string.hourWithParams, lesson.start, lesson.end,
                            TimeHandler.buildStartTime(lesson.start), TimeHandler.buildEndTime(lesson.end))

                        val email = lesson.email
                        emailInfo.text = email
                        emailInfo.setOnClickListener { context.writeEmail(arrayOf(email)) }

                        emailToAllButton.setOnClickListener {
                            sendEmailToAll(context, params.getOrElse("id", {
                                return@setOnClickListener
                            }))
                        }
                    }
                }
            }
        }
    }

    /**
     * Shows an email view which sends the email to all teacher or classes of the day where the course is taking place.
     *
     * @param [context] the context from which the emails will be send
     * @param [courseID] the id of the course which should take place at the day
     */
    private fun sendEmailToAll(context: Context, courseID: String) {
        val serializer = ListSerializer(String.serializer())

        val params = mutableMapOf("courseID" to courseID)

        val activity = activity

        if (activity is MainActivity && activity.differentTeacher) {
            params["teacherID"] = activity.currentTeacherID.toString()
        }

        if (activity != null) {
            // load the emails from the server
            JSONHandlerPhone.getJSON(activity, 106, serializer, params) { emails ->
                context.writeEmail(emails.toTypedArray())
            }
        }
    }
}
