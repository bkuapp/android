package de.bkukr.bku.schedule

import kotlinx.serialization.Serializable

// Copyright (C) 2020  Sven Op de Hipt
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

/**
 * Represents a schedule lesson for the detail schedule view with the given [name], the given [email],
 * the given [start] time, the given [end] time, the given [course] name and the given [room].
 *
 * @author Sven Op de Hipt
 *
 * @constructor Creates a schedule lesson for the detail schedule view with the given [name], the given [email],
 * the given [start] time, the given [end] time, the given [course] name and the given [room].
 *
 * @param [name] the teacher or class or room name of the lesson
 * @param [email] the email of the teacher or class of the lesson
 * @param [start] the start of the lesson
 * @param [end] the end of the lesson
 * @param [course] the course name of the lesson
 * @param [room] the room of the lesson
 */
@Serializable
data class DetailScheduleLesson(val name: String, val email: String, val start: Int, val end: Int, val course: String,
                                val room: String?)