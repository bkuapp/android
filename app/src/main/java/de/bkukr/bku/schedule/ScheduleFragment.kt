package de.bkukr.bku.schedule

import android.graphics.Paint
import android.os.Bundle
import android.util.TypedValue
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.fragment.app.Fragment
import de.bkukr.bku.R
import de.bkukr.bku.general.MainActivity
import de.bkukr.bku.general.getColorFromAttr
import de.bkukr.bku.general.isTeacher
import de.bkukr.bku.network.JSONHandlerPhone
import kotlinx.android.synthetic.main.fragment_schedule.*
import kotlin.math.max
import kotlin.math.min
import kotlin.math.roundToInt

// Copyright (C) 2020  Sven Op de Hipt
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

/**
 * Shows the schedule view with the schedule table to the user.
 *
 * @author Sven Op de Hipt
 */
class ScheduleFragment : Fragment() {
    /**
     * Indicates if touch gestures (swipe and detail click) are enabled.
     */
    var touchGesturesEnabled = true

    /**
     * The schedule data which stores information about the schedule week.
     * This data will be converted into the schedule layout.
     */
    var scheduleData: ScheduleWeek = ScheduleWeek("", 0, "", emptyArray())
        set(scheduleData) {
            if (activity != null) {
                val hours = activity?.findViewById<TableLayout>(getResourceID(0))
                var minHeight = getMinHours(scheduleData)
                var maxHeight = getMaxHours(scheduleData)

                hours?.removeAllViews()

                if (minHeight == 17) {
                    minHeight = 1
                }

                if (maxHeight == 0) {
                    maxHeight = 17
                }

                val week = scheduleData.week.split("-")
                if (week.size == 2) {
                    startDate.text = week[0]
                    endDate.text = week[1]
                }

                val context = context

                // teacher spinner
                if (context != null) {
                    val classOrTeacherNameString = scheduleData.name

                    val classOrTeacherView = if (context.isTeacher && classOrTeacherNameString.contains('(') &&
                                                 classOrTeacherNameString.contains(')')) {
                        val spinner = Spinner(context)

                        // load the teachers from the server
                        val serializer = TeacherSelectionList.serializer()
                        JSONHandlerPhone.getJSON(context, 108, serializer) { teacherList ->
                            val spinnerArray = teacherList.teachers
                            spinner.adapter = ArrayAdapter(context, R.layout.teacher_select_spinner_layout, spinnerArray)

                            val activity = activity as? MainActivity

                            if (activity != null) {
                                if (activity.currentTeacherID == 0) {
                                    activity.currentTeacherID = teacherList.currentTeacherID
                                }

                                spinner.setSelection(spinnerArray.indexOfFirst { it.id == activity.currentTeacherID })

                                spinner.onItemSelectedListener = object : AdapterView.OnItemSelectedListener {
                                    override fun onItemSelected(parentView: AdapterView<*>?, selectedItemView: View?,
                                                                position: Int, id: Long) {
                                        activity.differentTeacher = true

                                        (parentView?.getChildAt(0) as? TextView)?.
                                            setTextColor(context.getColorFromAttr(R.attr.red))

                                        val teacherID = spinnerArray[position].id
                                        if (teacherID != activity.currentTeacherID) {
                                            activity.currentTeacherID = teacherID
                                        }
                                    }

                                    override fun onNothingSelected(parent: AdapterView<*>?) {}
                                }
                            }
                        }
                        spinner
                    } else {
                        val paddingEnd = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 10f,
                            resources.displayMetrics).roundToInt()

                        val textView = TextView(context)
                        textView.text = classOrTeacherNameString
                        textView.setTextSize(TypedValue.COMPLEX_UNIT_PX, resources.getDimension(R.dimen.schedule_big_font_size))
                        textView.setPadding(0, 0, paddingEnd, 0)
                        textView
                    }

                    classOrTeacherName.removeAllViews()
                    classOrTeacherName.addView(classOrTeacherView)
                }

                val currentActivity = activity

                // hour bar
                for (hour in minHeight..maxHeight) {
                    val number = TextView(activity)
                    number.text = hour.toString()
                    number.gravity = Gravity.CENTER
                    number.height =
                        TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 60f, resources.displayMetrics).toInt()
                    number.background = activity?.getDrawable(R.drawable.border_schedule)
                    if (currentActivity != null && hour == scheduleData.currentHour) {
                        number.setTextColor(currentActivity.getColor(android.R.color.holo_blue_dark))
                    }
                    hours?.addView(number)
                }

                val generalLayoutParams = TableLayout.LayoutParams()

                // days and courses
                val dayNameIDS = arrayOf(R.id.monday, R.id.tuesday, R.id.wednesday, R.id.thursday, R.id.friday)
                val dayNameTexts = arrayOf(R.string.monday, R.string.tuesday, R.string.wednesday, R.string.thursday, R.string.friday)

                val days = scheduleData.days
                for (i in days.indices) {
                    val day = days[i]

                    val date = day.date
                    val isCurrentDate = date.contains('c')

                    val dateTextView = activity?.findViewById<TextView>(dayNameIDS[i])
                    dateTextView?.text = String.format(
                        "%s.,\n%s",
                        getString(dayNameTexts[i]),
                        date.removeSuffix("c")
                    )

                    if (currentActivity != null) {
                        if (isCurrentDate) {
                            dateTextView?.setTextColor(currentActivity.getColor(android.R.color.holo_blue_dark))
                        } else {
                            dateTextView?.setTextColor(currentActivity.getColorFromAttr(android.R.attr.textColor))
                        }

                        val resourceId = getResourceID(i + 1)
                        val entryRow = currentActivity.findViewById<TableLayout>(resourceId)
                        entryRow.removeAllViews()
                        val courses = day.courses

                        if (courses.count() != 0) {
                            for (hour in minHeight until courses.first().start) {
                                addEmptyView(entryRow)
                            }

                            var lastHour = 0
                            for (courseObject in courses) {
                                val start = courseObject.start
                                val end = courseObject.end

                                while (lastHour in minHeight until start - 1) {
                                    addEmptyView(entryRow)
                                    lastHour++
                                }

                                lastHour = end

                                entryRow.addView(
                                    generateTextView(courseObject, generalLayoutParams),
                                    generalLayoutParams
                                )
                            }

                            val lastCourse = courses.last()
                            val end = lastCourse.end

                            for (hour in end until maxHeight) {
                                addEmptyView(entryRow)
                            }
                        } else {
                            for (j in minHeight - 1 until maxHeight) {
                                addEmptyView(entryRow)
                            }
                        }
                    }
                }
            }
        }

    /**
     * Is called by the system if the view needs to be created.
     *
     * @param [inflater] the layout inflater which converts the resource file to a view
     * @param [container] the view group which contains the view
     * @param [savedInstanceState] the saved instance state for this view
     *
     * @return the created view
     */
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? =
        inflater.inflate(R.layout.fragment_schedule, container, false)

    /**
     * Is called by the system when the activity is created or this view is created first.
     * Loads the schedule data from the main activity and converts it to show it to the user.
     *
     * @param [savedInstanceState] the saved instance states for the view
     */
    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        val activity = activity

        if (activity is MainActivity) {
            val scheduleData = activity.scheduleData

            if (scheduleData != null) {
                this.scheduleData = scheduleData
            }
        }

        scheduleScroll.scheduleFragment = this
    }

    /**
     * Returns the minimum hours for the current [week].
     *
     * @param [week] the current week
     *
     * @return the minimum hours for the current [week]
     */
    private fun getMinHours(week: ScheduleWeek): Int {
        var minHours = 18

        for (day in week.days) {
            val courses = day.courses
            if (courses.count() != 0) {
                minHours = min(courses.first().start, minHours)
            }
        }

        minHours = when (minHours) {
            in 1..8 -> {
                1
            }
            in 9..17 -> {
                9
            }
            else -> {
                1
            }
        }

        return minHours
    }

    /**
     * Returns the maximum hours for the current [week].
     *
     * @param [week] the current week
     *
     * @return the maximum hours for the current [week]
     */
    private fun getMaxHours(week: ScheduleWeek): Int {
        var maxHours = 0

        for (day in week.days) {
            val courses = day.courses
            if (courses.count() != 0) {
                maxHours = max(courses.last().end, maxHours)
            }
        }

        return maxHours
    }

    /**
     * Returns the resource id for a specific schedule [day] or the hour bar.
     *
     * @param [day] the schedule day (0 = hour bar)
     *
     * @return the resource id for a specific schedule [day] or the hour bar
     */
    private fun getResourceID(day: Int) = resources.getIdentifier("schedule_row$day", "id", activity?.packageName)

    /**
     * Generates a text view for a schedule lesson.
     *
     * @param [course] the schedule lesson
     * @param [generalLayoutParams] the layout parameters for the text view
     *
     * @return the generated text view for the schedule lesson
     */
    private fun generateTextView(course: ScheduleLesson, generalLayoutParams: TableLayout.LayoutParams): TextView {
        val represented = course.represented
        val turnsOut = course.isCancelled

        val courseTV = TextView(activity)

        val nameInfo = course.classOrTeacher
        courseTV.text = String.format("%s\n", nameInfo)

        val courseInfo = course.course
        courseTV.append(String.format("%s\n", courseInfo))

        val roomInfo = course.roomOrTeacher
        courseTV.append(roomInfo)

        courseTV.gravity = Gravity.CENTER

        val start = course.start
        val end = course.end
        generalLayoutParams.weight = (end - start).toFloat()

        courseTV.height = TypedValue.applyDimension(
            TypedValue.COMPLEX_UNIT_DIP,
            60f,
            resources.displayMetrics
        ).toInt() * (end - start + 1)
        courseTV.background = activity?.getDrawable(R.drawable.border_schedule)

        val currentActivity = activity

        if (currentActivity != null) {
            if (represented) {
                courseTV.setTextColor(currentActivity.getColorFromAttr(R.attr.green))
            }
            else if (turnsOut) {
                courseTV.setTextColor(currentActivity.getColorFromAttr(R.attr.red))
                courseTV.paintFlags = courseTV.paintFlags or Paint.STRIKE_THRU_TEXT_FLAG
            }
        }

        if (touchGesturesEnabled) {
            courseTV.setOnClickListener {
                val map = HashMap<String, String>()
                map["id"] = course.id.toString()
                (activity as? MainActivity)?.showDetails(map, CourseFragment())
            }
        }

        return courseTV
    }

    /**
     * Adds an empty view to the [dayView] if there is space between two courses or at the start or end of the day.
     *
     * @param [dayView] the day view where the empty view should be added
     */
    private fun addEmptyView(dayView: TableLayout) {
        val space = View(activity)
        space.background = activity?.getDrawable(R.drawable.border_schedule)
        space.minimumHeight =
            TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP,60f, resources.displayMetrics).toInt()
        dayView.addView(space)
    }

    /**
     * Is called when the user swipes right.
     */
    fun onSwipeRight() {
        onSwipe(-1)
    }

    /**
     * Is called when the user swipes left.
     */
    fun onSwipeLeft() {
        onSwipe(1)
    }

    /**
     * Is called when the user performs a swipe gesture.
     *
     * @param [weeks] the weeks which should be added to the current one (can be negative)
     */
    private fun onSwipe(weeks: Int) {
        if (touchGesturesEnabled) {
            (activity as? MainActivity)?.updateScheduleWeekBy(weeks) {
                val scheduleData = (activity as? MainActivity)?.scheduleData

                if (scheduleData != null) {
                    this.scheduleData = scheduleData
                }
            }
        }
    }
}
