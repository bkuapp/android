package de.bkukr.bku.schedule

import kotlinx.serialization.Serializable

// Copyright (C) 2020  Sven Op de Hipt
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

/**
 * Represents a schedule day in schedule fragment with the given [date] and the given [courses].
 *
 * @author Sven Op de Hipt
 *
 * @constructor Creates a schedule day in schedule fragment with the given [date] and the given [courses].
 *
 * @param [date] the date where the schedule date will take place
 * @param [courses] the courses of the schedule day
 */
@Serializable
data class ScheduleDay(val date: String, val courses: Array<ScheduleLesson>) {
    /**
     * Returns true if the current object is equal to the [other] object.
     *
     * @param [other] the other object which will be compared
     *
     * @return true if the current object is equal to the [other] object
     */
    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as ScheduleDay

        if (date != other.date) return false
        if (!courses.contentEquals(other.courses)) return false

        return true
    }

    /**
     * Returns the hash code of the current object.
     *
     * @return the hash code of the current object
     */
    override fun hashCode(): Int {
        var result = date.hashCode()
        result = 31 * result + courses.contentHashCode()
        return result
    }
}