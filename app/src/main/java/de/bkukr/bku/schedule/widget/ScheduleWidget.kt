package de.bkukr.bku.schedule.widget

import android.app.PendingIntent
import android.appwidget.AppWidgetManager
import android.appwidget.AppWidgetProvider
import android.content.Context
import android.content.Intent
import android.graphics.Color
import android.os.Bundle
import android.widget.RemoteViews
import de.bkukr.bku.R
import de.bkukr.bku.general.MainActivity
import de.bkukr.bku.general.isLoggedIn
import de.bkukr.bku.general.isTeacher
import de.bkukr.bku.handler.TimeHandler
import de.bkukr.bku.network.JSONHandlerPhone
import kotlinx.serialization.builtins.ListSerializer
import kotlinx.serialization.builtins.list
import kotlin.math.floor
import kotlin.math.max
import kotlin.math.min
import kotlin.math.roundToInt

// Copyright (C) 2020  Sven Op de Hipt
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

/**
 * The schedule widget which shows the up to the next 4 next schedule lessons to the user.
 *
 * @author Sven Op de Hipt
 */
class ScheduleWidget : AppWidgetProvider() {
    private companion object {
        /**
         * The current pending intents to be able to cancel them later.
         */
        val currentIntents = mutableMapOf<Int, MutableList<PendingIntent>>()
    }

    /**
     * Is called by the system when the widget is created, resized or changed in any other way and updates the widget.
     * @param [context] the widgets context
     * @param [appWidgetManager] the manager of the widget
     * @param [appWidgetId] the id of the widget
     * @param [newOptions] the updated options of the widget
     */
    override fun onAppWidgetOptionsChanged(context: Context, appWidgetManager: AppWidgetManager, appWidgetId: Int, newOptions: Bundle) {
        super.onAppWidgetOptionsChanged(context, appWidgetManager, appWidgetId, newOptions)

        updateWidget(context, appWidgetManager, appWidgetId)
    }
    /**
     * Is called by the system every 30 minutes to updated the widget.
     *
     * @param [context] the widgets context
     * @param [appWidgetManager] the manager of the widget
     * @param [appWidgetIds] the ids of the widgets
     */
    override fun onUpdate(context: Context, appWidgetManager: AppWidgetManager, appWidgetIds: IntArray) {
        // There may be multiple widgets active, so update all of them
        for (appWidgetId in appWidgetIds) {
            updateWidget(context, appWidgetManager, appWidgetId)
        }
    }

    /**
     * Is called when the widget must be update during options changed or auto-updated
     * by [onAppWidgetOptionsChanged] or [onUpdate]
     *
     * @param [context] the widgets context
     * @param [appWidgetManager] the manager of the widget
     * @param [appWidgetId] the id of the widget
     */
    private fun updateWidget(context: Context, appWidgetManager: AppWidgetManager, appWidgetId: Int) {
        // check if the user is logged in
        if (context.isLoggedIn) {
            val serializer = ListSerializer(ScheduleLesson.serializer())
            JSONHandlerPhone.getJSON(context, 200, serializer) { lessons ->
                val views: RemoteViews

                // check if the lessons are empty
                if (lessons.isEmpty()) {
                    views = RemoteViews(context.packageName, R.layout.widget_info)
                    views.setTextViewText(R.id.info_text, context.getString(R.string.noMoreCourses))
                }
                // lessons aren't empty
                else {
                    // get the max height of the widget
                    val options = appWidgetManager.getAppWidgetOptions(appWidgetId)
                    val maxHeight = max(options.getInt(AppWidgetManager.OPTION_APPWIDGET_MAX_HEIGHT),
                        options.getInt(AppWidgetManager.OPTION_APPWIDGET_MIN_HEIGHT))

                    // calculate the row height, the number of rows and the padding for each row
                    val defaultRowHeight = 65f
                    val rows = floor(maxHeight / defaultRowHeight).toInt()
                    val padding = if (rows != 0) {
                        ((maxHeight / rows - defaultRowHeight) / 2f).roundToInt()
                    }
                    else {
                        0
                    }

                    views = RemoteViews(context.packageName, R.layout.schedule_widget)

                    // get all pending intents for the current widget
                    var intents = currentIntents[appWidgetId] ?: mutableListOf()

                    // cancel all pending intents, so that rows, that are partially visible are not clickable
                    for (intent in intents) {
                        intent.cancel()
                    }

                    // clear the pending intents
                    intents = mutableListOf()

                    // reset all texts of the widget, so that texts, which would be partially visible don`t have any text
                    for (i in 0..3) {
                        val ids = getIDs(i + 1, context)
                        views.apply {
                            setTextViewText(ids[1], "")
                            setTextViewText(ids[2], "")
                            setTextViewText(ids[3], "")
                            setTextViewText(ids[4], "")
                        }
                    }

                    for (i in 0 until min(rows, lessons.size)) {
                        val lesson = lessons[i]
                        val teacher = String.format("%s %s", lesson.title, lesson.lastName)
                        val className = lesson.nameOfClass

                        val ids = getIDs(i + 1, context)

                        // create and set the pending intent for the row
                        val intent = Intent(context, MainActivity::class.java)
                        intent.action = "Schedule Widget Click"
                        intent.putExtra("type", "schedule")
                        intent.putExtra("id", lesson.id)

                        val pendingIntent = PendingIntent.getActivity(context, i, intent, 0)
                        intents.add(pendingIntent)
                        views.setOnClickPendingIntent(ids[0], pendingIntent)

                        // set the padding for the row
                        if (padding > 0) {
                            views.setViewPadding(ids[0], 0, padding, 0, padding)
                        }
                        else {
                            views.setViewPadding(ids[0], 0, 0, 0, 0)
                        }

                        // set the texts for the row
                        views.setTextViewText(ids[1], buildTime(lesson.start, lesson.end))
                        if (context.isTeacher) {
                            views.setTextViewText(ids[2], className)
                        } else {
                            views.setTextViewText(ids[2], teacher)
                        }
                        views.setTextViewText(ids[3], lesson.course)
                        views.setTextViewText(ids[4], lesson.room)

                        // set the represented and is cancelled color for the text view
                        for (j in 1..4) {
                            when {
                                lesson.represented -> {
                                    context.getColor(android.R.color.holo_green_dark)
                                }
                                lesson.isCancelled -> {
                                    context.getColor(android.R.color.holo_red_dark)
                                }
                                else -> {
                                    Color.BLACK
                                }
                            }.apply {
                                views.setTextColor(ids[j], this)
                            }
                        }
                    }

                    currentIntents[appWidgetId] = intents
                }

                appWidgetManager.updateAppWidget(appWidgetId, views)
            }
        }
        // user is not logged in. display info
        else {
            val views = RemoteViews(context.packageName, R.layout.widget_info)
            views.setTextViewText(R.id.info_text, context.getString(R.string.pleaseLogin))

            appWidgetManager.updateAppWidget(appWidgetId, views)
        }
    }

    /**
     * get the text views for a given [row]
     * @param [row] the row for which the ids are
     * @param [context] the context which the ids belong to
     * @return the ids for the [row]
     */
    private fun getIDs(row: Int, context: Context): Array<Int> {
        return arrayOf(getResourceID(row, "", context),
            getResourceID(row, "time", context),
            getResourceID(row, "teacher_name", context),
            getResourceID(row, "course_name", context),
            getResourceID(row, "room", context))
    }

    /**
     * get the resource for a text view in a specific [row] with a specific [type]
     *
     * @sample getResourceID
     *
     * @param [row] the row for which the ids are
     * @param [type] the type of the text view
     * @param [context] the context which the ids belong to
     *
     * @return the id for the text view in the specified [row] with the given [type]
     */
    private fun getResourceID(row: Int, type: String, context: Context) =
        context.resources.getIdentifier("schedule_row$row" + "_$type", "id", context.packageName)

    /**
     * Convert the schedule [start] and [end] time to a multiline string which will be presented to the user.
     *
     * @param [start] the schedule start time
     * @param [end] the schedule end time
     *
     * @return the converted schedule [start] and [end] time as a multiline string
     */
    private fun buildTime(start: Int, end: Int) =
       "${TimeHandler.buildStartTime(start)}\n${TimeHandler.buildEndTime(end)}"
}