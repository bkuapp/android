package de.bkukr.bku.person

import android.graphics.Paint
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import de.bkukr.bku.R
import de.bkukr.bku.general.MainActivity
import de.bkukr.bku.general.isTeacher
import de.bkukr.bku.general.writeEmail
import de.bkukr.bku.network.JSONHandlerPhone
import de.bkukr.bku.schedule.ScheduleFragment
import de.bkukr.bku.schedule.ScheduleWeek
import kotlinx.android.synthetic.main.fragment_teacher.*

// Copyright (C) 2020  Sven Op de Hipt
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

/**
 * The person fragment which shows the detail person information to the user.
 *
 * @author Sven Op de Hipt
 */
class PersonFragment : Fragment() {
    /**
     * Is called by the system if the view needs to be created.
     *
     * @param [inflater] the layout inflater which converts the resource file to a view
     * @param [container] the view group which contains the view
     * @param [savedInstanceState] the saved instance state for this view
     *
     * @return the created view
     */
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? =
        inflater.inflate(R.layout.fragment_teacher, container, false)

    /**
     * Is called by the system when the activity is created or this view is created first.
     * Loads the person data from the server and parses it into the view.
     *
     * @param [savedInstanceState] the saved instance states for the view
     */
    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        emailOfPerson.paintFlags = emailOfPerson.paintFlags or Paint.UNDERLINE_TEXT_FLAG

        val extras = arguments

        var personDetailsLoaded = false
        var studentSchedulePlanLoaded = !(context?.isTeacher ?: true)

        if (extras != null) {
            val extraInfo = extras.getSerializable("data") as? HashMap<String, String>
            val currentActivity = activity

            if (extraInfo != null && currentActivity != null) {
                val context = context ?: activity

                if (context != null && context.isTeacher && extraInfo["personType"] == "class") {
                    val fragment = ScheduleFragment()
                    fragment.touchGesturesEnabled = false
                    activity?.supportFragmentManager?.beginTransaction()?.replace(R.id.scheduleFragment, fragment)?.commit()

                    val serializer = ScheduleWeek.serializer()
                    val scheduleParams = mutableMapOf("classID" to (extraInfo["id"] ?: ""), "inWeeks" to "0")

                    // load the schedule data from the server
                    JSONHandlerPhone.getJSON(context, 107, serializer, scheduleParams) { scheduleData ->
                        fragment.scheduleData = scheduleData

                        studentSchedulePlanLoaded = true
                        (activity as? MainActivity)?.loadingCircleShown = !personDetailsLoaded
                    }
                }
                else {
                    scheduleHeader.visibility = View.GONE
                }

                if (extraInfo["personType"] == "teacher") {
                    coursesLayout.visibility = View.GONE
                }

                // load the person data from the server
                val serializer = DetailPerson.serializer()
                JSONHandlerPhone.getJSON(currentActivity, 6, serializer, extraInfo) { person ->
                    val email = person.email

                    activity?.title = person.name
                    emailOfPerson.text = email
                    coursesOfPerson.text = generateCoursesString(person.courses)

                    emailOfPerson.setOnClickListener { context?.writeEmail(arrayOf(email)) }

                    personDetailsLoaded = true
                    (activity as? MainActivity)?.loadingCircleShown = !studentSchedulePlanLoaded && extraInfo["personType"] == "class"
                }
            }
        }
    }

    /**
     * Converts the [coursesData] from an array to a multiline string.
     *
     * @param [coursesData] the courses data as array
     *
     * @return the courses as multiline string
     */
    private fun generateCoursesString(coursesData: Array<String>): String {
        return if (!coursesData.isNullOrEmpty()) {
            val ret = StringBuilder()

            for (course in coursesData) {
                ret.append(course).append("\n")
            }

            ret.substring(0, ret.length - 1)
        }
        else {
            ""
        }
    }
}
