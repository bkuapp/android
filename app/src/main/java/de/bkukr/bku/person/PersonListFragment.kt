package de.bkukr.bku.person

import android.content.res.Configuration
import android.graphics.Point
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import de.bkukr.bku.R
import de.bkukr.bku.general.MainActivity
import de.bkukr.bku.general.isTeacher
import de.bkukr.bku.list.*
import kotlinx.android.synthetic.main.fragment_teacher_list.*

// Copyright (C) 2020  Sven Op de Hipt
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

/**
 * The person list which will be shown in the person tab. Shows all teachers and classes which are connected to the user.
 *
 * @author Sven Op de Hipt
 */
class PersonListFragment : Fragment() {
    /**
     * The data which contains information about the data in form of [Person] objects.
     */
    private var personData: List<List<Person>>
        get() {
            val result = (activity as? MainActivity)?.personData

            if (result != null) {
                return result
            }
            return emptyList()
        }
        set(personData) {
            val persons = mutableListOf<ListItem>()

            if (context?.isTeacher == true) {
                // classes
                persons.add(ListHeaderItem(context?.getString(R.string.classes) ?: ""))
                for (teacherOrClass in personData.firstOrNull() ?: emptyList()) {
                    val extraParams = HashMap<String, String>()
                    extraParams["personType"] = "class"

                    persons.add(ListBodyItem(teacherOrClass.name, teacherOrClass.id, PersonFragment(),
                        extraParams))
                }

                // teachers
                persons.add(ListHeaderItem(context?.getString(R.string.teachers) ?: ""))
                for (teacherOrClass in personData.lastOrNull() ?: emptyList()) {
                    val extraParams = HashMap<String, String>()
                    extraParams["personType"] = "teacher"

                    persons.add(ListBodyItem(teacherOrClass.name, teacherOrClass.id, PersonFragment(),
                        extraParams))
                }
            }
            else {
                // teachers
                for (teacherOrClass in personData.firstOrNull() ?: emptyList()) {
                    persons.add(ListBodyItem(teacherOrClass.toString(), teacherOrClass.id, PersonFragment()))
                }
            }

            val adapter = ListAdapter(activity, persons)
            personList.adapter = adapter
            personList.layoutManager = LinearLayoutManager(activity)
            personList.addItemDecoration(ListItemDecorator(adapter))
            personList.addItemDecoration(DividerItemDecoration(context, DividerItemDecoration.VERTICAL))
        }

    /**
     * Is called by the system if the view needs to be created.
     *
     * @param [inflater] the layout inflater which converts the resource file to a view
     * @param [container] the view group which contains the view
     * @param [savedInstanceState] the saved instance state for this view
     *
     * @return the created view
     */
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? =
        inflater.inflate(R.layout.fragment_teacher_list, container, false)

    /**
     * Is called by the system when the activity is created or this view is created first.
     * Saves the person data from the main view to this view.
     *
     * @param [savedInstanceState] the saved instance states for the view
     */
    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        personData = personData
    }

    /**
     * Is called by the system when the configuration has changed to a [newConfig].
     * Updates the font size depending on the screen size.
     *
     * @param [newConfig] the new configuration
     */
    override fun onConfigurationChanged(newConfig: Configuration) {
        super.onConfigurationChanged(newConfig)
        val root = activity

        if (root != null) {
            for (i in 0 until personList.childCount) {
                val screenSize = Point()
                root.windowManager.defaultDisplay.getSize(screenSize)

                val row = personList.getChildAt(i) as? TextView
                row?.textSize = screenSize.x / 25.6f
            }
        }
    }
}
