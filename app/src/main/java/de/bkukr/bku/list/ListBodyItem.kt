package de.bkukr.bku.list

import androidx.fragment.app.Fragment

// Copyright (C) 2020  Sven Op de Hipt
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

/**
 * A body item in the list.
 *
 * @author Sven Op de Hipt
 *
 * @constructor Creates a body item for the list with a [text], an [id], a [detailFragment] which will
 * be shown on click and optional [extraParams] which will be send to the server.
 *
 * @param [text] the text which will be shown in the row
 * @param [id] the id of the shown element
 * @param [detailFragment] the fragment which will be shown on click
 * @param [extraParams] the extra params which will be shown to the server and are empty by default
 * @param [textColor] the custom text color which is null by default
 */
class ListBodyItem(override val text: String, val id: Int, val detailFragment: Fragment,
                   val extraParams: HashMap<String, String> = HashMap(), override val textColor: Int? = null): ListItem {
    /**
     * The item type ([RowType.LIST_ITEM]).
     */
    override val itemType = RowType.LIST_ITEM.ordinal
}