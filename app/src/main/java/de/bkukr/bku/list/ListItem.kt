package de.bkukr.bku.list

// Copyright (C) 2020  Sven Op de Hipt
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

/**
 * A item in the list. Can be [ListBodyItem] or [ListHeaderItem].
 *
 * @author Sven Op de Hipt
 */
interface ListItem {
    /**
     * The type of the item (header or body).
     */
    val itemType: Int
    /**
     * The text which is shown in the item.
     */
    val text: String

    /**
     * Text color of the text which will be shown.
     */
    val textColor: Int?
}