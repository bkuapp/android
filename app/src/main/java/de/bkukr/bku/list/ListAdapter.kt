package de.bkukr.bku.list

import android.content.Context
import android.graphics.Paint
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import de.bkukr.bku.R
import de.bkukr.bku.general.MainActivity
import de.bkukr.bku.general.getColorFromAttr

// Copyright (C) 2020  Sven Op de Hipt
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

/**
 * The adapter for the list which categories the [items] in headers and bodies and shows them in the list.
 *
 * @author Sven Op de Hipt
 *
 * @constructor Creates an adapter for the list with the given [context] and [items].
 *
 * @param [context] the context where the list will be shown
 * @param [items] the items which will be shown in the list
 */
class ListAdapter(private val context: Context?, private val items: List<ListItem>):
    RecyclerView.Adapter<RecyclerView.ViewHolder>(), ListStickyHeaderInterface {
    /**
     * Returns the item type ([RowType.HEADER_ITEM] or [RowType.LIST_ITEM]) for the row at the given [position].
     *
     * @param [position] the position of the item which type should be returned
     *
     * @return the item type ([RowType.HEADER_ITEM] or [RowType.LIST_ITEM]) for the row at the given [position]
     */
    override fun getItemViewType(position: Int) = items[position].itemType

    /**
     * Creates the list row with the given [viewType].
     *
     * @param [parent] the parent which will contain the row
     * @param [viewType] the type of the view ([RowType.HEADER_ITEM] or [RowType.LIST_ITEM])
     *
     * @return the created row with the given [viewType]
     */
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return when (viewType) {
            RowType.HEADER_ITEM.ordinal -> HeaderViewHolder(
                LayoutInflater.from(parent.context).inflate(
                    R.layout.header_row_layout,
                    parent,
                    false
                )
            )
            else -> ItemViewHolder(
                LayoutInflater.from(parent.context).inflate(
                    R.layout.body_row_layout,
                    parent,
                    false
                )
            )
        }
    }

    /**
     * Binds the text in the row at the given [position].
     *
     * @param [holder] the row where the text should be bound in
     * @param [position] the position of the row
     */
    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        if (holder is ViewHolder) {
            holder.bindData(position)

            val item = items[position]
            if (item is ListBodyItem) {
                holder.itemView.setOnClickListener {
                    val params = item.extraParams
                    params["id"] = item.id.toString()
                    (context as? MainActivity)?.showDetails(params, item.detailFragment)
                }
            }
        }
    }

    /**
     * Returns the number of items.
     *
     * @return the number of items
     */
    override fun getItemCount() = items.size

    /**
     * Returns the header for the item at the given [itemPosition].
     *
     * @param [itemPosition] the position of the item which header position should be returned
     *
     * @return the position of the header for the [itemPosition]
     */
    override fun getHeaderPositionForItem(itemPosition: Int): Int {
        var headerPosition = 0
        var position = itemPosition
        do {
            if (isHeader(position)) {
                headerPosition = position
                break
            }
            position -= 1
        } while (position >= 0)
        return headerPosition
    }

    /**
     * Returns the header layout for the given [headerPosition].
     *
     * @param [headerPosition] the position of the header for which the layout should be returned
     *
     * @return the header layout for the given [headerPosition]
     */
    override fun getHeaderLayout(headerPosition: Int) = R.layout.header_row_layout

    /**
     * Binds the text in the [header] at the [headerPosition].
     *
     * @param [header] the header where the text should be bound in
     * @param [headerPosition] the position of the header where the text should be bound in
     */
    override fun bindHeaderData(header: View?, headerPosition: Int) {
        header?.findViewById<TextView>(R.id.header_row_text)?.text = items[headerPosition].text
    }

    /**
     * Returns true, if the item at the [itemPosition] is a header
     *
     * @param [itemPosition] the position of item the which should be checked
     *
     * @return true, if the item at the [itemPosition] is a header
     */
    override fun isHeader(itemPosition: Int) = getItemViewType(itemPosition) == RowType.HEADER_ITEM.ordinal

    /**
     * The view holder for rows which contains the row`s text.
     *
     * @author Sven Op de Hipt
     *
     * @constructor Creates a view holder with the given [itemView].
     *
     * @param [itemView] the view for the row
     */
    private abstract inner class ViewHolder(itemView: View): RecyclerView.ViewHolder(itemView) {
        /**
         * The text view where the text will be shown in.
         */
        protected abstract val textView: TextView

        /**
         * Binds the text in the view for the given [position].
         *
         * @param [position] the position of the row where the text should be bound
         */
        fun bindData(position: Int) {
            val item = this@ListAdapter.items[position]
            textView.text = item.text

            val context = context
            if (context != null) {
                if (item.textColor == context.getColorFromAttr(R.attr.red)) {
                    textView.paintFlags = textView.paintFlags or Paint.STRIKE_THRU_TEXT_FLAG
                }

                textView.setTextColor(item.textColor ?: context.getColorFromAttr(android.R.attr.textColor))
            }
        }
    }

    /**
     * The view holder for header rows which contains the row`s text.
     *
     * @author Sven Op de Hipt
     *
     * @constructor Creates a header view holder with the given [itemView].
     *
     * @param [itemView] the view for the header row
     */
    private inner class HeaderViewHolder(itemView: View): ViewHolder(itemView) {
        /**
         * The text view where the text will be shown in.
         */
        override val textView: TextView = itemView.findViewById(R.id.header_row_text)
    }

    /**
     * The view holder for body rows which contains the row`s text.
     *
     * @author Sven Op de Hipt
     *
     * @constructor Creates an item view holder with the given [itemView].
     *
     * @param [itemView] the view for the body row
     */
    private inner class ItemViewHolder(itemView: View): ViewHolder(itemView) {
        /**
         * The text view where the text will be shown in.
         */
        override val textView: TextView = itemView.findViewById(R.id.body_row_text)
    }
}