// Copyright (C) 2020  Sven Op de Hipt
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

package de.bkukr.bku.list

import android.graphics.Canvas
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView

// Copyright (C) 2020  Sven Op de Hipt
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

/**
 * The list item decorator which draws the current list header at the top of the list.
 *
 * @author Sven Op de Hipt
 *
 * @constructor Creates a list item decorator with the given [mListener].
 *
 * @param [mListener] the listener which returns the header position for a specific item and checks if items are headers.
 */
class ListItemDecorator(private val mListener: ListStickyHeaderInterface): RecyclerView.ItemDecoration() {
    /**
     * The height of the headers.
     */
    private var mStickyHeaderHeight = 0

    /**
     * Draws the header at the top of the list or moves it if another header reaches the top.
     *
     * @param [c] the canvas where the header will be drawn in
     * @param [parent] the list which contains the items
     * @param [state] the state of the list
     */
    override fun onDrawOver(c: Canvas, parent: RecyclerView, state: RecyclerView.State) {
        super.onDrawOver(c, parent, state)
        // get the position of the item which is at the top currently
        val topChild = parent.getChildAt(0) ?: return
        val topChildPosition = parent.getChildAdapterPosition(topChild)
        if (topChildPosition == RecyclerView.NO_POSITION) {
            return
        }
        // get the header position for the child at the top
        val headerPos = mListener.getHeaderPositionForItem(topChildPosition)
        /*
         * check if the element at the header position is really a header
         * can be false if the list doesn't contains an header
         */
        if (mListener.isHeader(headerPos)) {
            // get the header at the header position
            val currentHeader = getHeaderViewForItem(headerPos, parent)
            fixLayoutSize(parent, currentHeader)

            // move the header if it got in contact with another header
            val contactPoint = currentHeader.bottom
            val childInContact = getChildInContact(parent, contactPoint, headerPos)
            if (childInContact != null && mListener.isHeader(parent.getChildAdapterPosition(childInContact))) {
                moveHeader(c, currentHeader, childInContact)
                return
            }

            // draw the header at the top of the list
            drawHeader(c, currentHeader)
        }
    }

    /**
     * Returns the header view for the header at the [headerPosition] in the list.
     *
     * @param [headerPosition] the header position of the header view which should be returned
     * @param [parent] the list where the header is in
     *
     * @return the header view for the header at the [headerPosition] in the list
     */
    private fun getHeaderViewForItem(headerPosition: Int, parent: RecyclerView): View {
        val layoutResId = mListener.getHeaderLayout(headerPosition)
        val header = LayoutInflater.from(parent.context).inflate(layoutResId, parent, false)
        mListener.bindHeaderData(header, headerPosition)
        return header
    }

    /**
     * Draws the [header] at the top of the list.
     *
     * @param [c] the canvas where the header will be drawn in
     * @param [header] the header which will be drawn at the top of the list
     */
    private fun drawHeader(c: Canvas, header: View) {
        c.save()
        c.translate(0f, 0f)
        header.draw(c)
        c.restore()
    }

    /**
     * Moves the header if it got in contact with another header.
     *
     * @param [c] the canvas where the header will be moved in
     * @param [currentHeader] the current header which will be moved
     * @param [nextHeader] the next header where the current one got in contact with
     */
    private fun moveHeader(c: Canvas, currentHeader: View, nextHeader: View) {
        c.save()
        c.translate(0f, nextHeader.top - currentHeader.height.toFloat())
        currentHeader.draw(c)
        c.restore()
    }

    /**
     * Returns the view which is in contact with the [contactPoint] which is the currents header bottom.
     *
     * @param [parent] the list where the items are in
     * @param [contactPoint] the point where the view should be in contact with
     * @param [currentHeaderPos] the position of the current header
     *
     * @return the view which is in contact with the [contactPoint] which is the currents header bottom
     */
    private fun getChildInContact(parent: RecyclerView, contactPoint: Int, currentHeaderPos: Int): View? {
        var childInContact: View? = null
        for (i in 0 until parent.childCount) {
            var heightTolerance = 0
            val child = parent.getChildAt(i)

            // measure height tolerance with child if child is another header
            if (currentHeaderPos != i) {
                val isChildHeader = mListener.isHeader(parent.getChildAdapterPosition(child))
                if (isChildHeader) {
                    heightTolerance = mStickyHeaderHeight - child.height
                }
            }

            // add heightTolerance if child top be in display area
            val childBottomPosition = if (child.top > 0) {
                child.bottom + heightTolerance
            } else {
                child.bottom
            }
            if (childBottomPosition > contactPoint) {
                if (child.top <= contactPoint) { // This child overlaps the contactPoint
                    childInContact = child
                    break
                }
            }
        }
        return childInContact
    }


    /**
     * Properly measures and layouts the top sticky header.
     *
     * @param [parent] the view group where the header is in
     * @param [view] the view which should be measured
     */
    private fun fixLayoutSize(parent: ViewGroup, view: View) {
        val widthSpec = View.MeasureSpec.makeMeasureSpec(parent.width, View.MeasureSpec.EXACTLY)
        val heightSpec = View.MeasureSpec.makeMeasureSpec(parent.height, View.MeasureSpec.UNSPECIFIED)

        // Specs for children (headers)
        val childWidthSpec = ViewGroup.getChildMeasureSpec(widthSpec, parent.paddingLeft + parent.paddingRight,
            view.layoutParams.width)
        val childHeightSpec = ViewGroup.getChildMeasureSpec(heightSpec, parent.paddingTop + parent.paddingBottom,
            view.layoutParams.height)
        view.measure(childWidthSpec, childHeightSpec)
        view.layout(0, 0, view.measuredWidth, view.measuredHeight.also { mStickyHeaderHeight = it })
    }
}