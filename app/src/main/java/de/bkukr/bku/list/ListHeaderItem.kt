package de.bkukr.bku.list

// Copyright (C) 2020  Sven Op de Hipt
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

/**
 * A header item in the list.
 *
 * @author Sven Op de Hipt
 *
 * @constructor Creates a header item for the list with a [text].
 *
 * @param [text] the text which will be shown in the row
 */
class ListHeaderItem(override val text: String): ListItem {
    /**
     * The item type ([RowType.HEADER_ITEM]).
     */
    override val itemType = RowType.HEADER_ITEM.ordinal
    /**
     * The custom text color for header which is always null.
     */
    override val textColor: Int? = null
}