package de.bkukr.bku.list

import android.view.View

// Copyright (C) 2020  Sven Op de Hipt
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

/**
 * The interface which forces it's subclasses to implement the methods which draw the header at the top of the list.
 *
 * @author Sven Op de Hipt
 */
interface ListStickyHeaderInterface {
    /**
     * This method gets called by [ListItemDecorator] to fetch the position of the header item in the adapter
     * that is used for (represents) item at specified position.
     *
     * @param [itemPosition] adapter's position of the item for which to do the search of the position of the header item.
     *
     * @return position of the header item in the adapter.
     */
    fun getHeaderPositionForItem(itemPosition: Int): Int

    /**
     * This method gets called by [ListItemDecorator] to get layout resource id for the header item at specified
     * adapter's position.
     *
     * @param [headerPosition] position of the header item in the adapter.
     *
     * @return the layout resource id.
     */
    fun getHeaderLayout(headerPosition: Int): Int

    /**
     * This method gets called by [ListItemDecorator] to setup the header View.
     *
     * @param [header] header to set the data on.
     *
     * @param [headerPosition] position of the header item in the adapter.
     */
    fun bindHeaderData(header: View?, headerPosition: Int)

    /**
     * This method gets called by [ListItemDecorator] to verify whether the item represents a header.
     *
     * @param itemPosition the position of the item
     *
     * @return true, if item at the specified adapter's position represents a header.
     */
    fun isHeader(itemPosition: Int): Boolean
}