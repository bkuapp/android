package de.bkukr.bku.handler

import android.util.Base64
import kotlinx.serialization.Serializable
import kotlinx.serialization.json.Json
import kotlinx.serialization.json.JsonConfiguration
import java.nio.charset.Charset

// Copyright (C) 2020  Sven Op de Hipt
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

/**
 * Handles the json web token.
 *
 * @author Sven Op de Hipt
 */
object JWTHandler {
    /**
     * Decodes the [jwt] and returns it as [JWTPayload].
     *
     * @param [jwt] the json web token which should be decoded
     *
     * @return the decoded [jwt] as [JWTPayload]
     */
    fun decodeJWT(jwt: String?): JWTPayload? {
        val parts = jwt?.split(".")

        return if (!parts.isNullOrEmpty()) {
            val payload = base64Decode(parts[1])

            val json = Json { allowStructuredMapKeys = true }
            try {
                json.decodeFromString(JWTPayload.serializer(), payload)
            } catch (e: Exception) {
                null
            }
        }
        else {
            null
        }
    }

    /**
     * Base64Decodes a [string] and returns it.
     *
     * @param [string] the string which should be decoded
     *
     * @return the Base64Decoded [string]
     */
    private fun base64Decode(string: String): String {
        return String(Base64.decode(string, Base64.DEFAULT), Charset.forName("UTF-8"))
    }

    /**
     * Represents a jwt payload.
     *
     * @constructor Creates a jwt payload with the given [exp] date, the [id] and the [type].
     *
     * @param exp the date when the jwt expires
     * @param id the id of the user the jwt belongs to
     * @param type the type of the user the jwt belongs to (teacher or student)
     */
    @Serializable
    data class JWTPayload(val exp: Int, val id: Int, val type: String)
}