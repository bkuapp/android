package de.bkukr.bku.handler

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.graphics.*
import android.net.Uri
import android.util.Log
import androidx.core.content.FileProvider
import de.bkukr.bku.R
import de.bkukr.bku.general.getColorFromAttr
import de.bkukr.bku.representation.DetailRepresentation
import java.io.File
import java.io.FileOutputStream
import java.io.IOException
import kotlin.math.round

// Copyright (C) 2020  Sven Op de Hipt
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

/**
 * Handles the generation of the representation image.
 *
 * @author Sven Op de Hipt
 */
internal object ImageHandler {
    /**
     * Generates an image from a [representation].
     *
     * @param [context] the context of the fragment which is creating the image.
     * @param [representation] the representation which should be converted to an image.
     *
     * @return the generated image
     */
    fun generateImage(context: Activity, representation: DetailRepresentation): Bitmap {
        // splits the representation rules and calculate the rows per entry and the number of rows
        val representationRulesPair = representation.representationRules.replace(' ', '\n', 20)
        val representationRulesInfo = representationRulesPair.first

        var rowsPerEntry = representationRulesPair.second.toDouble() * 3 / 4
        if (representationRulesPair.second == 1 && representation.course.contains('-')) {
            rowsPerEntry += 3 / 4
        }

        val numberOfRow = rowsPerEntry * representation.hours.size + 3

        // calculate the width and height of the image and the height per row
        val size = Point()
        context.windowManager.defaultDisplay.getSize(size)
        var width = size.x

        var heightPerRow = width / 18.545454545454545
        var expandedHeight = 2 * heightPerRow

        var height = numberOfRow * heightPerRow

        if (height > size.y) {
            height = size.y.toDouble()
            heightPerRow = height / numberOfRow
            expandedHeight = 2 * heightPerRow
            width = (heightPerRow * 18.545454545454545).toInt()
        }

        val heightPerEntry = heightPerRow * rowsPerEntry

        // initializes the colors
        val color = Paint()
        color.textSize = width / 40f
        color.isDither = true
        color.isAntiAlias = true

        val black = Paint(color)
        black.color = context.getColorFromAttr(android.R.attr.textColor)

        val blue = Paint(color)
        blue.color = context.getColorFromAttr(R.attr.representationPlanTitleColor)

        val image = Bitmap.createBitmap(width, height.toInt(), Bitmap.Config.ARGB_8888)

        val backgroundColor = Paint(color)
        backgroundColor.color = context.getColorFromAttr(R.attr.representationPlanMainColor)
        backgroundColor.typeface = Typeface.create(Typeface.DEFAULT, Typeface.BOLD)

        // sets the row centers
        val rows = DoubleArray(9)
        rows[0] = width / 2.0
        rows[1] = width / 14.30617283950617
        rows[2] = width / 3.3615837301587302
        rows[3] = width / 4.728724279835391
        rows[4] = width / 3.008854748603352
        rows[5] = width / 2.572407407407407
        rows[6] = width / 1.8780986001913171
        rows[7] = width / 1.331239722370529
        rows[8] = width / 1.056895353886235

        // sets the row widths
        val rowWidth = DoubleArray(8)
        rowWidth[0] = rows[1] * 2
        rowWidth[1] = (rows[5] - rows[4] + rows[3] - rowWidth[0]) * 2.55
        rowWidth[2] = rowWidth[1] / 2
        rowWidth[3] = rowWidth[1] / 4
        rowWidth[4] = rowWidth[3]
        rowWidth[5] = rowWidth[0]
        rowWidth[6] = rowWidth[1] * 0.9
        rowWidth[7] = width - rowWidth[0] - rowWidth[1] - rowWidth[5] - rowWidth[6]

        // sets the row beginnings
        val rowBegin = DoubleArray(7)
        rowBegin[0] = 0.0
        for (i in 0 until 6) {
            val secondIndex = if (i == 0) {
                i
            } else {
                i + 1
            }
            rowBegin[i + 1] = (rowBegin[i] + rowWidth[secondIndex])
        }

        // set the line beginnings
        val lineBegin = ArrayList<Double>()
        lineBegin.add(0.0)
        for (i in 0..2) {
            lineBegin.add(lineBegin[i] + heightPerRow)
        }

        // sets the line centers
        val lines = ArrayList<Double>()
        lines.add(heightPerRow * 0.5)
        for (i in 0..2) {
            when (i) {
                0 -> lines.add(heightPerRow * 1.5)
                1 -> lines.add(expandedHeight / 2 + heightPerRow)
                else -> lines.add(heightPerRow * (i + 0.5))
            }
        }
        lines.add((lines.last() + (heightPerEntry / heightPerRow + 1) / 2.0 * heightPerRow) + heightPerRow * 0.35)

        val editImage = Canvas(image)

        // draw the header rectangles and background
        editImage.drawRect(rowBegin[0].toFloat(), lineBegin[0].toFloat(), rowBegin[0].toFloat() + width,
            (lineBegin[0] + heightPerRow).toFloat(), black)
        editImage.drawRect(rowBegin[0].toFloat() - 1, lineBegin[0].toFloat() - 1,
            rowBegin[0].toFloat() + width - 1, (lineBegin[0] + heightPerRow).toFloat() - 1, blue)

        editImage.drawRect(rowBegin[0].toFloat(), lineBegin[1].toFloat(), (rowBegin[0] + rowWidth[0]).toFloat(),
            (lineBegin[1] + expandedHeight).toFloat(), black)
        editImage.drawRect(rowBegin[0].toFloat() - 1, lineBegin[1].toFloat(),
            (rowBegin[0] + rowWidth[0]).toFloat() - 1, (lineBegin[1] + expandedHeight).toFloat() - 1,
            backgroundColor)

        editImage.drawRect(rowBegin[1].toFloat(), lineBegin[1].toFloat(), (rowBegin[1] + rowWidth[1]).toFloat(),
            (lineBegin[1] + heightPerRow).toFloat(), black)
        editImage.drawRect(rowBegin[1].toFloat(), lineBegin[1].toFloat(), (rowBegin[1] + rowWidth[1]).toFloat() - 1,
            (lineBegin[1] + heightPerRow).toFloat() - 1, backgroundColor)

        editImage.drawRect(rowBegin[1].toFloat(), lineBegin[2].toFloat(), (rowBegin[1] + rowWidth[2]).toFloat(),
            (lineBegin[2] + heightPerRow).toFloat(), black)
        editImage.drawRect(rowBegin[1].toFloat(), lineBegin[2].toFloat(), (rowBegin[1] + rowWidth[2]).toFloat() - 1,
            (lineBegin[2] + heightPerRow).toFloat() - 1, backgroundColor)

        editImage.drawRect(rowBegin[2].toFloat(), lineBegin[2].toFloat(), (rowBegin[2] + rowWidth[3]).toFloat(),
            (lineBegin[2] + heightPerRow).toFloat(), black)
        editImage.drawRect(rowBegin[2].toFloat(), lineBegin[2].toFloat(), (rowBegin[2] + rowWidth[3]).toFloat() - 1,
            (lineBegin[2] + heightPerRow).toFloat() - 1, backgroundColor)

        editImage.drawRect(rowBegin[3].toFloat(), lineBegin[2].toFloat(), (rowBegin[3] + rowWidth[4]).toFloat(),
            (lineBegin[2] + heightPerRow).toFloat(), black)
        editImage.drawRect(rowBegin[3].toFloat(), lineBegin[2].toFloat(), (rowBegin[3] + rowWidth[4]).toFloat() - 1,
            (lineBegin[2] + heightPerRow).toFloat() - 1, backgroundColor)

        editImage.drawRect(rowBegin[4].toFloat(), lineBegin[1].toFloat(), (rowBegin[4] + rowWidth[5]).toFloat(),
            (lineBegin[1] + expandedHeight).toFloat(), black)
        editImage.drawRect(rowBegin[4].toFloat(), lineBegin[1].toFloat(), (rowBegin[4] + rowWidth[5]).toFloat() - 1,
            (lineBegin[1] + expandedHeight).toFloat() - 1, backgroundColor)

        editImage.drawRect(rowBegin[5].toFloat(), lineBegin[1].toFloat(), (rowBegin[5] + rowWidth[6]).toFloat(),
            (lineBegin[1] + expandedHeight).toFloat(), black)
        editImage.drawRect(rowBegin[5].toFloat(), lineBegin[1].toFloat(), (rowBegin[5] + rowWidth[6]).toFloat() - 1,
            (lineBegin[1] + expandedHeight).toFloat() - 1, backgroundColor)

        editImage.drawRect(rowBegin[6].toFloat(), lineBegin[1].toFloat(), (rowBegin[6] + rowWidth[7]).toFloat(),
            (lineBegin[1] + expandedHeight).toFloat(), black)
        editImage.drawRect(rowBegin[6].toFloat(), lineBegin[1].toFloat(), (rowBegin[6] + rowWidth[7]).toFloat() - 1,
            (lineBegin[1] + expandedHeight).toFloat() - 1, backgroundColor)

        val headline = context.getString(R.string.representationForClass) + " " + representation.nameOfClass
        val date = context.getString(R.string.date)
        val plannedLesson = context.getString(R.string.plannedLesson)
        val teacher = context.getString(R.string.teacher)
        val hourText = context.getString(R.string.hour)
        val course = context.getString(R.string.course)
        val representationThrough = context.getString(R.string.representationThrough)
        val representationRules = context.getString(R.string.representationRule)
        val inRoom = context.getString(R.string.inRoom)

        // draw the header string
        drawString(editImage, headline, rows[0].toFloat(), (lines[0] + heightPerRow / 3f * 1.3).toFloat(), backgroundColor)
        drawString(editImage, date, rows[1].toFloat(), (lines[2] + heightPerRow / 3f).toFloat(), black)
        drawString(editImage, plannedLesson, rows[2].toFloat() * 1.01f, (lines[1] + heightPerRow / 3f * 1.2).toFloat(),
            black)
        drawString(editImage, teacher, rows[3].toFloat() * 1.03f, (lines[3] + heightPerRow / 3f).toFloat(), black)
        drawString(editImage, hourText, rows[4].toFloat() * 1.03f, (lines[3] + heightPerRow / 3f).toFloat(), black)
        drawString(editImage, course, rows[5].toFloat() * 1.085f, (lines[3] + heightPerRow / 3f).toFloat(), black)
        drawString(editImage, representationThrough, rows[6].toFloat(), (lines[2] + heightPerRow / 3f).toFloat(), black)
        drawString(editImage, representationRules, rows[7].toFloat(), (lines[2] + heightPerRow / 3f).toFloat(), black)
        drawString(editImage, inRoom, rows[8].toFloat(), (lines[2] + heightPerRow / 3f).toFloat(), black)

        for (hour in representation.hours) {
            val lastLineBegin = lineBegin.last().toFloat()
            val lastLine = lines.last().toFloat()

            // draw the representation rectangles and background
            editImage.drawRect(rowBegin[0].toFloat(), lastLineBegin, (rowBegin[0] + rowWidth[0]).toFloat(),
                lastLineBegin + heightPerEntry.toFloat(), black)
            editImage.drawRect(rowBegin[0].toFloat() - 1, lastLineBegin,
                (rowBegin[0] + rowWidth[0]).toFloat() - 1, lastLineBegin + heightPerEntry.toFloat() - 1,
                backgroundColor)

            editImage.drawRect(rowBegin[1].toFloat(), lastLineBegin, (rowBegin[1] + rowWidth[2]).toFloat(),
                lastLineBegin + heightPerEntry.toFloat(), black)
            editImage.drawRect(rowBegin[1].toFloat(), lastLineBegin, (rowBegin[1] + rowWidth[2]).toFloat() - 1,
                lastLineBegin + heightPerEntry.toFloat() - 1, backgroundColor)

            editImage.drawRect(rowBegin[2].toFloat(), lastLineBegin, (rowBegin[2] + rowWidth[3]).toFloat(),
                lastLineBegin + heightPerEntry.toFloat(), black)
            editImage.drawRect(rowBegin[2].toFloat(), lastLineBegin, (rowBegin[2] + rowWidth[3]).toFloat() - 1,
                lastLineBegin + heightPerEntry.toFloat() - 1, backgroundColor)

            editImage.drawRect(rowBegin[3].toFloat(), lastLineBegin, (rowBegin[3] + rowWidth[4]).toFloat(),
                lastLineBegin + heightPerEntry.toFloat(), black)
            editImage.drawRect(rowBegin[3].toFloat(), lastLineBegin, (rowBegin[3] + rowWidth[4]).toFloat() - 1,
                lastLineBegin + heightPerEntry.toFloat() - 1, backgroundColor)

            editImage.drawRect(rowBegin[4].toFloat(), lastLineBegin, (rowBegin[4] + rowWidth[5]).toFloat(),
                lastLineBegin + heightPerEntry.toFloat(), black)
            editImage.drawRect(rowBegin[4].toFloat(), lastLineBegin, (rowBegin[4] + rowWidth[5]).toFloat() - 1,
                lastLineBegin + heightPerEntry.toFloat() - 1, backgroundColor)

            editImage.drawRect(rowBegin[5].toFloat(), lastLineBegin, (rowBegin[5] + rowWidth[6]).toFloat(),
                lastLineBegin + heightPerEntry.toFloat(), black)
            editImage.drawRect(rowBegin[5].toFloat(), lastLineBegin, (rowBegin[5] + rowWidth[6]).toFloat() - 1,
                lastLineBegin + heightPerEntry.toFloat() - 1, backgroundColor)

            editImage.drawRect(rowBegin[6].toFloat(), lastLineBegin, (rowBegin[6] + rowWidth[7]).toFloat(),
                lastLineBegin + heightPerEntry.toFloat(), black)
            editImage.drawRect(rowBegin[6].toFloat(), lastLineBegin, (rowBegin[6] + rowWidth[7]).toFloat() - 1,
                lastLineBegin + heightPerEntry.toFloat() - 1, backgroundColor)

            val dateInfo = representation.date
            val teacherInfo = representation.teacher
            val courseInfo = representation.course.replace("-", "-\n")
            val representationThroughInfo = representation.representation
            val inRoomInfo = representation.room

            // draw the representation texts
            drawString(editImage, dateInfo, rows[1].toFloat(), lastLine, black)
            drawString(editImage, teacherInfo, rows[3].toFloat() * 1.03f, lastLine, black)
            drawString(editImage, hour, rows[4].toFloat() * 1.03f, lastLine, black)
            drawString(editImage, courseInfo, rows[5].toFloat() * 1.085f, lastLine, black)
            drawString(editImage, representationThroughInfo, rows[6].toFloat(), lastLine, black)
            drawString(editImage, representationRulesInfo, rows[7].toFloat(), lastLine, black)

            if (inRoomInfo != null) {
                drawString(editImage, inRoomInfo, rows[8].toFloat(), lastLine, black)
            }

            lineBegin.add(lineBegin.last() + heightPerEntry)
            lines.add(lines.last() + heightPerEntry)
        }

        return image
    }

    /**
     * Draws the [string] at the [x] and [y] position on the [editImage] with the given [color].
     *
     * @param [editImage] the image on which the string should be drawn.
     * @param [string] the string which should be drawn.
     * @param [x] the x position where the string should be drawn.
     * @param [y] the y position where the string should be drawn.
     * @param [color] the color in which the string should be drawn.
     */
    private fun drawString(editImage: Canvas, string: String, x: Float, y: Float, color: Paint) {
        if (string.contains("\n")) {
            val parts = string.split("\n".toRegex()).dropLastWhile { it.isEmpty() }.toTypedArray()

            val part1Size = Rect()
            color.getTextBounds(parts[0], 0, parts[0].length, part1Size)

            val length = parts.size

            var current = y - part1Size.height() * (length + 1) / 2f

            for (part in parts) {
                val partSize = Rect()
                color.getTextBounds(part, 0, part.length, partSize)

                val partHeight = partSize.height().toFloat()

                editImage.drawText(part, x - partSize.width() / 2f, current + partHeight / 2f, color)

                current += partHeight + 5
            }
        } else {
            val stringSize = Rect()
            color.getTextBounds(string, 0, string.length, stringSize)

            var onlyMinus = true
            for (c in string.toCharArray()) {
                if (c != '-') {
                    onlyMinus = false
                }
            }

            val height = if (onlyMinus) {
                15f
            } else {
                stringSize.height() / 2f
            }
            editImage.drawText(string, x - stringSize.width() / 2f, y - height, color)
        }
    }

    /**
     * Shares the representation [image] via share sheet.
     * @param [context] the context from which the image will be shared
     * @param [image] the image which should be shared
     */
    fun shareImage(context: Context, image: Uri) {
        val intent = Intent(Intent.ACTION_SEND)
        intent.putExtra(Intent.EXTRA_STREAM, image)
        intent.addFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION)
        intent.type = "image/jpg"
        context.startActivity(Intent.createChooser(intent, context.getString(R.string.share)))
    }

    /**
     * Saves the [image] in the cache to share it later.
     *
     * @param [context] the context which the cache belongs to
     * @param [image] the image should will be saved in cache
     *
     * @return the uri of the cached image
     */
    fun saveImageInCache(context: Context, image: Bitmap) =
        try {
            val imagesFolder = File(context.cacheDir, "images")
            if (imagesFolder.exists() || imagesFolder.mkdirs()) {
                val file = File(imagesFolder, context.getString(R.string.representationPlan) + ".jpg")

                val stream = FileOutputStream(file)
                image.compress(Bitmap.CompressFormat.PNG, 90, stream)
                stream.flush()
                stream.close()
                FileProvider.getUriForFile(context, "de.bkukr.bku", file)
            } else {
                Log.w("Directory Error", "Couldn't create directory in cache: images")
                null
            }
        } catch (e: IOException) {
            Log.w("File Saving Error", "IOException while trying to write file for sharing: " + e.message)
            null
        }

    /**
     * Replaced the nearest occurrences of [search] with [replace] every [charCount] chars.
     *
     * @param [search] the character which should be replaced
     * @param [replace] the character which replaces [search]
     * @param [charCount] the count where the [search] char should be replaced with the [replace] char
     *
     * @return the string with the replaces chars
     */
    private fun String.replace(search: Char, replace: Char, charCount: Int): Pair<String, Int> {
        if (count() > charCount) {
            val chars = this.toCharArray()

            var counter = 1

            val last = round(count().toDouble()/charCount).toInt() + 1
            for (i in 1 until last) {
                for (j in charCount * i downTo charCount * (i - 1)) {
                    if (j < chars.count() && chars[j] == search) {
                        chars[j] = replace
                        counter++
                        break
                    }
                }
            }

            val ret = String(chars)

            return ret to counter
        }
        else {
            return this to 1
        }
    }
}
