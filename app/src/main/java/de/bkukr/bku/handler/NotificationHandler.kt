package de.bkukr.bku.handler

import android.app.AlarmManager
import android.app.Notification
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.os.Build
import android.os.Bundle
import androidx.core.app.NotificationCompat
import androidx.core.app.NotificationManagerCompat
import androidx.preference.PreferenceManager
import de.bkukr.bku.R
import de.bkukr.bku.general.MainActivity
import de.bkukr.bku.network.JSONHandlerPhone
import de.bkukr.bku.publisher.NotificationPublisher
import kotlinx.serialization.builtins.ListSerializer
import kotlinx.serialization.builtins.list
import java.util.*

// Copyright (C) 2020  Sven Op de Hipt
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

/**
 * Creates notifications and show it to the user.
 *
 * @author Sven Op de Hipt
 */
internal object NotificationHandler {
    /**
     * Shows a [notification] to the user from the given [context].
     *
     * @param [context] the context from which the [notification] will be send
     * @param [notification] the notification which will be send
     */
    fun sendNotification(context: Context, notification: Notification) {
        val notificationID =
            getNotificationID(context)
        val notificationManager = NotificationManagerCompat.from(context)
        // notificationId is a unique int for each notification that you must define
        notificationManager.notify(notificationID, notification)
    }

    /**
     * Schedules the exam notifications.
     *
     * @param [context] the context from which the exam notifications should be scheduled
     */
    fun setupExamNotification(context: Context) {
        val general = PreferenceManager.getDefaultSharedPreferences(context)
        val options = context.resources.getStringArray(R.array.notificationTimes)
        val currentOption = general.getString("notificationExamTime", options[5])
        val currentOptionIndex = options.indexOf(currentOption)

        val params = mutableMapOf("mode" to currentOptionIndex.toString())

        // loads the notification data from the server
        val serializer = ListSerializer(NotificationExam.serializer())
        JSONHandlerPhone.getJSON(context, 105, serializer, params) { exams ->
            val currentIDS = general.getStringSet("examNotificationIDs", mutableSetOf())

            for (exam in exams) {
                val timeStamp = exam.notificationTimestamp

                val cal = Calendar.getInstance(Locale.GERMAN)
                cal.timeInMillis = timeStamp * 1000
                val date = exam.examDate

                val id = exam.id
                val intent = Intent(context, MainActivity::class.java)
                intent.putExtra("type", "exam")
                intent.putExtra("id", id)
                intent.action = "Exam Notification Click"

                val message = context.getString(R.string.at, exam.course, date)
                sendDelayedNotification(context, intent, context.getString(R.string.approachingExams),
                    message, id, intent.extras, timeStamp)
                currentIDS?.add(id.toString())
            }

            // saves the exam notification ids to be able to cancel them later
            general.edit().putStringSet("examNotificationIDs", currentIDS).apply()
        }
    }

    /**
     * Cancels all exam notifications.
     * @param [context] the context from which the exam notifications should be cancelled
     */
    fun cancelExamNotification(context: Context) {
        val general = PreferenceManager.getDefaultSharedPreferences(context)

        val currentIDS = general.getStringSet("examNotificationIDs", emptySet())
        if (currentIDS != null) {
            for (id in currentIDS) {
                cancelDelayedNotification(context, id.toInt())
            }

            general.edit().putStringSet("examNotificationIDs", mutableSetOf()).apply()
        }
    }

    /**
     * Creates a notification with the given [context], the [destination], the [title], the [id], the [message]
     * and the [extras].
     *
     * @param [context] the context of the notification
     * @param [destination] the destination of the intent
     * @param [title] the title of the notification
     * @param [id] the id of the notification
     * @param [message] the message of the notification
     * @param [extras] the extras of the notification
     *
     * @return the created notification
     */
    fun getNotification(context: Context, destination: Intent?, title: String, id: Int, message: String,
                        extras: Bundle?): Notification {
        val builder = NotificationCompat.Builder(context, "1")
            .setSmallIcon(R.drawable.ic_notification_icon)
            .setContentTitle(title)
            .setContentText(message)
            .setContentIntent(generateIntent(context, destination, id, extras))
            .setVisibility(NotificationCompat.VISIBILITY_PUBLIC)
            .setAutoCancel(true)

        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.O) {
            builder.priority = NotificationCompat.PRIORITY_DEFAULT
        }
        return builder.build()
    }

    /**
     * Returns an unique id for each notification.
     *
     * @param [context] the context which saves the last notification id
     *
     * @return a unique id for each notification
     */
    private fun getNotificationID(context: Context): Int {
        val notificationPref = context.getSharedPreferences("notification", Context.MODE_PRIVATE)
        val newNotificationID = notificationPref.getInt("lastID", -1) + 1
        notificationPref.edit().putInt("lastID", newNotificationID).apply()
        return newNotificationID
    }

    /**
     * Generates a pending intent from an [destination] intent for a notification.
     *
     * @param [context] the context which the pending intent will belong to
     * @param [destination] the intent which should be converted
     * @param [id] the id of the pending intent
     * @param [extras] the extras of the pending intent
     *
     * @return the generated pending intent
     */
    private fun generateIntent(context: Context, destination: Intent?, id: Int, extras: Bundle?): PendingIntent? {
        return if (destination != null) {
            PendingIntent.getActivity(context, id, destination, PendingIntent.FLAG_UPDATE_CURRENT, extras)
        }
        else {
            null
        }
    }

    /**
     * Shows a notification to the user at a specific [timeStamp]
     *
     * @param [context] the context of the delayed notification
     * @param [notificationIntent] the intent ot the delayed notification
     * @param [title] the title of the delayed notification
     * @param [message] the message of the delayed notification
     * @param [id] the id of the delayed notification
     * @param [extras] the the extras of the delayed notification
     * @param [timeStamp] the time stamp when the delayed notification should be shown
     */
    private fun sendDelayedNotification(context: Context, notificationIntent: Intent?, title: String, message: String,
                                        id: Int, extras: Bundle?, timeStamp: Long) {
        val timeStampInMillis = timeStamp * 1000
        if (System.currentTimeMillis() < timeStampInMillis) {
            val alarmIntent = Intent(context, NotificationPublisher::class.java)
            alarmIntent.action = "Exam Notification"
            alarmIntent.putExtra(NotificationPublisher.INTENT, notificationIntent)
            alarmIntent.putExtra(NotificationPublisher.TITLE, title)
            alarmIntent.putExtra(NotificationPublisher.ID, id)
            alarmIntent.putExtra(NotificationPublisher.MESSAGE, message)
            alarmIntent.putExtra(NotificationPublisher.EXTRAS, extras)
            val pendingAlarmIntent = PendingIntent.getBroadcast(context, id, alarmIntent, PendingIntent.FLAG_UPDATE_CURRENT)

            val alarmManager = context.getSystemService(Context.ALARM_SERVICE) as? AlarmManager
            alarmManager?.cancel(pendingAlarmIntent)
            alarmManager?.set(AlarmManager.RTC_WAKEUP, timeStampInMillis, pendingAlarmIntent)
        }
    }

    /**
     * Cancels an delayed notification with a specific [id].
     *
     * @param [context] the context of the delayed notification
     * @param [id] the id of the delayed notification
     */
    private fun cancelDelayedNotification(context: Context, id: Int) {
        val notificationIntent = Intent(context, NotificationPublisher::class.java)
        notificationIntent.action = "Exam Notification"
        val pendingIntent = PendingIntent.getBroadcast(context, id, notificationIntent, PendingIntent.FLAG_UPDATE_CURRENT)

        val alarmManager = context.getSystemService(Context.ALARM_SERVICE) as? AlarmManager
        alarmManager?.cancel(pendingIntent)
        pendingIntent?.cancel()
    }
}