package de.bkukr.bku.handler

import android.app.Activity
import android.content.Context
import android.content.Intent
import android.content.pm.ShortcutManager
import android.os.Build
import android.view.MenuItem
import androidx.preference.PreferenceManager
import com.google.firebase.messaging.FirebaseMessaging
import com.google.firebase.messaging.FirebaseMessagingService
import de.bkukr.bku.R
import de.bkukr.bku.general.MainActivity
import de.bkukr.bku.network.NetworkHandler
import de.bkukr.bku.settings.SettingsActivity
import java.util.*

// Copyright (C) 2020  Sven Op de Hipt
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

/**
 * Handles the menu events.
 *
 * @author Sven Op de Hipt
 */
internal object MenuHandler {
    /**
     * Handles the menu events when an [item] is clicked in the given [activity].
     *
     * @param [item] the menu item which is clicked
     * @param [activity] the activity in which the item is clicked
     *
     * @return true if the event is handled
     */
    fun onOptionsItemSelected(item: MenuItem, activity: Activity) =
        when (item.itemId) {
            R.id.settings_schedule -> {
                openSettings(activity)
                true
            }
            R.id.logout_schedule -> {
                logout(activity)
                true
            }
            else -> {
                false
            }
        }

    /**
     * Logs the current user out and deletes it notification token from the server
     *
     * @param context the context from which the uer should bei logged out
     */
    fun logout(context: Context) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N_MR1) {
            val shortcutManager = context.getSystemService(ShortcutManager::class.java)
            shortcutManager?.removeAllDynamicShortcuts()
        }

        val sharedPref = PreferenceManager.getDefaultSharedPreferences(context)

        sharedPref.edit().remove("loggedIn").apply()

        val preferences = context.getSharedPreferences("token", FirebaseMessagingService.MODE_PRIVATE)
        val token = preferences.getString("token", null) ?: ""
        val params = mutableMapOf("token" to token)
        NetworkHandler().execute(context, 2, params).get()

        NotificationHandler.cancelExamNotification(context)

        preferences.getString("username", null)?.substring(0 until 4)?.
            toLowerCase(Locale.GERMAN)?.apply {
            FirebaseMessaging.getInstance().unsubscribeFromTopic(this)
        }

        if (context is MainActivity) {
            context.finish()
        }
    }

    /**
     * Opens the settings with given [activity].
     *
     * @param [activity] the activity which should open the settings
     */
    private fun openSettings(activity: Activity) {
        val settings = Intent(activity, SettingsActivity::class.java)
        activity.startActivityForResult(settings, 5)
    }
}
