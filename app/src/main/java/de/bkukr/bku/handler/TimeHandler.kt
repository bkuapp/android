package de.bkukr.bku.handler

// Copyright (C) 2020  Sven Op de Hipt
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

/**
 * Converts schedule lesson to times.
 */
object TimeHandler {
    /**
     * Converts the [start] of a lesson to a time.
     *
     * @param [start] the start time of a lesson
     *
     * @return the time converted from [start] of a lesson
     */
    fun buildStartTime(start: Int) =
        when (start) {
            1 -> "7:30"
            2 -> "8:15"
            3 -> "9:25"
            4 -> "10:10"
            5 -> "11:15"
            6 -> "12:00"
            7 -> "13:00"
            8 -> "13:45"
            9 -> "14:45"
            10 -> "15:30"
            11 -> "16:15"
            12 -> "17:00"
            13 -> "17:45"
            14 -> "18:40"
            15 -> "19:25"
            16 -> "20:15"
            17 -> "21:00"
            else -> ""
        }

    /**
     * Converts the [end] of a lesson to a time.
     *
     * @param [end] the end time of a lesson
     *
     * @return the time converted from the [end] of a lesson
     */
    fun buildEndTime(end: Int) =
        when (end) {
            1 -> "8:15"
            2 -> "9:00"
            3 -> "10:10"
            4 -> "10:55"
            5 -> "12:00"
            6 -> "12:45"
            7 -> "13:45"
            8 -> "14:30"
            9 -> "15:30"
            10 -> "16:15"
            11 -> "17:00"
            12 -> "17:45"
            13 -> "18:30"
            14 -> "19:25"
            15 -> "20:10"
            16 -> "21:00"
            17 -> "21:45"
            else -> ""
        }
}