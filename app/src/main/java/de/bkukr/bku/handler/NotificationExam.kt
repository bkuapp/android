package de.bkukr.bku.handler

import kotlinx.serialization.Serializable

// Copyright (C) 2020  Sven Op de Hipt
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

/**
 * Represents a notification exam.
 *
 * @constructor Creates an notification exam with the given [id], the [course] name, the [notificationTimestamp] when the notification will be shown
 * and the [examDate] when the exam will occur.
 *
 * @param [id] the id of the exam
 * @param [course] the course name ot the exam
 * @param [notificationTimestamp] the timestamp when the notification should be shown
 * @param [examDate] the date of the exam
 *
 * @author Sven Op de Hipt
 */
@Serializable
data class NotificationExam(val id: Int, val course: String, val notificationTimestamp: Long, val examDate: String)