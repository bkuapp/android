package de.bkukr.bku.publisher

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import de.bkukr.bku.handler.NotificationHandler

// Copyright (C) 2020  Sven Op de Hipt
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

/**
 * The publisher which shows the notification to the user to a specific time when it receive a scheduled broadcast.
 *
 * @author Sven Op de Hipt
 */
class NotificationPublisher : BroadcastReceiver() {
    /**
     * Is called when a scheduled broadcast is received.
     *
     * @param [context] the context of the broadcast
     * @param [intent] the intent of the broadcast
     */
    override fun onReceive(context: Context, intent: Intent) {
        val notificationIntent = intent.getParcelableExtra<Intent>(INTENT)
        val title = intent.getStringExtra(TITLE)
        val id = intent.getIntExtra(ID, 0)
        val message = intent.getStringExtra(MESSAGE)
        val extras = intent.getBundleExtra(EXTRAS)

        // create the notification and show it to the user
        if (title != null && message != null) {
            val notification = NotificationHandler.getNotification(context, notificationIntent, title, id, message, extras)

            NotificationHandler.sendNotification(context, notification)
        }
    }

    companion object {
        /**
         * The intent key of the broadcast intent.
         */
        const val INTENT = "intent"
        /**
         * The title key of the broadcast intent.
         */
        const val TITLE = "title"
        /**
         * The id key of the broadcast intent.
         */
        const val ID = "id"
        /**
         * The message key of the broadcast intent.
         */
        const val MESSAGE = "message"
        /**
         * The extras key of the broadcast intent.
         */
        const val EXTRAS = "extras"
    }
}
