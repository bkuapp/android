package de.bkukr.bku.settings

import android.content.SharedPreferences
import android.os.Build
import android.os.Bundle
import android.view.MenuItem
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.app.AppCompatDelegate
import androidx.preference.PreferenceFragmentCompat
import androidx.preference.PreferenceManager
import de.bkukr.bku.R
import de.bkukr.bku.general.isTGStudent
import de.bkukr.bku.handler.NotificationHandler

// Copyright (C) 2020  Sven Op de Hipt
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

/**
 * Shows the settings view to the user.
 *
 * @author Sven Op de Hipt
 */
class SettingsActivity : AppCompatActivity(), SharedPreferences.OnSharedPreferenceChangeListener {
    /**
     * Is created by the system when the activity is created. Loads the settings file as a view.
     * @param [savedInstanceState] the saved instance state
     */
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        val general = PreferenceManager.getDefaultSharedPreferences(this)

        if (Build.VERSION.SDK_INT >= 29) {
            AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_FOLLOW_SYSTEM)
        }
        else {
            val darkTheme = general.getBoolean("darkMode", true)
            if (darkTheme) {
                AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_YES)
            } else {
                AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO)
            }
        }

        general.registerOnSharedPreferenceChangeListener(this)

        setContentView(R.layout.settings_activity)

        // shows the settings fragment
        supportFragmentManager
            .beginTransaction()
            .replace(
                R.id.settings,
                SettingsFragment()
            )
            .commit()

        supportActionBar?.setDisplayHomeAsUpEnabled(true)
    }

    /**
     * Is called by the system if a menu [item] is clicked. Shows the previous view if the back icon is clicked.
     *
     * @param [item] the menu which is being clicked
     *
     * @return true if the event should not be handled by the system.
     */
    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        if (item.itemId == android.R.id.home) {
            finish()
        }

        return super.onOptionsItemSelected(item)
    }

    /**
     * Is called if a setting is changed by the user.
     *
     * @param [sharedPreferences] the shared preferences where the setting changed
     * @param [key] the key of the changed setting
     */
    override fun onSharedPreferenceChanged(sharedPreferences: SharedPreferences, key: String) {
        when (key) {
            "darkMode" -> {
                val darkTheme = sharedPreferences.getBoolean("darkMode", true)
                if (darkTheme) {
                    AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_YES)
                } else {
                    AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO)
                }
            }
            "notificationExam" -> {
                val examNotification = sharedPreferences.getBoolean("notificationExam", true)
                if (examNotification) {
                    NotificationHandler.setupExamNotification(this)
                }
                else {
                    NotificationHandler.cancelExamNotification(this)
                }
            }
            "notificationExamTime" -> {
                // can only be changed when exam notification is activated
                NotificationHandler.setupExamNotification(this)
            }
        }
    }

    /**
     * The settings fragment which shows the settings to the user. Differs between tg settings and normal preferences.
     */
    class SettingsFragment : PreferenceFragmentCompat() {
        /**
         * Is called by the system to create the setting preferences.
         *
         * @param [savedInstanceState] the saved instance states for the fragment
         * @param [rootKey] the root key for the preferences
         */
        override fun onCreatePreferences(savedInstanceState: Bundle?, rootKey: String?) {
            val preferences = if (context?.isTGStudent == true) {
                R.xml.tg_preferences
            }
            else {
                R.xml.normal_preferences
            }
            setPreferencesFromResource(preferences, rootKey)
        }
    }
}