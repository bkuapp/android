package de.bkukr.bku.room

import android.content.Context
import android.os.Bundle
import android.util.TypedValue
import android.view.Gravity
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.TextView
import androidx.fragment.app.Fragment
import de.bkukr.bku.R
import de.bkukr.bku.general.MainActivity
import kotlinx.android.synthetic.main.fragment_room_list.*

// Copyright (C) 2020  Sven Op de Hipt
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

/**
 * Shows the rooms in a list to the user.
 *
 * @author Sven Op de Hipt
 */
class RoomListFragment : Fragment() {
    /**
     * The room data which is used in the list and shown to the user.
     */
    private var rooms: List<Room>
        get() {
            val result = (activity as? MainActivity)?.roomData

            if (result != null) {
                return result
            }
            return emptyList()
        }
        set(teacherData) {
            val currentActivity = activity

            if (currentActivity != null) {
                roomList.adapter = RoomArrayAdapter(currentActivity, teacherData)
                roomList.onItemClickListener = AdapterView.OnItemClickListener { _, _, position, _ ->
                    val params = HashMap<String, String>()
                    params["id"] = teacherData[position].id.toString()
                    (activity as? MainActivity)?.showDetails(params, RoomDetailFragment())
                }
            }
        }

    /**
     * Is called by the system if the view needs to be created.
     *
     * @param [inflater] the layout inflater which converts the resource file to a view
     * @param [container] the view group which contains the view
     * @param [savedInstanceState] the saved instance state for this view
     *
     * @return the created view
     */
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? =
        inflater.inflate(R.layout.fragment_room_list, container, false)

    /**
     * Is called by the system when the activity is created or this view is created first.
     * Loads the room data from the main activity and shows it in a list.
     *
     * @param [savedInstanceState] the saved instance states for the view
     */
    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        rooms = rooms
    }

    /**
     * The array adapter for the rooms which use the room data as an array and returns the right view for each row.
     *
     * @constructor Creates an [RoomArrayAdapter] with the context of the list and the [rooms] which will be shown.
     *
     * @param [context] the context of the list
     * @param [rooms] the rooms which will be shown in the list
     */
    private inner class RoomArrayAdapter(context: Context, private val rooms: List<Room>):
        ArrayAdapter<String>(context, -1, arrayOfNulls(rooms.size)) {

        /**
         * Returns the view for the specified [position].
         *
         * @param [position] the position of the row for which the view should be returned.
         * @param [convertView] the view which will be converted and returned. Can be null (a new view needs to be created then).
         * @param [parent] the view ground where the view will be located in.
         *
         * @return the view for the specified position.
         */
        override fun getView(position: Int, convertView: View?, parent: ViewGroup): View {
            val rowView =
                if (convertView != null || convertView !is TextView) {
                    TextView(context)
                } else {
                    convertView
                }

            val room = rooms[position]

            val leftPadding = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP,10f, resources.displayMetrics).toInt()
            rowView.setPadding(leftPadding, 0, 0, 0)
            rowView.text = room.name
            rowView.height = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP,60f, resources.displayMetrics).toInt()
            rowView.gravity = Gravity.CENTER_VERTICAL
            rowView.setTextSize(TypedValue.COMPLEX_UNIT_SP, 20f)
            return rowView
        }
    }
}
