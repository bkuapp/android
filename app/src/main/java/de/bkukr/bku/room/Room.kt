package de.bkukr.bku.room

import kotlinx.serialization.Serializable

// Copyright (C) 2020  Sven Op de Hipt
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

/**
 * Represents a room in the room list which is shown to the user.
 *
 * @author Sven Op de Hipt
 *
 * @constructor Creates a room with the given [id] and [name].
 *
 * @param [id] the id of the room
 * @param [name] the name of the room
 */
@Serializable
data class Room(val id: Int, val name: String)