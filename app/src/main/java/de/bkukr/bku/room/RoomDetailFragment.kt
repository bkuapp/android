package de.bkukr.bku.room

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import de.bkukr.bku.R
import de.bkukr.bku.general.MainActivity
import de.bkukr.bku.network.JSONHandlerPhone
import de.bkukr.bku.schedule.ScheduleFragment
import de.bkukr.bku.schedule.ScheduleWeek
import java.util.*

// Copyright (C) 2020  Sven Op de Hipt
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

/**
 * Shows the room plan to the user.
 *
 * @author Sven Op de Hipt
 */
class RoomDetailFragment : Fragment() {
    /**
     * Is called by the system if the view needs to be created.
     *
     * @param [inflater] the layout inflater which converts the resource file to a view
     * @param [container] the view group which contains the view
     * @param [savedInstanceState] the saved instance state for this view
     *
     * @return the created view
     */
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? =
        inflater.inflate(R.layout.fragment_room_detail, container, false)

    /**
     * Is called by the system when the activity is created or this view is created first.
     * Loads the room data from the server and shows it in a schedule.
     *
     * @param [savedInstanceState] the saved instance states for the view
     */
    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        val extras = arguments
        val params = extras?.getSerializable("data") as? HashMap<String, String>
        val context = context

        if (params != null && context != null) {
            val fragment = ScheduleFragment()
            fragment.touchGesturesEnabled = false
            activity?.supportFragmentManager?.beginTransaction()?.replace(R.id.scheduleFragment, fragment)?.commit()

            (activity as? MainActivity)?.loadingCircleShown = true

            // loads the room data from the server
            val serializer = ScheduleWeek.serializer()
            JSONHandlerPhone.getJSON(context, 111, serializer, params) { week ->
                fragment.scheduleData = week
                (activity as? MainActivity)?.loadingCircleShown = false
            }
        }
    }
}
