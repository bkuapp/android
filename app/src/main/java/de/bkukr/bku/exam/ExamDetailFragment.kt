package de.bkukr.bku.exam

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import de.bkukr.bku.R
import de.bkukr.bku.general.MainActivity
import de.bkukr.bku.network.JSONHandlerPhone
import kotlinx.android.synthetic.main.fragment_exam_detail.*

// Copyright (C) 2020  Sven Op de Hipt
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

/**
 * Shows the detail exam view to the user.
 *
 * @author Sven Op de Hipt
 */
class ExamDetailFragment : Fragment() {
    /**
     * Is called by the system when the fragment is first created to inflate the layout file.
     *
     * @param [inflater] the layout inflater, which inflates the layout
     * @param [container] the container for the layout
     * @param [savedInstanceState] the saved instance state for the fragment
     *
     * @return the inflated view
     */
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View =
        inflater.inflate(R.layout.fragment_exam_detail, container, false)

    /**
     * Is called by the system every time the fragment is loaded.
     *
     * @param [savedInstanceState] the saved instance state for the fragment
     */
    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        val context = activity ?: context
        val extras = arguments

        if (context != null && extras != null) {
            // convert the data to a map
            val params = extras.getSerializable("data") as? HashMap<String, String>
            if (params != null) {
                (activity as? MainActivity)?.loadingCircleShown = true

                // load the exam data from the server and parse it to the view
                val serializer = DetailExam.serializer()
                JSONHandlerPhone.getJSON(context, 104, serializer, params) { exam ->
                    activity?.title = exam.course
                    dateInfo.text = exam.date
                    roomInfo.text = exam.room
                    timeInfo.text = exam.time
                    (activity as? MainActivity)?.loadingCircleShown = false
                }
            }
        }
    }
}
