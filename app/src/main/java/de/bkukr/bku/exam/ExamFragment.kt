package de.bkukr.bku.exam

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import de.bkukr.bku.R
import de.bkukr.bku.general.MainActivity
import de.bkukr.bku.list.*
import kotlinx.android.synthetic.main.fragment_exam.*

// Copyright (C) 2020  Sven Op de Hipt
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

/**
 * Shows the exam in a list to the user.
 *
 * @author Sven Op de Hipt
 */
class ExamFragment : Fragment() {
    /**
     * the exam data which is the data source for the fragment
     */
    private var examData: List<ExamQuarter>
    get() {
        val result = (activity as? MainActivity)?.examData

        if (result != null) {
            return result
        }
        return emptyList()
    }
    set(examData) {
        // check if the exams are not empty
        if (examData.isNotEmpty()) {
            val items: ArrayList<ListItem> = ArrayList()
            // loop through the quarters
            for (dataPart in examData) {
                val quarterNumber = dataPart.quarterNumber

                // add an header for each quarter
                items.add(ListHeaderItem(getString(R.string.appointment, quarterNumber)))

                val examsPerQuarter = dataPart.data
                // loop through the exams for the quarter and add an item for each
                for (exam in examsPerQuarter) {
                    val course = exam.course.replace('\n', ' ')
                    val date = exam.date.substring(5)
                    val id = exam.id
                    items.add(ListBodyItem(String.format("%s, %s", course, date), id, ExamDetailFragment()))
                }
            }

            // setup the exam list
            val adapter = ListAdapter(activity, items)
            exam_list.adapter = adapter
            exam_list.layoutManager = LinearLayoutManager(activity)
            exam_list.addItemDecoration(ListItemDecorator(adapter))
            exam_list.addItemDecoration(DividerItemDecoration(exam_list.context, DividerItemDecoration.VERTICAL))
        }
    }

    /**
     * is called by the system when the fragment is first created to inflate the layout file
     * @param [inflater] the layout inflater, which inflates the layout
     * @param [container] the container for the layout
     * @param [savedInstanceState] the saved instance state for the fragment
     *
     * @return the inflated view
     */
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View =
        // Inflate the layout for this fragment
        inflater.inflate(R.layout.fragment_exam, container, false)

    /**
     * is called by the system every time the fragment is loaded
     * @param [savedInstanceState] the saved instance state for the fragment
     */
    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        examData = examData
    }
}
