package de.bkukr.bku.exam.widget

import kotlinx.serialization.Serializable

// Copyright (C) 2020  Sven Op de Hipt
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

/**
 * Represents an exam in the exam widget.
 *
 * @author Sven Op de Hipt
 *
 * @constructor Creates an exam with the given [id], the [course] name, the [date], the [time] and the [room].
 * @param [id] the ID of the exam
 * @param [course] the name of the course which the exam is about
 * @param [date] the date of the exam
 * @param [time] the time of the exam
 * @param [room] the room of the exam
 */
@Serializable
data class Exam(val id: Int, val course: String, val date: String, val time: String, val room: String)