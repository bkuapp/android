package de.bkukr.bku.exam.widget

import android.app.PendingIntent
import android.appwidget.AppWidgetManager
import android.appwidget.AppWidgetProvider
import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.widget.RemoteViews
import de.bkukr.bku.R
import de.bkukr.bku.general.MainActivity
import de.bkukr.bku.general.isLoggedIn
import de.bkukr.bku.general.isTGStudent
import de.bkukr.bku.network.JSONHandlerPhone
import kotlinx.serialization.builtins.ListSerializer
import kotlinx.serialization.builtins.list
import kotlin.math.max
import kotlin.math.min
import kotlin.math.roundToInt

// Copyright (C) 2020  Sven Op de Hipt
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

/**
 * Implementation of Exam Widget functionality, which is only accessible for tg students.
 *
 * @author Sven Op de Hipt
 */
class ExamWidget: AppWidgetProvider() {
    private companion object {
        /**
         * All pending intents which are currently active mapped with the id of the widget.
         */
        val currentIntents = mutableMapOf<Int, MutableList<PendingIntent>>()
    }

    /**
     * Is called by the system when the widget is created, resized or changed in any other way and updates the widget.
     *
     * @param [context] the widgets context
     * @param [appWidgetManager] the manager of the widget
     * @param [appWidgetId] the id of the widget
     * @param [newOptions] the updated options of the widget
     */
    override fun onAppWidgetOptionsChanged(context: Context, appWidgetManager: AppWidgetManager, appWidgetId: Int, newOptions: Bundle) {
        super.onAppWidgetOptionsChanged(context, appWidgetManager, appWidgetId, newOptions)

        updateWidget(context, appWidgetManager, appWidgetId)
    }

    /**
     * Is called by the system every 30 minutes to update the widget.
     *
     * @param [context] the widgets context
     * @param [appWidgetManager] the manager of the widget
     * @param [appWidgetIds] the ids of the widgets
     */
    override fun onUpdate(context: Context, appWidgetManager: AppWidgetManager, appWidgetIds: IntArray) {
        // There may be multiple widgets active, so update all of them
        for (appWidgetId in appWidgetIds)
        {
            updateWidget(context, appWidgetManager, appWidgetId)
        }
    }

    /**
     * Is called when the widget must be updated during options changed or auto-updated
     * by [onAppWidgetOptionsChanged] or [onUpdate]
     *
     * @param [context] the widgets context
     * @param [appWidgetManager] the manager of the widget
     * @param [appWidgetId] the id of the widget
     */
    private fun updateWidget(context: Context, appWidgetManager: AppWidgetManager, appWidgetId: Int) {
        // check if the user is logged in
        if (context.isLoggedIn) {
            // check if the user is a tg student
            if (context.isTGStudent) {
                // get exam json from the server
                val serializer = ListSerializer(Exam.serializer())
                JSONHandlerPhone.getJSON(context, 203, serializer) { exams ->
                    val views: RemoteViews

                    // check if the user no any approaching exams
                    if (exams.isEmpty()) {
                        // display info
                        views = RemoteViews(context.packageName, R.layout.widget_info)
                        views.setTextViewText(
                            R.id.info_text,
                            context.getString(R.string.noMoreExams)
                        )
                    }
                    // user has approaching exams
                    else {
                        // get the max height of the widget
                        val options = appWidgetManager.getAppWidgetOptions(appWidgetId)
                        val maxHeight = max(options.getInt(AppWidgetManager.OPTION_APPWIDGET_MAX_HEIGHT),
                            options.getInt(AppWidgetManager.OPTION_APPWIDGET_MIN_HEIGHT))

                        // calculate the row height, the number of rows and the padding for each row
                        val defaultRowHeight = 65f
                        val rows = (maxHeight / defaultRowHeight).roundToInt()
                        val padding = ((maxHeight / rows - defaultRowHeight) / 2f).roundToInt()

                        views = RemoteViews(context.packageName, R.layout.exam_widget)

                        // get all pending intents for the current widget
                        var intents = currentIntents[appWidgetId] ?: mutableListOf()

                        // cancel all pending intents, so that rows, that are partially visible are not clickable
                        for (intent in intents) {
                            intent.cancel()
                        }

                        // clear the pending intents
                        intents = mutableListOf()

                        // reset all texts of the widget, so that texts, which would be partially visible don`t have any text
                        for (i in 0..2) {
                            val ids = getIDs(i + 1, context)
                            views.apply {
                                setTextViewText(ids[1], "")
                                setTextViewText(ids[2], "")
                                setTextViewText(ids[3], "")
                                setTextViewText(ids[4], "")
                            }
                        }

                        for (i in 0 until min(rows, exams.size)) {
                            val exam = exams[i]

                            val ids = getIDs(i + 1, context)

                            // create and set the pending intent for the row
                            val intent = Intent(context, MainActivity::class.java)
                            intent.putExtra("type", "exam")
                            intent.putExtra("id", exam.id)
                            intent.action = "Exam Widget Click"
                            val pendingIntent = PendingIntent.getActivity(context, i, intent, 0)

                            views.setOnClickPendingIntent(ids[0], pendingIntent)

                            // set the padding for the row
                            if (padding > 0) {
                                views.setViewPadding(ids[0], 0, padding, 0, padding)
                            }
                            else {
                                views.setViewPadding(ids[0], 0, 0, 0, 0)
                            }

                            // set the texts for the row
                            views.setTextViewText(ids[1], exam.time.replace('-', '\n'))
                            views.setTextViewText(ids[2], exam.date)
                            views.setTextViewText(ids[3], exam.course.replace('\n', ' '))
                            views.setTextViewText(ids[4], exam.room)
                        }

                        currentIntents[appWidgetId] = intents
                    }

                    appWidgetManager.updateAppWidget(appWidgetId, views)
                }
            }
            // user isn`t a tg student. display info
            else {
                val views = RemoteViews(context.packageName, R.layout.widget_info)
                views.setTextViewText(R.id.info_text, context.getString(R.string.examPlanNotAvailable))

                appWidgetManager.updateAppWidget(appWidgetId, views)
            }
        }
        // user is not logged in. display info
        else {
            val views = RemoteViews(context.packageName, R.layout.widget_info)
            views.setTextViewText(R.id.info_text, context.getString(R.string.pleaseLogin))

            appWidgetManager.updateAppWidget(appWidgetId, views)
        }
    }

    /**
     * Get the text views for a given [row].
     * @param [row] the row for which the ids are
     * @param [context] the context which the ids belong to
     * @return the ids for the [row]
     */
    private fun getIDs(row: Int, context: Context): Array<Int> {
        return arrayOf(getResourceID(row, "", context),
            getResourceID(row, "time", context),
            getResourceID(row, "date", context),
            getResourceID(row, "course_name", context),
            getResourceID(row, "room", context))
    }

    /**
     * Get the resource for a text view in a specific [row] with a specific [type].
     *
     * @sample getResourceID
     *
     * @param [row] the row for which the ids are
     * @param [type] the type of the text view
     * @param [context] the context which the ids belong to
     *
     * @return the id for the text view in the specified [row] with the given [type]
     */
    private fun getResourceID(row: Int, type: String, context: Context) =
        // split into to strings because the _ would be recognized as a part of the variable otherwise
        context.resources.getIdentifier("exam_row$row" + "_$type", "id", context.packageName)
}