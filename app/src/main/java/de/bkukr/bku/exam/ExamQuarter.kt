package de.bkukr.bku.exam

import kotlinx.serialization.Serializable

// Copyright (C) 2020  Sven Op de Hipt
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

/**
 * Represents an exam quarter in the exam list.
 *
 * @author Sven Op de Hipt
 *
 * @constructor Creates an exam section with the given [quarterNumber] and the given exams.
 *
 * @param [quarterNumber] the quarter number of the exam quarter
 * @param [data] the exams of the exam quarter
 */
@Serializable
data class ExamQuarter(val quarterNumber: Int, val data: Array<Exam>) {
    /**
     * Returns true if the [other] object is equal to the current object.
     *
     * @param [other] the other object which will be compared
     *
     * @return true if the [other] object is equal to the current object
     */
    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as ExamQuarter

        if (quarterNumber != other.quarterNumber) return false
        if (!data.contentEquals(other.data)) return false

        return true
    }

    /**
     * Returns the hash code of the current object.
     *
     * @return the hash code of the current object
     */
    override fun hashCode(): Int {
        var result = quarterNumber
        result = 31 * result + data.contentHashCode()
        return result
    }
}