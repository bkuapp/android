package de.bkukr.bku.general

import android.content.Intent
import android.widget.Toast
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import com.google.android.gms.wearable.MessageEvent
import com.google.android.gms.wearable.WearableListenerService
import de.bkukr.bku.login.LoginActivity
import org.json.JSONArray

// Copyright (C) 2020  Sven Op de Hipt
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

/**
 * The service which retrieves messages from the phone with the phone data.
 *
 * @author Sven Op de Hipt
 */
class MessageService : WearableListenerService() {
    /**
     * Is called when a message from the phone is received. Passes this intent to the [LoginActivity].
     *
     * @param [messageEvent] the event which contains the message
     */
    override fun onMessageReceived(messageEvent: MessageEvent?) {
        super.onMessageReceived(messageEvent)

        if (messageEvent != null) {
            when (messageEvent.path) {
                "loggedIn" -> {
                    val data = JSONArray(String(messageEvent.data))

                    when(data.length()) {
                        3 -> {
                            // forwards the data to the login activity
                            val messageIntent = Intent()
                            messageIntent.action = Intent.ACTION_SEND
                            messageIntent.putExtra("loggedIn", data[0].toString().toBoolean())
                            messageIntent.putExtra("username", data[1].toString())
                            messageIntent.putExtra("jwt", data[2].toString())

                            LocalBroadcastManager.getInstance(this).sendBroadcast(messageIntent)
                        }
                        1 -> {
                            Toast.makeText(baseContext, data[0].toString(), Toast.LENGTH_LONG).show()
                        }
                        else -> {
                            super.onMessageReceived(messageEvent)
                        }
                    }
                }
                else -> {
                    super.onMessageReceived(messageEvent)
                }
            }
        } else {
            super.onMessageReceived(messageEvent)
        }
    }
}
