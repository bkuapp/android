package de.bkukr.bku.general

import android.content.Context
import androidx.preference.PreferenceManager

// Copyright (C) 2020  Sven Op de Hipt
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

/**
 * The username of the currently logged in user.
 */
val Context.username: String get() =
    PreferenceManager.getDefaultSharedPreferences(this).getString("username", null) ?: ""

/**
 * If the currently logged in user is a tg student.
 */
val Context.isTGStudent get() = username.startsWith("tg", true)

/**
 * If the currently logged in user is a teacher.
 */
val Context.isTeacher get() = username.startsWith("l-", true)