package de.bkukr.bku.general

import android.content.SharedPreferences
import android.os.Bundle
import android.os.Handler
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleObserver
import androidx.lifecycle.OnLifecycleEvent
import androidx.lifecycle.ProcessLifecycleOwner
import androidx.preference.PreferenceManager
import androidx.wear.widget.drawer.WearableNavigationDrawerView
import de.bkukr.bku.R
import de.bkukr.bku.exam.Exam
import de.bkukr.bku.exam.ExamFragment
import de.bkukr.bku.network.JSONHandlerWatch
import de.bkukr.bku.person.Person
import de.bkukr.bku.person.PersonFragment
import de.bkukr.bku.representation.Representation
import de.bkukr.bku.representation.RepresentationFragment
import de.bkukr.bku.schedule.ScheduleFragment
import de.bkukr.bku.schedule.ScheduleLesson
import kotlinx.android.synthetic.main.activity_main.*
import kotlinx.serialization.builtins.ListSerializer
import kotlinx.serialization.builtins.list
import kotlin.math.min

// Copyright (C) 2020  Sven Op de Hipt
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

/**
 * The main activity which hosts all fragments which show the information to the user.
 *
 * @author Sven Op de Hipt
 */
class MainActivity: FragmentActivity(), WearableNavigationDrawerView.OnItemSelectedListener, LifecycleObserver {
    /**
     * The texts which are shown in the navigation drawer.
     */
    private lateinit var texts: MutableList<Int>
    /**
     * The images which are shown in the navigation drawer.
     */
    private lateinit var images : MutableList<Int>
    /**
     * The fragments which can be accessed from the navigation drawer.
     */
    private lateinit var fragments : MutableList<Fragment>

    /**
     * The lessons which are shown to the user in the schedule list.
     */
    var lessons: List<ScheduleLesson>? = null
    /**
     * The representations which are shown to the user in the representation list.
     */
    var representations: List<Representation>? = null
    /**
     * The exams which are shown to the user in the exam list.
     */
    var exams: List<Exam>? = null
    /**
     * The classes or teachers which are shown to the user in the person list.
     */
    var persons: List<List<Person>>? = null

    /**
     * Is called when the app starts again after it was in the background.
     */
    @OnLifecycleEvent(Lifecycle.Event.ON_START)
    fun onMoveToForeground() {
        loadData()
    }

    /**
     * Is called by the system when the activity is created. Initializes the fragments and the navigation drawer.
     * Shows the schedule fragment at start.
     *
     * @param [savedInstanceState] the saved instance state
     */
    override fun onCreate(savedInstanceState: Bundle?) {
        texts = mutableListOf(R.string.schedule, R.string.representation_plan,
            R.string.exam_list, R.string.teacher_list, R.string.reload)
        images = mutableListOf(R.drawable.ic_schedule_icon, R.drawable.ic_representation_icon,
            R.drawable.ic_exam_icon, R.drawable.ic_teacher_icon, R.drawable.ic_refresh_icon)
        fragments = mutableListOf(ScheduleFragment(), RepresentationFragment(), ExamFragment(), PersonFragment())

        val preferences = PreferenceManager.getDefaultSharedPreferences(this)
        val username = preferences.getString("username", null)

        if (username != null) {
            if (!isTGStudent) {
                texts.remove(R.string.exam_list)
                images.remove(R.drawable.ic_exam_icon)
                fragments.removeAt(2)
            }

            if (isTeacher) {
                texts.remove(R.string.teacher_list)
                texts.add(2, R.string.classes)
            }

        }

        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        // Top navigation drawer
        top_navigation_drawer.apply {
            setAdapter(NavigationDrawerAdapter())
            addOnItemSelectedListener(this@MainActivity)
            controller.peekDrawer()
        }

        loadData {
            setFragment(ScheduleFragment())
        }

        ProcessLifecycleOwner.get().lifecycle.addObserver(this)
    }

    /**
     * Is called when the user clicks the back button. Closes the activity with a result code of zero.
     */
    override fun onBackPressed() {
        setResult(0)
        finish()
    }

    /**
     * Is called if an item at the specified [position] in the navigation drawer is selected.
     *
     * @param [position] the position of the item which was selected.
     */
    override fun onItemSelected(position: Int) {
        when (position) {
            in 0 until fragments.size -> {
                setFragment(fragments[position])
            }
            else -> {
                loadData()
                Handler().postDelayed({
                    top_navigation_drawer.setCurrentItem(fragments.size - 1, true)
                }, 500)
            }
        }
    }

    /**
     * Loads the data from the server and saves it to show it to the user later.
     *
     * @param [onSuccess] the function, which will be called if the server data was loaded successful.
     */
    private fun loadData(onSuccess: () -> Unit = {}) {
        // schedule data
        val lessonSerializer = ListSerializer(ScheduleLesson.serializer())
        JSONHandlerWatch.getJSON(this,200, lessonSerializer) { lessons ->
            this.lessons = lessons

            // representation data
            val representationSerializer = ListSerializer(Representation.serializer())
            JSONHandlerWatch.getJSON(this, 201, representationSerializer) { representations ->
                this.representations = representations

                // person data
                val personSerializer = ListSerializer(ListSerializer(Person.serializer()))
                JSONHandlerWatch.getJSON(this, 5, personSerializer) { persons ->
                    this.persons = persons
                }

                val preferences: SharedPreferences = PreferenceManager.getDefaultSharedPreferences(this)
                val username = preferences.getString("username", null)

                // exam data
                if (username != null && isTGStudent) {
                    val examSerializer = ListSerializer(Exam.serializer())
                    JSONHandlerWatch.getJSON(this, 203, examSerializer) { exams ->
                        this.exams = exams
                        onSuccess()
                    }
                }
                else {
                    onSuccess()
                }
            }
        }
    }

    /**
     * Sets the [newFragment] as the current fragment to show it to the user.
     *
     * @param [newFragment] the new fragment which will replace the current
     */
    private fun setFragment(newFragment: Fragment) {
        supportFragmentManager.beginTransaction().replace(R.id.mainFragment, newFragment).commit()
    }

    /**
     * The adapter for the navigation drawer which lets the user change the fragments.
     */
    private inner class NavigationDrawerAdapter: WearableNavigationDrawerView.WearableNavigationDrawerAdapter() {
        /**
         * Returns the drawable at the specified [position] for the navigation drawer.
         *
         * @param [position] the position for which the drawable should be returned
         *
         * @return the drawable at the specified [position] for the navigation drawer
         */
        override fun getItemDrawable(position: Int) = getDrawable(images[position])

        /**
         * Returns the count of items which will be shown in the navigation drawer.
         *
         * @return the count of items which will be shown in the navigation drawer
         */
        override fun getCount() = min(images.size, texts.size)

        /**
         * Returns the text at the specified [position] for the navigation drawer.
         *
         * @return the text at the specified [position] for the navigation drawer
         */
        override fun getItemText(position: Int) = getString(texts[position])
    }
}