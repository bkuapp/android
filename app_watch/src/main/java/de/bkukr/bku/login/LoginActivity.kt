package de.bkukr.bku.login

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.os.Bundle
import android.support.wearable.activity.WearableActivity
import android.view.View
import androidx.localbroadcastmanager.content.LocalBroadcastManager
import androidx.preference.PreferenceManager
import com.google.android.gms.tasks.Tasks
import com.google.android.gms.wearable.Wearable
import de.bkukr.bku.R
import de.bkukr.bku.general.MainActivity

// Copyright (C) 2020  Sven Op de Hipt
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

/**
 * Shows the login view to the user and retrieves the login data from the user.
 *
 * @author Sven Op de Hipt
 */
class LoginActivity : WearableActivity() {
    /**
     * Is called by the system when the activity is created. Shows the login view to the user.
     *
     * @param [savedInstanceState] the saved instance state
     */
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_login)

        val newFilter = IntentFilter(Intent.ACTION_SEND)
        val messageReceiver = LoginReceiver()

        LocalBroadcastManager.getInstance(this).registerReceiver(messageReceiver, newFilter)

        sendDataToPhone()
    }

    /**
     * Is called when the activity after this sends a result code when finishing.
     *
     * @param [requestCode] the request code of the activity result
     * @param [resultCode] the result code of the activity result
     * @param [data] the data which was transferred with the result
     */
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        if (requestCode == 0 && resultCode == 0) {
            finishAffinity()
        }
    }

    /**
     * Is called by the system if the user tabs on the back button. Closes the app.
     */
    override fun onBackPressed() {
        finishAffinity()
    }

    /**
     * Reloads the data from the phone, when the reload [button] is clicked.
     *
     * @param [button] the button, which was clicked
     */
    fun reloadDataFromPhone(button: View) {
        sendDataToPhone()
    }

    /**
     * Sends data to the phone to retrieve the login data in the [LoginReceiver].
     */
    private fun sendDataToPhone() {
        Thread(
            Runnable {
                val nodeListTask = Wearable.getNodeClient(applicationContext).connectedNodes
                val nodes = Tasks.await(nodeListTask)
                for (node in nodes) {
                    Wearable.getMessageClient(this@LoginActivity).sendMessage(node.id, "loggedIn", "loggedIn".toByteArray())

                }
            }
        ).start()
    }

    /**
     * Receives the login data response from the phone.
     */
    inner class LoginReceiver: BroadcastReceiver() {
        /**
         * Is called when the data from the login is received.
         *
         * @param [context] the context of the response
         * @param [intent] the intent which contains the response data
         */
        override fun onReceive(context: Context, intent: Intent) {
            if (intent.hasExtra("loggedIn") && intent.hasExtra("username") && intent.hasExtra("jwt")) {
                if (intent.getBooleanExtra("loggedIn", false)) {
                    val username = intent.getStringExtra("username")
                    val jwt = intent.getStringExtra("jwt")
                    if (username != null && jwt != null) {
                        val preferences = PreferenceManager.getDefaultSharedPreferences(context)
                        preferences.edit().putString("username", username).putString("jwt", jwt).apply()
                        val login = Intent(this@LoginActivity, MainActivity::class.java)
                        this@LoginActivity.startActivityForResult(login, 0)
                    }
                }
            }
        }
    }
}
