package de.bkukr.bku.person

import android.os.Bundle
import android.support.wearable.activity.WearableActivity
import de.bkukr.bku.R
import de.bkukr.bku.general.isTeacher
import de.bkukr.bku.network.JSONHandlerWatch
import kotlinx.android.synthetic.main.activity_teacher_detail.*

// Copyright (C) 2020  Sven Op de Hipt
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

/**
 * Shows the list with teachers or classes to the user.
 *
 * @author Sven Op de Hipt
 */
class PersonDetailActivity : WearableActivity() {
    /**
     * Is called by the system when the activity is created.
     * Loads the person data from the server and parses it to the view.
     *
     * @param [savedInstanceState] the saved instance state
     */
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_teacher_detail)

        val id = intent?.getIntExtra("id", 0)

        if (id != null) {
            val serializer = DetailPerson.serializer()
            val params = mutableMapOf("id" to id.toString())

            if (isTeacher) {
                params["personType"] = "class"
            }

            // load the person data
            JSONHandlerWatch.getJSON(this, 6, serializer, params) { teacher ->
                var courses = ""

                for (course in teacher.courses) {
                    courses += course + "\n"
                }

                courses = courses.substring(0..courses.length - 2)

                teacherName.text = teacher.name
                coursesText.text = courses
                emailText.text = teacher.email
            }
        }
    }
}
