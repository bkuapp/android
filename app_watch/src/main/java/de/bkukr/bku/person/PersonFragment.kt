package de.bkukr.bku.person

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.RecyclerView
import androidx.wear.widget.WearableLinearLayoutManager
import de.bkukr.bku.R
import de.bkukr.bku.general.MainActivity
import kotlinx.android.synthetic.main.fragment_teacher.*

// Copyright (C) 2020  Sven Op de Hipt
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

/**
 * Shows the detailed person information to the user.
 *
 * @author Sven Op de Hipt
 */
class PersonFragment : Fragment() {
    /**
     * Is called by the system if the view needs to be created.
     *
     * @param [inflater] the layout inflater which converts the resource file to a view
     * @param [container] the view group which contains the view
     * @param [savedInstanceState] the saved instance state for this view
     *
     * @return the created view
     */
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? =
        inflater.inflate(R.layout.fragment_teacher, container, false)

    /**
     * Is called by the system when the activity is created or this view is created first.
     * Loads the person data from the main activity and parses it to the view.
     *
     * @param [savedInstanceState] the saved instance states for the view
     */
    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        val persons = (activity as MainActivity).persons

        if (persons != null) {
            val adapter = PersonAdapter(persons.firstOrNull() ?: emptyList())
            person_table.setHasFixedSize(true)
            person_table.isEdgeItemsCenteringEnabled = true
            person_table.layoutManager = WearableLinearLayoutManager(context)
            person_table.adapter = adapter
        }
    }

    /**
     * The adapter which maps the [persons] on the list.
     *
     * @constructor Creates a person adapter with the given [persons].
     *
     * @param [persons] the persons which will be mapped on the list
     */
    inner class PersonAdapter(private val persons: List<Person>):
        RecyclerView.Adapter<PersonAdapter.PersonViewHolder>() {

        /**
         * Creates a person row which is shown to the user.
         *
         * @param [parent] the group where the row will be created in
         * @param [viewType] the type of the row
         *
         * @return the created row
         */
        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PersonViewHolder {
            val view = LayoutInflater.from(context).inflate(R.layout.view_teacher_row, parent, false)

            return PersonViewHolder(view)
        }

        /**
         * Binds the person data for the specific [position] to the exam row to show the content to the user.
         *
         * @param [holder] the view holder which contains the person row view
         * @param [position] the position of the row which data should be mapped
         */
        override fun onBindViewHolder(holder: PersonViewHolder, position: Int) {
            val teacher = persons[position]

            holder.nameTextView.text = teacher.name

            holder.nameTextView.setOnClickListener {
                val toTeacherDetails = Intent(context, PersonDetailActivity::class.java)
                toTeacherDetails.putExtra("id", teacher.id)
                startActivity(toTeacherDetails)
            }
        }

        /**
         * Returns the number of rows in the person list.
         *
         * @return the number of rows in the person list
         */
        override fun getItemCount(): Int {
            return persons.size
        }

        /**
         * The view holder which represents a person row view.
         *
         * @constructor Creates an person view holder for the specified view.
         *
         * @param [view] the view, which will be parsed in the person view holder.
         */
        inner class PersonViewHolder(view: View) : RecyclerView.ViewHolder(view) {
            /**
             * The name text view of the person row.
             */
            val nameTextView: TextView = view.findViewById(R.id.teacher_name)
        }
    }

}
