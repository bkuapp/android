package de.bkukr.bku.person

import kotlinx.serialization.Serializable

// Copyright (C) 2020  Sven Op de Hipt
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

/**
 * Represents a class or teacher which is shown in the person detail view.
 *
 * @author Sven Op de Hipt
 *
 * @constructor Creates a person for the detail view with the given [name], the [email] and
 * the [courses] which the logged in user has with the class or teacher.
 *
 * @param [name] the name of the class or teacher
 * @param [email] the email of the class or teacher
 * @param [courses] the courses which the logged in user has with the class or teacher
 */
@Serializable
data class DetailPerson(val name: String, val email: String, val courses: Array<String>) {
    /**
     * Returns true if the [other] object is equal to the current object.
     *
     * @param [other] the other object which will be compared
     *
     * @return true if the [other] object is equal to the current object
     */
    override fun equals(other: Any?): Boolean {
        if (this === other) return true
        if (javaClass != other?.javaClass) return false

        other as DetailPerson

        if (name != other.name) return false
        if (email != other.email) return false
        if (!courses.contentEquals(other.courses)) return false

        return true
    }

    /**
     * Returns the hash code of the current object.
     *
     * @return the hash code of the current object
     */
    override fun hashCode(): Int {
        var result = name.hashCode()
        result = 31 * result + email.hashCode()
        result = 31 * result + courses.contentHashCode()
        return result
    }
}