package de.bkukr.bku.exam

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.RecyclerView
import androidx.wear.widget.WearableLinearLayoutManager
import de.bkukr.bku.R
import de.bkukr.bku.general.MainActivity
import kotlinx.android.synthetic.main.fragment_exam.*

// Copyright (C) 2020  Sven Op de Hipt
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

/**
 * Shows the list with the exams to the user.
 *
 * @author Sven Op de Hipt
 */
class ExamFragment : Fragment() {
    /**
     * Is called by the system if the view needs to be created.
     *
     * @param [inflater] the layout inflater which converts the resource file to an view
     * @param [container] the view group which contains the view
     * @param [savedInstanceState] the saved instance state for this view
     *
     * @return the created view
     */
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? =
        inflater.inflate(R.layout.fragment_exam, container, false)

    /**
     * Is called by the system when the activity is created or this view is created first.
     * Loads the exam data from the main activity and parses it to this view.
     *
     * @param [savedInstanceState] the saved instance states for the view
     */
    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        val exams = (activity as? MainActivity)?.exams

        if (exams != null) {
            val adapter = ExamAdapter(exams)
            exam_table.setHasFixedSize(true)
            exam_table.isEdgeItemsCenteringEnabled = true
            exam_table.layoutManager = WearableLinearLayoutManager(context)
            exam_table.adapter = adapter
        }
    }

    /**
     * The adapter which maps the [exams] on the list.
     *
     * @constructor Creates an exam adapter with the given [exams].
     *
     * @param [exams] the exams which will be mapped on the list
     */
    private inner class ExamAdapter(private val exams: List<Exam>):
        RecyclerView.Adapter<ExamAdapter.ExamViewHolder>() {

        /**
         * Creates an exam row which is shown to the user.
         *
         * @param [parent] the group where the row will be created in
         * @param [viewType] the type of the row
         *
         * @return the created row
         */
        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ExamViewHolder {
            val view = LayoutInflater.from(context).inflate(R.layout.view_exam_row, parent, false)

            return ExamViewHolder(view)
        }

        /**
         * Binds the exam data for the specific [position] to the exam row to show the content to the user.
         *
         * @param [holder] the view holder which contains the exam row view
         * @param [position] the position of the row which data should be mapped
         */
        override fun onBindViewHolder(holder: ExamViewHolder, position: Int) {
            val exam = exams[position]

            holder.courseTextView.text = exam.course.replace('\n', ' ')
            holder.dateTextView.text = exam.date
            holder.roomTextView.text = exam.room
            holder.timeTextView.text = exam.time
        }

        /**
         * Returns the number of rows in the exam list.
         *
         * @return the number of rows in the exam list
         */
        override fun getItemCount(): Int {
            return exams.size
        }

        /**
         * The view holder which represents an exam row view.
         *
         * @constructor Creates an exam view holder for the specified view.
         *
         * @param [view] the view, which will be parsed in the exam view holder.
         */
        private inner class ExamViewHolder(view: View) : RecyclerView.ViewHolder(view) {
            /**
             * The course text view of the exam row.
             */
            val courseTextView: TextView = view.findViewById(R.id.exam_row_course)
            /**
             * The date text view of the exam row.
             */
            val dateTextView: TextView = view.findViewById(R.id.exam_row_date)
            /**
             * The room text view of the exam row.
             */
            val roomTextView: TextView = view.findViewById(R.id.exam_row_room)
            /**
             * The time text view of the exam row.
             */
            val timeTextView: TextView = view.findViewById(R.id.exam_row_time)
        }
    }

}
