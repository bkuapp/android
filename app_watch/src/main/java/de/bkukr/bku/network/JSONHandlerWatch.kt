package de.bkukr.bku.network

import android.content.Context
import android.content.Intent
import de.bkukr.bku.login.LoginActivity
import kotlinx.serialization.KSerializer

// Copyright (C) 2020  Sven Op de Hipt
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

/**
 * The JSON Handler for the watch which hides the loading circle on error and logs the user out if the token is invalid.
 *
 * @author Sven Op de Hipt
 */
internal object JSONHandlerWatch {
    /**
     * Parses the json via the JSONHandler.
     *
     * @param [context] the context from which the request was made
     * @param [typeIndex] the type index of the request which classifies the request type
     * @param [serializer] the serializer which indicates what type the request result is
     * @param [postDataParams] the http post params which will be transmitted to the server
     * @param [onSuccess] is called if the request was successful and transmitted the request result
     */
    fun <T> getJSON(context: Context, typeIndex: Int, serializer: KSerializer<T>,
                    postDataParams: MutableMap<String, String> = mutableMapOf(), onSuccess: (T) -> Unit) {
        JSONHandler.getJSON(context, typeIndex, serializer, postDataParams, onSuccess) { error ->
            when (error) {
                "invalid token" -> {
                    val intent = Intent(context, LoginActivity::class.java)
                    context.startActivity(intent)
                }
            }
        }
    }
}