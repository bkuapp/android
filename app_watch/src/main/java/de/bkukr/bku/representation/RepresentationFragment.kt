package de.bkukr.bku.representation

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.RecyclerView
import androidx.wear.widget.WearableLinearLayoutManager
import de.bkukr.bku.R
import de.bkukr.bku.general.MainActivity
import kotlinx.android.synthetic.main.fragment_representation.*

// Copyright (C) 2020  Sven Op de Hipt
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

/**
 * Shows the representation list to the user.
 *
 * @author Sven Op de Hipt
 */
class RepresentationFragment: Fragment() {
    /**
     * Is called by the system if the view needs to be created.
     *
     * @param [inflater] the layout inflater which converts the resource file to a view
     * @param [container] the view group which contains the view
     * @param [savedInstanceState] the saved instance state for this view
     *
     * @return the created view
     */
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? =
        inflater.inflate(R.layout.fragment_representation, container, false)

    /**
     * Is called by the system when the activity is created or this view is created first.
     * Loads the representation data from the main activity and parses it to the view.
     *
     * @param [savedInstanceState] the saved instance states for the view
     */
    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        val representations = (activity as? MainActivity)?.representations

        if (representations != null) {
            val adapter = RepresentationAdapter(representations)
            representation_table.setHasFixedSize(true)
            representation_table.isEdgeItemsCenteringEnabled = true
            representation_table.layoutManager = WearableLinearLayoutManager(context)
            representation_table.adapter = adapter
        }
    }

    /**
     * The adapter which maps the [representations] on the list.
     *
     * @constructor Creates a representation adapter with the given [representations].
     *
     * @param [representations] the representations which will be mapped on the list
     */
    inner class RepresentationAdapter(private val representations: List<Representation>):
        RecyclerView.Adapter<RepresentationAdapter.RepresentationViewHolder>() {

        /**
         * Creates a representation row which is shown to the user.
         *
         * @param [parent] the group where the row will be created in
         * @param [viewType] the type of the row
         *
         * @return the created row
         */
        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RepresentationViewHolder {
            val view = LayoutInflater.from(context).inflate(R.layout.view_representation_row, parent, false)

            return RepresentationViewHolder(view)
        }

        /**
         * Binds the representation data for the specific [position] to the
         * representation row to show the content to the user.
         *
         * @param [holder] the view holder which contains the representation row view
         * @param [position] the position of the row which data should be mapped
         */
        override fun onBindViewHolder(holder: RepresentationViewHolder, position: Int) {
            val representation = representations[position]

            holder.courseTextView.text = representation.course
            holder.hourTextView.text = representation.hour
            holder.dateTextView.text = representation.date
            holder.roomTextView.text = representation.room

            holder.container.setOnClickListener {
                val toRepresentationDetails = Intent(context, RepresentationDetailActivity::class.java)
                toRepresentationDetails.putExtra("id", representation.id)
                startActivity(toRepresentationDetails)
            }
        }

        /**
         * Returns the number of rows in the representation list.
         *
         * @return the number of rows in the representation list
         */
        override fun getItemCount(): Int {
            return representations.size
        }

        /**
         * The view holder which represents a representation row view.
         *
         * @constructor Creates an representation view holder for the specified view.
         *
         * @param [view] the view, which will be parsed in the representation view holder.
         */
        inner class RepresentationViewHolder(view: View) : RecyclerView.ViewHolder(view) {
            /**
             * The container which contains all text views of the representation row.
             */
            val container: LinearLayout = view.findViewById(R.id.representation_container)
            /**
             * The course name text view of the representation row.
             */
            val courseTextView: TextView = view.findViewById(R.id.representation_row_course)
            /**
             * The hour text view of the representation row.
             */
            val hourTextView: TextView = view.findViewById(R.id.representation_row_hour)
            /**
             * The date text view of the representation row.
             */
            val dateTextView: TextView = view.findViewById(R.id.representation_row_date)
            /**
             * The room text view of the representation row.
             */
            val roomTextView: TextView = view.findViewById(R.id.representation_row_room)
        }
    }
}
