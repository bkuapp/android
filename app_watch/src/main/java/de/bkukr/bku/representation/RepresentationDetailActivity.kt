package de.bkukr.bku.representation

import android.os.Bundle
import android.support.wearable.activity.WearableActivity
import de.bkukr.bku.R
import de.bkukr.bku.network.JSONHandlerWatch
import kotlinx.android.synthetic.main.activity_representation_detail.*

// Copyright (C) 2020  Sven Op de Hipt
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

/**
 * Shows the representation detail view, which contains the representation rules, to the user.
 *
 * @author Sven Op de Hipt
 */
class RepresentationDetailActivity : WearableActivity() {
    /**
     * Is called by the system when the activity is created.
     * Loads the representation data from the server and parses it to the view.
     *
     * @param [savedInstanceState] the saved instance state
     */
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_representation_detail)

        val id = intent?.getIntExtra("id", 0)

        if (id != null) {
            // representation data loading
            val serializer = DetailRepresentation.serializer()
            val params = mutableMapOf("id" to id.toString())
            JSONHandlerWatch.getJSON(this, 202, serializer, params) { representation ->
                representation_rules.text = representation.representationRules

                title = representation.course
            }
        }
    }
}
