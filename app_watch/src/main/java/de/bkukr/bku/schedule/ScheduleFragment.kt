package de.bkukr.bku.schedule

import android.content.Intent
import android.graphics.Color
import android.graphics.Paint
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.LinearLayout
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.RecyclerView
import androidx.wear.widget.WearableLinearLayoutManager
import de.bkukr.bku.R
import de.bkukr.bku.general.MainActivity
import de.bkukr.bku.general.isTeacher
import kotlinx.android.synthetic.main.fragment_schedule.*

// Copyright (C) 2020  Sven Op de Hipt
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

/**
 * Shows the schedule list to the user.
 *
 * @author Sven Op de Hipt
 */
class ScheduleFragment : Fragment() {
    /**
     * Is called by the system if the view needs to be created.
     *
     * @param [inflater] the layout inflater which converts the resource file to a view
     * @param [container] the view group which contains the view
     * @param [savedInstanceState] the saved instance state for this view
     *
     * @return the created view
     */
    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? =
        inflater.inflate(R.layout.fragment_schedule, container, false)

    /**
     * Is called by the system when the activity is created or this view is created first.
     * Loads the schedule data from the main activity and parses it to the view.
     *
     * @param [savedInstanceState] the saved instance states for the view
     */
    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        val lessons = (activity as? MainActivity)?.lessons

        if (lessons != null) {
            val adapter = ScheduleAdapter(lessons)
            schedule_table.setHasFixedSize(true)
            schedule_table.isEdgeItemsCenteringEnabled = true
            schedule_table.layoutManager = WearableLinearLayoutManager(context)
            schedule_table.adapter = adapter
        }
    }

    /**
     * The adapter which maps the [lessons] on the list.
     *
     * @constructor Creates a representation adapter with the given [lessons].
     *
     * @param [lessons] the representations which will be mapped on the list
     */
    inner class ScheduleAdapter(private val lessons: List<ScheduleLesson>):
        RecyclerView.Adapter<ScheduleAdapter.ScheduleViewHolder>() {

        /**
         * Creates a schedule row which is shown to the user.
         *
         * @param [parent] the group where the row will be created in
         * @param [viewType] the type of the row
         *
         * @return the created row
         */
        override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ScheduleViewHolder {
            val view = LayoutInflater.from(context).inflate(R.layout.view_schedule_row, parent, false)

            return ScheduleViewHolder(view)
        }

        /**
         * Binds the schedule data for the specific [position] to the schedule row to show the content to the user.
         *
         * @param [holder] the view holder which contains the schedule row view
         * @param [position] the position of the row which data should be mapped
         */
        override fun onBindViewHolder(holder: ScheduleViewHolder, position: Int) {
            val lesson = lessons[position]

            holder.courseTextView.text = lesson.course
            holder.timeTextView.text = String.format("%s-%s", lesson.start, lesson.end)

            holder.roomTextView.text = lesson.room

            if (context != null) {
                if (context?.isTeacher == true) {
                    holder.teacherTextView.text = lesson.nameOfClass
                }
                else {
                    val text = String.format("%s %s", lesson.title,
                        lesson.lastName)

                    holder.teacherTextView.text = text
                }
            }

            holder.apply {
                when {
                    lesson.represented -> {
                        Color.GREEN
                    }
                    lesson.isCancelled -> {
                        teacherTextView.paintFlags =
                            teacherTextView.paintFlags or Paint.STRIKE_THRU_TEXT_FLAG
                        courseTextView.paintFlags =
                            courseTextView.paintFlags or Paint.STRIKE_THRU_TEXT_FLAG
                        timeTextView.paintFlags =
                            timeTextView.paintFlags or Paint.STRIKE_THRU_TEXT_FLAG
                        roomTextView.paintFlags =
                            roomTextView.paintFlags or Paint.STRIKE_THRU_TEXT_FLAG
                        Color.RED
                    }
                    else -> {
                        Color.WHITE
                    }
                }.apply {
                    teacherTextView.setTextColor(this)
                    courseTextView.setTextColor(this)
                    timeTextView.setTextColor(this)
                    roomTextView.setTextColor(this)
                }

                container.setOnClickListener {
                    val toScheduleDetails = Intent(context, ScheduleDetailsActivity::class.java)
                    toScheduleDetails.putExtra("id", lesson.id)
                    startActivity(toScheduleDetails)
                }
            }
        }

        /**
         * Returns the number of rows in the schedule list.
         *
         * @return the number of rows in the schedule list
         */
        override fun getItemCount(): Int {
            return lessons.size
        }

        /**
         * The view holder which represents a schedule row view.
         *
         * @constructor Creates an schedule view holder for the specified view.
         *
         * @param [view] the view, which will be parsed in the schedule view holder.
         */
        inner class ScheduleViewHolder(view: View) : RecyclerView.ViewHolder(view) {
            /**
             * The container which contains all text views of the schedule row.
             */
            val container: LinearLayout = view.findViewById(R.id.lesson_container)
            /**
             * The course name text view of the schedule row.
             */
            val courseTextView: TextView = view.findViewById(R.id.schedule_row_course)
            /**
             * The time text view of the schedule row.
             */
            val timeTextView: TextView = view.findViewById(R.id.schedule_row_time)
            /**
             * The teacher name text view of the schedule row.
             */
            val teacherTextView: TextView = view.findViewById(R.id.schedule_row_teacher)
            /**
             * The room text view of the schedule row.
             */
            val roomTextView: TextView = view.findViewById(R.id.schedule_row_room)
        }
    }

}
