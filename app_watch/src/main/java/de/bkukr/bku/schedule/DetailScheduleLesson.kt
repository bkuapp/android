package de.bkukr.bku.schedule

import kotlinx.serialization.Serializable

// Copyright (C) 2020  Sven Op de Hipt
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

/**
 * Represents a schedule lesson in the schedule detail view.
 *
 * @constructor Creates a schedule lesson with the given [name], the [email] of the teacher or class, the [start] time,
 * the [end] time, the [course] name and the [room].
 *
 * @param [name] the name of the lesson
 * @param [email] the email of the teacher or class which takes part in the lesson
 * @param [start] the start time of the lesson as schedule hour
 * @param [end] the end time of the lesson as schedule hour
 * @param [course] the course name of the lesson
 * @param [room] the room of the lesson
 *
 * @author Sven Op de Hipt
 */
@Serializable
data class DetailScheduleLesson(var name: String, var email: String, var start: Int, var end: Int, var course: String,
                                var room: String?)