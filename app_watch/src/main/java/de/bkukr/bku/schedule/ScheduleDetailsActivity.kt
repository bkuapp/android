package de.bkukr.bku.schedule

import android.os.Bundle
import android.support.wearable.activity.WearableActivity
import de.bkukr.bku.R
import de.bkukr.bku.network.JSONHandlerWatch
import kotlinx.android.synthetic.main.activity_schedule_details.*

// Copyright (C) 2020  Sven Op de Hipt
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

/**
 * Shows the detail schedule view to the user.
 *
 * @author Sven Op de Hipt
 */
class ScheduleDetailsActivity : WearableActivity() {
    /**
     * Is called by the system when the activity is created.
     * Loads the schedule data from the server and parses it to the view.
     *
     * @param [savedInstanceState] the saved instance state
     */
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_schedule_details)

        val id = intent?.getIntExtra("id", 0)

        if (id != null) {
            // schedule data loading
            val serializer = DetailScheduleLesson.serializer()
            val params = mutableMapOf("id" to id.toString())
            JSONHandlerWatch.getJSON(this, 4, serializer, params) { lesson ->
                courseName.text = lesson.course

                teacherName.text = lesson.name
                emailText.text = lesson.email
                timeText.text = String.format("%s. - %s.", lesson.start, lesson.end)
            }
        }
    }
}
