package de.bkukr.bku.network

import android.content.Context
import android.util.Log
import android.widget.Toast
import androidx.preference.PreferenceManager
import kotlinx.serialization.KSerializer
import kotlinx.serialization.json.Json
import kotlinx.serialization.json.JsonConfiguration
import kotlinx.serialization.parse

// Copyright (C) 2020  Sven Op de Hipt
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

/**
 * The json handler which parses the network json result to objects.
 *
 * @author Sven Op de Hipt
 */
object JSONHandler {
    /**
     * Loads a network string via the [NetworkHandler] for a specific [typeIndex] and parses it to an object of type [T]
     * which is transmitted via the [serializer]. Can also return a [ErrorResult] if an error occurred.
     *
     * @param [context] the context from which the request was made
     * @param [typeIndex] the type index of the request which classifies the request type
     * @param [serializer] the serializer which indicates what type the request result is
     * @param [postDataParams] the http post params which will be transmitted to the server
     * @param [onSuccess] is called if the request was successful and transmitted the request result
     * @param [onError] is called if an error occurred and transmitted the error message
     */
    fun <T> getJSON(context: Context, typeIndex: Int, serializer: KSerializer<T>,
                    postDataParams: MutableMap<String, String> = mutableMapOf(),
                    onSuccess: (T) -> Unit, onError: (error: String) -> Unit) {

        // load the request from the server
        NetworkHandler { response ->
            val json = Json { allowStructuredMapKeys = true }
            try {
                // throws an exception if no error occurred
                val error = json.decodeFromString(ErrorResult.serializer(), response).error

                Log.e("API Error", error)

                onError(error)
                if (error == "invalid token") {
                    Toast.makeText(context, R.string.invalidToken, Toast.LENGTH_LONG).show()
                }
            } catch (e: Exception) {
                try {
                    // parse the json
                    val result =
                        json.decodeFromString(RequestResult.serializer(serializer), response)

                    val preferences = PreferenceManager.getDefaultSharedPreferences(context)
                    val editor = preferences.edit()
                    editor.putString("jwt", result.token)
                    editor.apply()

                    onSuccess(result.res)
                } catch (e: Exception) {
                    // error at parsing the json
                    onError("json parsing error")
                    Toast.makeText(
                        context,
                        R.string.errorAtLoadingTheDataFromTheServer,
                        Toast.LENGTH_LONG
                    ).show()
                    Log.e(
                        "Parsing Error",
                        "Error at parsing the json for index $typeIndex: $e"
                    )
                }
            }
        }.execute(context, typeIndex, postDataParams)
    }
}
