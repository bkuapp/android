package de.bkukr.bku.network

import kotlinx.serialization.Serializable

// Copyright (C) 2020  Sven Op de Hipt
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

/**
 * The request result which is loaded by the server and holds the json web [token] and the request result.
 *
 * @author Sven Op de Hipt
 *
 * @constructor Creates request result which is loaded by the server and holds the json web [token] and the request result.
 *
 * @param [token] the json web token which is used for the authentication
 * @param [res] the request result
 */
@Serializable
internal data class RequestResult<T>(val token: String, val res: T)