package de.bkukr.bku.network

import android.content.Context
import android.content.SharedPreferences
import android.net.ConnectivityManager
import android.net.NetworkCapabilities
import android.os.AsyncTask
import android.util.Log
import androidx.preference.PreferenceManager
import java.io.*
import java.net.HttpURLConnection
import java.net.URL
import java.net.URLEncoder
import java.nio.charset.StandardCharsets
import javax.net.ssl.HttpsURLConnection

// Copyright (C) 2020  Sven Op de Hipt
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <https://www.gnu.org/licenses/>.

/**
 * The network handler which handles the network request and parses it to a [String].
 * Calls the [onFinish] method if the request has finished loading.
 *
 * @author Sven Op de Hipt
 *
 * @constructor Creates a network handler with the [onFinish] method which is called when the request is finished.
 *
 * @param [onFinish] the method which will be called when the request is finished
 */
class NetworkHandler(private val onFinish: (output: String) -> Unit = {}): AsyncTask<Any, Void, String?>() {
    /**
     * The method which will be called if the request should start.
     *
     * @param [array] the data which is used by the method bundled as an array.
     *
     * @return the string which is loaded from the network request
     */
    override fun doInBackground(vararg array: Any?): String? {
        val context = array[0]
        val typeIndex = array[1]
        val postDataParams = array[2]

        return if (context is Context && typeIndex is Int && postDataParams is MutableMap<*, *>) {
            if (hasInternetAccess(context)) {
                try {
                    val preferences: SharedPreferences =
                        PreferenceManager.getDefaultSharedPreferences(context)

                    val params = postDataParams as? MutableMap<String, String>

                    if (params != null) {
                        val url = URL("https://app.bkukr.de")

                        params["type"] = typeIndex.toString()

                        // add the jwt token if a jwt token is saved
                        if (preferences.contains("jwt")) {
                            params["jwt"] = preferences.getString("jwt", null) ?: ""
                        }

                        // prepare the request
                        val conn = url.openConnection() as? HttpURLConnection
                        conn?.readTimeout = 15000
                        conn?.connectTimeout = 15000
                        conn?.requestMethod = "GET"
                        conn?.doInput = true
                        conn?.doOutput = true

                        // convert the post data params to a string
                        val postDataString = getPostDataString(postDataParams)

                        // do not log if the password is included
                        if (!params.contains("password")) {
                            Log.d("params", postDataString)
                        }

                        // read the data from the request and parse it to a string
                        if (conn != null) {
                            val os = conn.outputStream
                            val writer =
                                BufferedWriter(OutputStreamWriter(os, StandardCharsets.UTF_8))
                            writer.write(postDataString)

                            writer.flush()
                            writer.close()
                            os.close()
                            val responseCode = conn.responseCode

                            if (responseCode == HttpsURLConnection.HTTP_OK) {
                                val br = BufferedReader(InputStreamReader(conn.inputStream))
                                val response = br.use { it.readText() }

                                Log.d("response", response)

                                response
                            } else {
                                null
                            }
                        } else {
                            null
                        }
                    } else {
                        null
                    }
                } catch (e: Exception) {
                    Log.e("Network Error (index: $typeIndex)", "Error during loading data from the database: $e")
                    null
                }
            }
            else {
                null
            }
        }
        else {
            null
        }
    }

    /**
     * Is called when the request has finished and the [result] string is available.
     * Calls the [onFinish] method and delivers the string with it.
     *
     * @param [result] the request result as string
     */
    override fun onPostExecute(result: String?) {
        super.onPostExecute(result)

        if (result != null) {
            onFinish(result)
        }
    }

    /**
     * Converts the http [params] from a map to a string and returns it.
     *
     * @param [params] the http params as map
     *
     * @return the http params as string
     */
    @Throws(UnsupportedEncodingException::class)
    private fun getPostDataString(params: Map<String, String>): String {
        val result = StringBuilder()
        var first = true
        for ((key, value) in params) {
            if (first) {
                first = false
            } else {
                result.append("&")
            }

            result.append(URLEncoder.encode(key, "UTF-8"))
            result.append("=")
            result.append(URLEncoder.encode(value, "UTF-8"))
        }

        return result.toString()
    }

    /**
     * Returns true if the user has internet access. Checks if a network is available and does a
     * test request with 0 byte to test the internet connection.
     *
     * @param [context] the context from which the internet access should be checked
     * @param [handler] the handler which will transmit if the user has internet or not
     */
    private fun hasInternetAccess(context: Context?): Boolean {
        var internetAccess = false

        val checkThread = Thread(
            Runnable {
                if (isNetworkAvailable(context)) {
                    try {
                        val connection = URL("https://clients3.google.com/generate_204").openConnection()
                        connection.setRequestProperty("User-Agent", "Android")
                        connection.setRequestProperty("Connection", "close")
                        connection.connectTimeout = 1500
                        connection.connect()
                        internetAccess = (connection as? HttpURLConnection)?.responseCode == 204 && connection.contentLength == 0
                    } catch (e: IOException) {
                        Log.e("network error", "Error checking internet connection", e)
                    }
                } else {
                    Log.e("network error", "No network available!")
                }
            }
        )

        checkThread.start()

        checkThread.join()

        return internetAccess
    }

    /**
     * Checks if the user has a connection to a network.
     *
     * @param [context] the context from which the network access should be checked
     *
     * @return true if the user is connected to a network
     */
    private fun isNetworkAvailable(context: Context?): Boolean {
        val connectivityManager = context?.getSystemService(Context.CONNECTIVITY_SERVICE) as? ConnectivityManager
        val activeNetwork = connectivityManager?.activeNetwork
        val networkCapabilities = connectivityManager?.getNetworkCapabilities(activeNetwork) ?: return false

        return when {
            networkCapabilities.hasTransport(NetworkCapabilities.TRANSPORT_WIFI) -> true
            networkCapabilities.hasTransport(NetworkCapabilities.TRANSPORT_CELLULAR) -> true
            networkCapabilities.hasTransport(NetworkCapabilities.TRANSPORT_ETHERNET) -> true
            else -> false
        }
    }
}